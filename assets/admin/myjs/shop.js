function postData(url, data, callback) {
	$.ajax({
		type: "POST",
		contentType: "application/json",
		dataType: "json",
		url: url,
		data: JSON.stringify(data),
		success: function (response) {
			return callback(null, response);
		},
		error: function (err) {
			return callback(true, err);
		}
	});
}

function loadView(url, div) {
	$.ajax({
		url: url,
		beforeSend: function () {
		},
		success: function (data) {
			$(div).html(data);
		}
	});
}

function do_something(url) {
	$.ajax({
		type: "POST",
		url: url
	});
}

function _(id){
	return document.getElementById(id);
}

function ValidateEmail(mail, btn, btn_error, id_msg) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
		$(id_msg).hide();
		$(btn_error).hide();
		return true;
	}
	$(id_msg).show();
	$(btn).hide();
	$(btn_error).show();
}

function shop_menu(sidebar, shop_url) {
	var old_sidebar = document.cookie.replace(/(?:(?:^|.*;\s*)sidebar\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(old_sidebar).removeClass("active");

	$(sidebar).addClass("active");

	document.cookie = "sidebar="+sidebar;
	document.cookie = "shop_url="+shop_url;

	$("#search").val('');
	$("#product_categories").val('');

	loadView(shop_url, '.content');
}

function hs_menu(sidebar, shop_url, d_hide, d_show) {
	var old_sidebar = document.cookie.replace(/(?:(?:^|.*;\s*)sidebar\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(old_sidebar).removeClass("active");

	$(sidebar).addClass("active");

	document.cookie = "sidebar="+sidebar;
	document.cookie = "shop_url="+shop_url;
	document.cookie = "d_hide="+d_hide;
	document.cookie = "d_show="+d_show;

	$("#search").val('');
	$("#product_categories").val('');

	$(d_hide).hide();
	$(d_show).show();

	loadView(shop_url, '.content');
}

function logout_shop() {

	do_something("shop_controller/logout");
	setTimeout(() => {
		hs_menu("#home", "shop_controller/home/1/null/null",'#','#sidebar');
		setTimeout(() => {
		location.reload();				
		}, 500);
	}, 500);

}