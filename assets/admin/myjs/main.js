function postData(url, data, callback) {
	$.ajax({
		type: "POST",
		contentType: "application/json",
		dataType: "json",
		url: url,
		data: JSON.stringify(data),
		success: function (response) {
			return callback(null, response);
		},
		error: function (err) {
			return callback(true, err);
		}
	});
}

function loadPagination(url, data, div) {
	$.ajax({
		type: "POST",
		contentType: "application/json",
		dataType: "json",
		url: url,
		data: JSON.stringify(data),
		success: function (data) {
			$(div).html(data);
		}
	});
}

function loadView(url, div) {
	$.ajax({
		url: url,
		beforeSend: function () {
		},
		success: function (data) {
			$(div).html(data);
		}
	});
}

function do_something(url) {
	$.ajax({
		type: "POST",
		url: url
	});
}

function _(id){
	return document.getElementById(id);
}

function logout() {
	var main_id = '#home';
	var admin_url= "main_controller/home/1";

	document.cookie = "main_id=" + main_id;
	document.cookie = "main_sub_id=";
	document.cookie = "sub_id=";
	document.cookie = "admin_url="+admin_url;

		do_something("main_controller/logout");
		setTimeout(() => {
			location.reload();
		}, 500);
	
}

function ValidateEmail(mail, btn, id_msg) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
		$(id_msg).hide();
		return true;
	}
	$(id_msg).show();
	$(btn).attr("disabled", "disabled");
}

function main_menu(main_id, admin_url) {
	var main_id_old = document.cookie.replace(/(?:(?:^|.*;\s*)main_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(main_id_old).removeClass("active");

	var main_sub_id = document.cookie.replace(/(?:(?:^|.*;\s*)main_sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(main_sub_id).removeClass("active");

	var sub_id = document.cookie.replace(/(?:(?:^|.*;\s*)sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(sub_id).removeClass("active");

	$(main_id).addClass("active");

	document.cookie = "main_id=" + main_id;
	document.cookie = "main_sub_id=";
	document.cookie = "sub_id=";
	document.cookie = "admin_url="+admin_url;

	loadView(admin_url, '.content');
}

function sub_menu(main_sub_id, sub_id, admin_url) {
	var main_id = document.cookie.replace(/(?:(?:^|.*;\s*)main_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(main_id).removeClass("active");

	var main_sub_id_old = document.cookie.replace(/(?:(?:^|.*;\s*)main_sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(main_sub_id_old).removeClass("active");

	var sub_id_old = document.cookie.replace(/(?:(?:^|.*;\s*)sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(sub_id_old).removeClass("active");

	$(main_sub_id).addClass("active");
	$(sub_id).addClass("active");

	document.cookie = "main_id=";
	document.cookie = "main_sub_id=" + main_sub_id;
	document.cookie = "sub_id=" + sub_id;
	document.cookie = "admin_url="+admin_url;

	loadView(admin_url, '.content');
}