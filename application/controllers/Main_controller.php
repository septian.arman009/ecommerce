<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        $this->load->model('d_table');
        $this->perPage = 10;
        $this->auth();
    }

    public function auth()
    {
        if ($this->session->userdata('com_in')) {
            return true;
        } else {
            redirect('signin');
        }
    }

    public function index()
    {
        if($_SESSION['com_in']['role'] == 3){
            $data['check_unread_main'] = $this->main_model->count2where('discussion', 'id', 'status', 'unread', 'to_email', $_SESSION['com_in']['email']);
        }else{
            $data['bill_count'] = $this->main_model->count('billing', 'id');
        }
        $this->load->view('content/admin/main', $data);
    }

    public function home($page)
    {

        $total_row = $this->main_model->count('log_activity', 'id');
        $data['all'] = ceil($total_row / 10);

        if ($page != 1) {

            $end = $this->perPage * $page;
            $start = $end - 10;
            $data['page'] = $page;

            if ($start > 0) {
                $data['logs'] = $this->main_model->limit($start, $end, 'log_activity');
                $this->load->view('content/admin/home/home', $data);
            } else {
                $data['logs'] = '';
                $this->load->view('content/admin/home/home', $data);
            }

        } else {
            $data['page'] = $page;

            $data['logs'] = $this->main_model->limit(0, $this->perPage, 'log_activity');

            $this->load->view('content/admin/home/home', $data);
        }

    }

    public function logout()
    {
        $this->session->unset_userdata('com_in');
    }

    public function destroy($table, $column)
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;

        if ($table == 'users' && $id == $_SESSION['com_in']['id']) {
            $status = array('status' => 'error');
            $this->mylib->setJSON();
            echo json_encode($status);

        } else if ($_SESSION['com_in']['role'] == 4) {

            if ($table == 'users') {

                $this->main_model->destroy($table, $column, $id);
                $activity['user_id'] = $_SESSION['com_in']['id'];
                $activity['name'] = $_SESSION['com_in']['name'];
                $activity['activity'] = 'Menghapus Pengguna dengan ID :' . $id . ' pada ' . date('d/m/Y H:i:s');

                $this->main_model->store('log_activity', $activity);
                $status = array('status' => 'success');
                $this->mylib->setJSON();
                echo json_encode($status);
            } else if ($table == 'members') {
                $this->main_model->destroy($table, $column, $id);

                $activity['user_id'] = $_SESSION['com_in']['id'];
                $activity['name'] = $_SESSION['com_in']['name'];
                $activity['activity'] = 'Menghapus Memmber dengan ID :' . $id . ' pada ' . date('d/m/Y H:i:s');
                $this->main_model->store('log_activity', $activity);
                $this->main_model->destroy('billing', 'member_id', $id);
                $this->main_model->destroy('purchasement', 'member_id', $id);
                $this->main_model->destroy('address', 'member_id', $id);
                $this->main_model->destroy('cart', 'member_id', $id);
                $this->main_model->destroy('discussion', 'member_id', $id);
                $this->main_model->destroy('reward', 'member_id', $id);
                $activity['user_id'] = $_SESSION['com_in']['id'];
                $activity['name'] = $_SESSION['com_in']['name'];
                $activity['activity'] = 'Menghapus Member dengan ID :' . $id . ' pada ' . date('d/m/Y H:i:s');

                $this->main_model->store('log_activity', $activity);
                $status = array('status' => 'success');
                $this->mylib->setJSON();
                echo json_encode($status);
            }

        } else if ($_SESSION['com_in']['role'] == 1) {

            if ($table == 'slider') {
                $getFileName = $this->main_model->gda3p('slider', 'id', $id);

                $delete = $this->main_model->destroy($table, $column, $id);

                if ($delete) {
                    $activity['user_id'] = $_SESSION['com_in']['id'];
                    $activity['name'] = $_SESSION['com_in']['name'];
                    $activity['activity'] = 'Menghapus Slide dengan ID :' . $id . ' pada ' . date('d/m/Y H:i:s');

                    $this->main_model->store('log_activity', $activity);
                    if ($getFileName[0]['image'] != '') {
                        unlink("./assets/slider/" . $getFileName[0]['image']);
                    }
                    $status = array('status' => 'success');
                    $this->mylib->setJSON();
                    echo json_encode($status);
                }
            }else if ($table == 'categories') {
                $check = $this->main_model->gda3p('products', 'product_categories', $id);
                if (!$check) {
                    $delete = $this->main_model->destroy($table, $column, $id);
                    if ($delete) {
                        $activity['user_id'] = $_SESSION['com_in']['id'];
                        $activity['name'] = $_SESSION['com_in']['name'];
                        $activity['activity'] = 'Menghapus Kategori dengan ID :' . $id . ' pada ' . date('d/m/Y H:i:s');

                        $this->main_model->store('log_activity', $activity);
                        $status = array('status' => 'success');
                        $this->mylib->setJSON();
                        echo json_encode($status);
                    }
                } else {
                    $status = array('status' => 'used');
                    $this->mylib->setJSON();
                    echo json_encode($status);
                }

            } else if ($table == 'products') {
                $getFileName = $this->main_model->gda3p('products', 'id', $id);

                $delete = $this->main_model->destroy($table, $column, $id);

                if ($delete) {
                    if ($getFileName[0]['image'] != '') {
                        unlink("./assets/products/" . $getFileName[0]['image']);
                    }
                    $activity['user_id'] = $_SESSION['com_in']['id'];
                    $activity['name'] = $_SESSION['com_in']['name'];
                    $activity['activity'] = 'Menghapus Produk dengan ID :' . $id . ' pada ' . date('d/m/Y H:i:s');

                    $this->main_model->store('log_activity', $activity);
                    $status = array('status' => 'success');
                    $this->mylib->setJSON();
                    echo json_encode($status);
                }
            } else if ($table == 'bank') {
                $getFileName = $this->main_model->gda3p('bank', 'id', $id);

                $delete = $this->main_model->destroy($table, $column, $id);

                if ($delete) {
                    if ($getFileName[0]['image'] != '') {
                        unlink("./assets/banks/" . $getFileName[0]['image']);
                    }
                    $activity['user_id'] = $_SESSION['com_in']['id'];
                    $activity['name'] = $_SESSION['com_in']['name'];
                    $activity['activity'] = 'Menghapus Bank Penerima dengan ID :' . $id . ' pada ' . date('d/m/Y H:i:s');

                    $this->main_model->store('log_activity', $activity);
                    $status = array('status' => 'success');
                    $this->mylib->setJSON();
                    echo json_encode($status);
                }
            } 

        } else if ($_SESSION['com_in']['role'] == 3) {

            if ($table == 'reward_list') {

                $delete = $this->main_model->destroy($table, $column, $id);

                if ($delete) {
                    $activity['user_id'] = $_SESSION['com_in']['id'];
                    $activity['name'] = $_SESSION['com_in']['name'];
                    $activity['activity'] = 'Menghapus Reward Umum dengan ID :' . $id . ' pada ' . date('d/m/Y H:i:s');

                    $this->main_model->store('log_activity', $activity);
                    $status = array('status' => 'success');
                    $this->mylib->setJSON();
                    echo json_encode($status);
                }
            }

        } else if ($_SESSION['com_shop']['role'] == 2) {

            if ($table == 'address') {

                $delete = $this->main_model->destroy($table, $column, $id);

                if ($delete) {
                    $status = array('status' => 'success');
                    $this->mylib->setJSON();
                    echo json_encode($status);
                }
            }

        } else {
            $status = array('status' => 'error');
            $this->mylib->setJSON();
            echo json_encode($status);

        }

    }

    public function check_email()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;
        $email = $obj->email;

        if ($id == 'null') {
            $check = $this->main_model->gda3p($table, 'email', $email);

            if (!$check) {
                $status = array('status' => 'success');
                $this->mylib->setJSON();
                echo json_encode($status);
            }

        } else {

            $email_old = $this->main_model->gdo4p($table, 'email', 'id', $id);

            if ($email == $email_old) {
                $status = array('status' => 'success');
                $this->mylib->setJSON();
                echo json_encode($status);
            } else {
                if ($table == 'users') {
                    $check = $this->main_model->gda3p('users', 'email', $email);

                    if (!$check) {
                        $status = array('status' => 'success');
                        $this->mylib->setJSON();
                        echo json_encode($status);
                    }
                } else {
                    $check_users = $this->main_model->gda3p('users', 'email', $email);
                    $check_members = $this->main_model->gda3p('members', 'email', $email);

                    if (!$check_users && !$check_members) {
                        $status = array('status' => 'success');
                        $this->mylib->setJSON();
                        echo json_encode($status);
                    }
                }

            }

        }

    }

    public function email_sender()
    {
        $data['setting'] = $this->main_model->gda3p('setting', 'id', 1);
        $this->load->view('content/admin/settings/email_sender', $data);
    }

    public function update_email_sender()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['send_mail'] = $obj->email;
        $data['send_pass'] = $obj->password;

        $update = $this->main_model->update('setting', $data, 'id', 1);
        if ($update) {
            $activity['user_id'] = $_SESSION['com_in']['id'];
            $activity['name'] = $_SESSION['com_in']['name'];
            $activity['activity'] = 'Mengupdate Email Pengiriman :' . $data['send_mail'] . ' pada ' . date('d/m/Y H:i:s');

            $this->main_model->store('log_activity', $activity);
            $status = array('status' => 'success');
            $this->mylib->setJSON();
            echo json_encode($status);
        }

    }

    public function change_password()
    {
        $this->load->view('content/admin/settings/change_password');
    }

    public function check_old_password()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $password = md5($obj->old_password);

        $check = $this->main_model->gda5p('users', 'password', $password, 'id', $_SESSION['com_in']['id']);
        if ($check) {
            $status = array('status' => 'success');
            $this->mylib->setJSON();
            echo json_encode($status);
        }

    }

    public function update_password()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['password'] = md5($obj->password);

        $update = $this->main_model->update('users', $data, 'id', $_SESSION['com_in']['id']);
        if ($update) {
            $activity['user_id'] = $_SESSION['com_in']['id'];
            $activity['name'] = $_SESSION['com_in']['name'];
            $activity['activity'] = 'Mengubah password pada ' . date('d/m/Y H:i:s');

            $this->main_model->store('log_activity', $activity);
            $status = array('status' => 'success');
            $this->mylib->setJSON();
            echo json_encode($status);
        }

    }

    public function detail()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;

        $data = $this->main_model->gda3p($table, 'id', $id);

        if ($data) {
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $data);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

}
