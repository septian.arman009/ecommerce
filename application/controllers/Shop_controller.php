<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Shop_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->perPage = 6;
        $this->load->model('main_model');
        $this->load->library('cart');
    }

    public function auth()
    {
        if ($this->session->userdata('com_shop')) {
            return true;
        } else {
            redirect('signin');
        }
    }

    public function index()
    {
        if ($this->session->userdata('com_shop')) {
            $data['reward'] = $this->main_model->count2where('reward', 'id', 'status', 'tersedia', 'member_id', $_SESSION['com_shop']['id']);
            $data['check_unread_main'] = $this->main_model->count2where('discussion', 'id', 'status', 'unread', 'to_email', $_SESSION['com_shop']['email']);
        }
        $data['categories'] = $this->main_model->gda1p('categories');
        $this->load->view('shop/shop', $data);
    }

    public function home($page, $key, $categories)
    {
        if ($this->session->userdata('com_shop')) {
            $this->check_billing();
            $this->check_reward();
        }

        $total_row = $this->main_model->count('products', 'id');
        $data['all'] = ceil($total_row / 6);

        if ($page != 1) {

            $end = $this->perPage * $page;
            $start = $end - 6;
            $data['page'] = $page;

            if ($start > 0) {
                if ($key != 'null' && $categories != 'null') {
                    $data['products'] = $this->main_model->limit2Where($start, $end, 'products', 'product_name', $key, 'product_categories', $categories);
                } else if ($key != 'null' && $categories == 'null') {
                    $data['products'] = $this->main_model->limitWhere($start, $end, 'products', 'product_name', $key);
                } else if ($key == 'null' && $categories != 'null') {
                    $data['products'] = $this->main_model->limitWhereNotLike($start, $end, 'products', 'product_categories', $categories);
                } else {
                    $data['products'] = $this->main_model->limit($start, $end, 'products');
                }
            }

        } else {
            $data['page'] = $page;

            if ($key != 'null' && $categories != 'null') {
                $data['products'] = $this->main_model->limit2Where(0, $this->perPage, 'products', 'product_name', $key, 'product_categories', $categories);
            } else if ($key != 'null' && $categories == 'null') {
                $data['products'] = $this->main_model->limitWhere(0, $this->perPage, 'products', 'product_name', $key);
            } else if ($key == 'null' && $categories != 'null') {
                $data['products'] = $this->main_model->limitWhereNotLike(0, $this->perPage, 'products', 'product_categories', $categories);
            } else {
                $data['products'] = $this->main_model->limit(0, $this->perPage, 'products');
            }
        }

        $data['key_pass'] = $key;
        $data['categori'] = $categories;
        $data['categories'] = $this->main_model->gda1p('categories');
        $data['slider'] = $this->main_model->gda1p('slider');
        $this->load->view('shop/content/home', $data);
    }

    public function product_detail($id)
    {
        $categories = $this->main_model->gdo4p('products', 'product_categories', 'id', $id);
        $data['related'] = $data['products'] = $this->main_model->limitWhereNotLikeOrder(0, $this->perPage, 'products', 'product_categories', $categories, 'random');
        $data['products'] = $this->main_model->gda3p('products', 'id', $id);
        $this->load->view('shop/content/product_detail', $data);
    }

    public function checkout()
    {
        if ($this->session->userdata('com_shop')) {
            $data['address'] = $this->main_model->gda3p('address', 'member_id', $_SESSION['com_shop']['id']);
            $this->load->view('shop/content/choose_address', $data);
        } else {
            $this->sign_form();
        }
    }

    public function choose_bank()
    {
        $this->auth();
        $data['bank'] = $this->main_model->gda1p('bank');
        $this->load->view('shop/content/choose_bank', $data);
    }

    public function transfer_detail()
    {
        $this->auth();

        $cart = $this->main_model->gda3p('cart', 'member_id', $_SESSION['com_shop']['id']);
        $total = 0;
        foreach ($cart as $key => $value) {
            if ($total == 0) {
                $total = $value['subtotal'];
            } else {
                $total += $value['subtotal'];
            }
        }

        $data['bank'] = $this->main_model->gda3p('bank', 'id', $_COOKIE['id_bank']);

        if (!isset($_COOKIE['unik'])) {
            $data['unik'] = $this->mylib->random_number(3);
        } else {
            if ($_COOKIE['unik'] != 0) {
                $data['unik'] = $_COOKIE['unik'];
            } else {
                $data['unik'] = $this->mylib->random_number(3);
            }

        }

        if (!isset($_COOKIE['voucher'])) {
            $data['discount'] = 0; 
            $data['percentage'] = 0; 
        } else {
            if($_COOKIE['voucher']!=''){
                $data_voucher = $this->main_model->gda3p('reward', 'voucher', $_COOKIE['voucher']);
                if($data_voucher){
                    $data['percentage'] = $data_voucher[0]['discount'];
                    $data['discount'] = (($total/100)*$data_voucher[0]['discount']); 
                }
            }else{
                $data['discount'] = 0; 
                $data['percentage'] = 0; 
            }
           
        }
        
        $data['total'] = ($total + $data['unik']) - $data['discount'];
        $this->load->view('shop/content/transfer_detail', $data);
    }

    public function detail_order()
    {
        $this->auth();
        $data['cart'] = $this->main_model->gda3p('cart', 'member_id', $_SESSION['com_shop']['id']);
        $data['bank'] = $this->main_model->gda3p('bank', 'id', $_COOKIE['id_bank']);
        $data['billing_transfer'] = $_COOKIE['billing_transfer'];
        $data['unik'] = $_COOKIE['unik'];
        $this->load->view('shop/content/detail_order', $data);
    }

    public function payment()
    {
        $data['billing'] = $this->main_model->gda3p('billing', 'member_id', $_SESSION['com_shop']['id']);
        foreach ($data['billing'] as $key => $value) {
            $data['receiver_bank'][] = $this->main_model->gda3p('bank', 'id', $value['bank_id']);
        }
        $data['bill'] = $this->main_model->countwhere('billing', 'id', 'p_o_pay', '');

        $data['purchased'] = $this->main_model->gda3p('purchasement', 'member_id', $_SESSION['com_shop']['id']);
        $data['purchase'] = count($data['purchased']);
        if ($data['purchased']) {
            $data['address'] = unserialize($data['purchased'][0]['address']);
        }

        $this->load->view('shop/content/payment', $data);
    }

    public function delivered()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $data['status'] = 'delivered';
        $data['satisfaction'] = 'unreview';

        $inv = $this->main_model->gdo4p('purchasement', 'purchasement_inv', 'id', $id);
        $update = $this->main_model->update('purchasement', $data, 'id', $id);
        if ($update) {
            $member_id = $this->main_model->gdo4p('purchasement', 'member_id', 'id', $id);
            $purchase = $this->main_model->countwhere('purchasement', 'id', 'member_id', $member_id);
            if ($purchase == 1) {
                $reward['member_id'] = $member_id;
                $reward['discount'] = 10;

                $reward['expired'] = 365;
                $day = $reward['expired'] * 24;
                $validity = '+' . $day . ' hour';
                $startTime = date("Y-m-d H:i:s");
                $cenvertedTime = date('Y-m-d H:i:s', strtotime($validity, strtotime($startTime)));

                $reward['voucher'] = 'BELANJAPERTAMA';
                $reward['validity'] = $cenvertedTime;
                $reward['description'] = 'Rewar transaki pertama kamu :)';
                $reward['status'] = 'tersedia';

                $this->main_model->store('reward', $reward);
            }
            $activity['user_id'] = $_SESSION['com_shop']['id'];
            $activity['name'] = $_SESSION['com_shop']['name'];
            $activity['activity'] = 'Konfirmasi Terima Barang dari Member : '.$_SESSION['com_shop']['name'].' dengan No. Pembayaran : '.$inv.' pada ' . date('d/m/Y H:i:s');

            $this->main_model->store('log_activity', $activity);
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $update);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function save_review()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $data['satisfaction'] = $obj->satisfaction;
        $data['review'] = $obj->review;

        $store = $this->main_model->update('purchasement', $data, 'id', $id);
        if ($store) {
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $store);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function detail_inv($id)
    {
        $data['billing'] = $this->main_model->gda3p('billing', 'id', $id);
       
        foreach ($data['billing'] as $key => $value) {
            $data['product_id'] = unserialize($value['product_id']);
            $data['product_qty'] = unserialize($value['product_qty']);
            $data['product_price'] = unserialize($value['product_price']);
            $data['product_subtotal'] = unserialize($value['product_subtotal']);
            $data['product_image'] = unserialize($value['product_image']);
            $data['buyer_notes'] = unserialize($value['buyer_notes']);
            $data['billing_transfer'] = $value['billing_transfer'];
            $data['discount'] = $value['discount'];
            $data['discount_ammount'] = $value['discount_ammount'];
            $data['address'] = $this->main_model->gda3p('address', 'id', $value['address_id']);
            $data['p_o_pay'] = $value['p_o_pay'];
        }

        foreach ($data['product_id'] as $key => $value) {
            $data['name'][] = $this->main_model->gdo4p('products', 'product_name', 'id', $value);
        }

        $data['province'] = $this->main_model->gdo4p('province', 'name', 'id', $data['address'][0]['province_id']);
        $data['city'] = $this->main_model->gdo4p('city', 'name', 'id', $data['address'][0]['city_id']);
        $data['district'] = $this->main_model->gdo4p('district', 'name', 'id', $data['address'][0]['district_id']);

        $this->load->view('shop/content/detail_inv', $data);
    }

    public function save_billing()
    {
        $this->auth();
        $cart = $this->main_model->gda3p('cart', 'member_id', $_SESSION['com_shop']['id']);
        $ok = 0;
        $total = 0;
        foreach ($cart as $key => $value) {
            $stock = $this->main_model->gdo4p('products', 'stock', 'id', $value['product_id']);

            if ($value['qty'] <= $stock) {
                $product_id[] = $value['product_id'];
                $product_qty[] = $value['qty'];
                $product_price[] = $value['price'];
                $product_subtotal[] = $value['subtotal'];
                $product_image[] = $value['image'];
                $buyer_notes[] = $value['buyer_notes'];
                $ok++;
                $stock_data['stock'] = $stock - $value['qty'];
                $this->main_model->update('products', $stock_data, 'id', $value['product_id']);
                $this->main_model->destroy('cart', 'id', $value['id']);
                $total += $value['subtotal'];
            }

        }

        $startTime = date("Y-m-d H:i:s");

        $cenvertedTime = date('Y-m-d H:i:s', strtotime('+24 hour', strtotime($startTime)));

        $data['billing_inv'] = $this->generate_inv();
        $data['product_id'] = serialize($product_id);
        $data['product_qty'] = serialize($product_qty);
        $data['product_price'] = serialize($product_price);
        $data['product_subtotal'] = serialize($product_subtotal);
        $data['product_image'] = serialize($product_image);
        $data['buyer_notes'] = serialize($buyer_notes);
        $data['member_id'] = $_SESSION['com_shop']['id'];
        $data['address_id'] = $_COOKIE['id_address'];
        $data['bank_id'] = $_COOKIE['id_bank'];
        $data['expired'] = $cenvertedTime;
        $data['status'] = 'active';
        $data['billing_transfer'] = $total - $_COOKIE['discount'];

        $data['discount_ammount'] = $_COOKIE['discount'];
        $data['discount'] = $_COOKIE['percentage'];

        $store = $this->main_model->store('billing', $data);
        if ($store) {
            $activity['user_id'] = $_SESSION['com_shop']['id'];
            $activity['name'] = $_SESSION['com_shop']['name'];
            $activity['activity'] = 'Order baru dari Member '.$_SESSION['com_shop']['name'].' No. Tagihan:' . $data['billing_inv'] . ' pada ' . date('d/m/Y H:i:s');

            $this->main_model->store('log_activity', $activity);

            
            if (isset($_COOKIE['voucher'])) {
                if($_COOKIE['voucher'] != ''){
                    $data_voucher['status'] = 'terpakai';
                    $this->main_model->update6p('reward', $data_voucher, 'voucher', $_COOKIE['voucher'], 'member_id', $_SESSION['com_shop']['id']);
                }
               
            } 

            if (count($cart) == $ok) {
                $status = array('status' => 'success');
                $this->mylib->setJSON();
                echo json_encode($status);
            } else {
                $status = array('status' => 'error');
                $this->mylib->setJSON();
                echo json_encode($status);
            }
        }

    }

    public function generate_inv()
    {
        $date = explode('-', date('y-m-d'));
        $max = $this->main_model->max('billing', 'billing_inv');
        if ($max) {
            $inv = explode('/', $max);
            $id = $inv[3] + 1;
            return 'INV/T/' . $date[0] . '/' . $id;
        } else {
            return 'INV/T/' . $date[0] . '/1';
        }
    }

    public function myaccount()
    {
        $this->auth();
        $data['address'] = $this->main_model->gda3p('address', 'member_id', $_SESSION['com_shop']['id']);
        $data['members'] = $this->main_model->gda3p('members', 'id', $_SESSION['com_shop']['id']);
        $this->load->view('shop/content/myaccount', $data);
    }

    public function register_form()
    {
        $this->load->view('shop/register');
    }

    public function sign_form()
    {
        $this->load->view('shop/signin');
    }

    public function add_adress($id)
    {
        $this->auth();
        $data['id'] = $id;
        $data['province'] = $this->main_model->gda1p('province');
        $this->load->view('shop/content/address_form', $data);
    }

    public function save_address()
    {
        $this->auth();
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $data['name'] = $obj->name;
        $data['phone'] = $obj->phone;
        $data['address'] = $obj->address;
        $data['province_id'] = $obj->province;
        $data['city_id'] = $obj->city;
        $data['district_id'] = $obj->district;
        $data['postcode'] = $obj->postcode;
        $data['member_id'] = $_SESSION['com_shop']['id'];

        if ($id == 'null') {
            $store = $this->main_model->store('address', $data);

            if ($store) {
                $status = array('status' => 'success');
                $this->mylib->setJSON();
                echo json_encode($status);
            } else {
                $status = array('status' => 'error');
                $this->mylib->setJSON();
                echo json_encode($status);
            }
        } else {
            $update = $this->main_model->update('address', $data, 'id', $id);

            if ($update) {
                $status = array('status' => 'success');
                $this->mylib->setJSON();
                echo json_encode($status);
            } else {
                $status = array('status' => 'error');
                $this->mylib->setJSON();
                echo json_encode($status);
            }
        }

    }

    public function add_ajax_city($prov_id)
    {
        $this->auth();
        $query = $this->db->get_where('city', array('province_id' => $prov_id));
        $data = "<option value=''>- Pilih Kota / Kabupaten -</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='" . $value->id . "'>" . $value->name . "</option>";
        }
        echo $data;
    }

    public function add_ajax_district($id_kab)
    {
        $this->auth();
        $query = $this->db->get_where('district', array('city_id' => $id_kab));
        $data = "<option value=''>- Pilih Kecamatan -</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='" . $value->id . "'>" . $value->name . "</option>";
        }
        echo $data;
    }

    public function check_email()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;

        $check_users = $this->main_model->gda3p('users', 'email', $email);
        $check_members = $this->main_model->gda3p('members', 'email', $email);

        if (!$check_users && !$check_members) {
            $status = array('status' => 'success');
            $this->mylib->setJSON();
            echo json_encode($status);
        } else {
            $status = array('status' => 'error');
            $this->mylib->setJSON();
            echo json_encode($status);
        }

    }

    public function check_email_forgot()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;

        $check_members = $this->main_model->gda3p('members', 'email', $email);

        if ($check_members) {
            $status = array('status' => 'success');
            $this->mylib->setJSON();
            echo json_encode($status);
        } else {
            $status = array('status' => 'error');
            $this->mylib->setJSON();
            echo json_encode($status);
        }

    }

    public function register()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['name'] = $obj->name;
        $data['email'] = $obj->email;
        $data['password'] = md5($obj->password);
        $data['activate'] = 0;
        $data['role'] = 2;
        $data['token'] = $this->mylib->random_string(40);

        $message = 'Berikut ini adalah link aktivasi akun anda : ' . base_url() . 'members_activation/' . $data['token'];
        $subject = 'Link aktivasi member';

        $store = $this->main_model->store('members', $data);

        if ($store) {
            $id = $this->main_model->gdo4p('members', 'id', 'email', $data['email']);
            $cart = $this->cart->contents();
            foreach ($cart as $key => $value) {
                $check = $this->main_model->gda5p('cart', 'product_id', $value['id'], 'member_id', $id);
                if (!$check) {
                    $data_cart['product_id'] = $value['product_id'];
                    $data_cart['member_id'] = $id;
                    $data_cart['name'] = $value['name'];
                    $data_cart['image'] = $value['image'];
                    $data_cart['condition'] = $value['condition'];
                    $data_cart['price'] = $value['price'];
                    $data_cart['qty'] = $value['qty'];
                    $data_cart['subtotal'] = $value['subtotal'];
                    $data_cart['buyer_notes'] = $value['buyer_notes'];
                    $store = $this->main_model->store('cart', $data_cart);
                } else {
                    $data_cart['qty'] = $check[0]['qty'] + $value['qty'];
                    $data_cart['subtotal'] = $value['price'] * $data_cart['qty'];
                    $data_cart['buyer_notes'] = $value['buyer_notes'];
                    $update = $this->main_model->update('cart', $data_cart, 'id', $value['id']);
                }
            }
            $cart = $this->cart->destroy();

            $send_mail = $this->send_mail($data['email'], $message, $subject);

            if ($send_mail) {
                $status = array('status' => 'success');
                $this->mylib->setJSON();
                echo json_encode($status);
            } else {
                $status = array('status' => 'error');
                $this->mylib->setJSON();
                echo json_encode($status);
            }

        }

    }

    public function send_token()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['email'] = $obj->email;
        $data['token'] = $this->mylib->random_string(40);

        $message = 'Berikut ini adalah link reset password akun anda : ' . base_url() . 'reset_password/' . $data['token'];
        $subject = 'Link reset password';

        $send_mail = $this->send_mail($data['email'], $message, $subject);

        if ($send_mail) {
            $store = $this->main_model->store('token_password', $data);
            $status = array('status' => 'success');
            $this->mylib->setJSON();
            echo json_encode($status);
        } else {
            $status = array('status' => 'error');
            $this->mylib->setJSON();
            echo json_encode($status);
        }

    }

    public function members_activation($token)
    {
        $check_token = $this->main_model->gda3p('members', 'token', $token);
        if ($check_token) {
            $data['activate'] = 1;
            $data['token'] = '';
            $update = $this->main_model->update('members', $data, 'id', $check_token[0]['id']);
            //Login
            $session_array = array();
            foreach ($check_token as $key => $val) {
                $session_array = array(
                    'id' => $val['id'],
                    'name' => $val['name'],
                    'email' => $val['email'],
                    'role' => $val['role'],
                );
            }
            $this->session->set_userdata('com_shop', $session_array);
            redirect(base_url());
        }
    }

    public function signin_proccess()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;
        $password = md5($obj->password);

        $members = $this->main_model->gda5p('members', 'email', $email, 'password', $password);

        if ($members) {
            $check_activation = $this->main_model->gdo4p('members', 'activate', 'email', $email);
            if ($check_activation == 1) {
                $session_array = array();
                foreach ($members as $key => $val) {
                    $session_array = array(
                        'id' => $val['id'],
                        'name' => $val['name'],
                        'email' => $val['email'],
                        'role' => $val['role'],
                    );
                }

                $cart = $this->cart->contents();
                foreach ($cart as $key => $value) {
                    $check = $this->main_model->gda5p('cart', 'product_id', $value['id'], 'member_id', $members[0]['id']);
                    if (!$check) {
                        $data_cart['product_id'] = $value['id'];
                        $data_cart['member_id'] = $members[0]['id'];
                        $data_cart['name'] = $value['name'];
                        $data_cart['image'] = $value['image'];
                        $data_cart['condition'] = $value['condition'];
                        $data_cart['price'] = $value['price'];
                        $data_cart['qty'] = $value['qty'];
                        $data_cart['subtotal'] = $value['subtotal'];
                        $data_cart['buyer_notes'] = $value['buyer_notes'];
                        $store = $this->main_model->store('cart', $data_cart);
                    } else {
                        $data_cart['qty'] = $check[0]['qty'] + $value['qty'];
                        $data_cart['subtotal'] = $value['price'] * $data_cart['qty'];
                        $data_cart['buyer_notes'] = $value['buyer_notes'];
                        $update = $this->main_model->update('cart', $data_cart, 'id', $check[0]['id']);
                    }
                }

                $cart = $this->cart->destroy();

                $this->session->set_userdata('com_shop', $session_array);
                $status = array('status' => 'success');
                $this->mylib->setJSON();
                echo json_encode($status);
            } else {
                $status = array('status' => 'notactive');
                $this->mylib->setJSON();
                echo json_encode($status);
            }

        } else {
            $status = array('status' => 'errors');
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('com_shop');
    }

    public function send_mail($email, $message, $subject)
    {
        $setting = $this->main_model->gda1p('setting');
        //Load email library
        $this->load->library('email');

        //SMTP & mail configuration
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.hostinger.co.id',
            'smtp_port' => 587,
            'smtp_user' => $setting[0]['send_mail'],
            'smtp_pass' => $setting[0]['send_pass'],
            'mailtype' => 'html',
            'charset' => 'utf-8',
        );
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        $this->email->to($email);
        $this->email->from($setting[0]['send_mail'], 'Admin Rumah Tiedye');
        $this->email->subject($subject);
        $this->email->message($message);

        //Send email
        $status = $this->email->send();

        return $status;
    }

    public function detail()
    {
        $this->auth();
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;

        $data = $this->main_model->gda3p($table, 'id', $id);

        if ($data) {
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $data);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function detail_address()
    {
        $this->auth();
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;

        $data = $this->main_model->gda3p($table, 'id', $id);
        $data[0]['province'] = $this->main_model->gdo4p('province', 'name', 'id', $data[0]['province_id']);
        $data[0]['city'] = $this->main_model->gdo4p('city', 'name', 'id', $data[0]['city_id']);
        $data[0]['district'] = $this->main_model->gdo4p('district', 'name', 'id', $data[0]['district_id']);

        if ($data) {
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $data);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function save_p_o_pay()
    {
        $id = $this->input->post('id');
        $config['upload_path'] = "./assets/payments";
        $config['allowed_types'] = 'jpg|png|jpeg';

        $file_name = $_FILES["file"]["name"];
        $file_ext = pathinfo($file_name, PATHINFO_EXTENSION);

        $new_name = $this->mylib->random_string(30) . '.' . $file_ext;

        $config['file_name'] = $new_name;

        $this->load->library('upload', $config);
        $this->load->library('image_lib');

        $this->upload->do_upload("file");

        $upload = $this->upload->data();

        if ($upload) {
            $configer = array(
                'image_library' => 'gd2',
                'source_image' => $upload['full_path'],
                'create_thumb' => false,
                'maintain_ratio' => true,
                'quality' => '100%',
                'width' => 300,
                'height' => 300,
            );
            $this->image_lib->clear();
            $this->image_lib->initialize($configer);
            $this->image_lib->resize();

            $data['p_o_pay'] = $new_name;
            $data['created_at'] = date('Y-m-d H:i:s');

            $getFileName = $this->main_model->gda3p('billing', 'id', $id);

            $update = $this->main_model->update('billing', $data, 'id', $id);

            if ($update) {
                if ($getFileName[0]['image'] != '') {
                    unlink("./assets/payments/" . $getFileName[0]['image']);
                }
                echo "Success";
            }

        }

    }

    public function destroy($table, $column)
    {
        $this->auth(0);
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;

        if ($_SESSION['com_shop']['role'] == 2) {

            if ($table == 'address') {

                $delete = $this->main_model->destroy($table, $column, $id);

                if ($delete) {
                    $status = array('status' => 'success');
                    $this->mylib->setJSON();
                    echo json_encode($status);
                }
            }

        } else {
            $status = array('status' => 'error');
            $this->mylib->setJSON();
            echo json_encode($status);
        }

    }

    public function check_billing()
    {
        $bill = $this->main_model->gda3p('billing', 'member_id', $_SESSION['com_shop']['id']);
        foreach ($bill as $key => $value) {
            if ($value['expired'] < date('Y-m-d H:i:s') && $value['p_o_pay'] == '') {
                $this->main_model->destroy('billing', 'id', $value['id']);
            }
        }
    }

    public function check_voucher(){
        $voucher = $this->main_model->gda3p('reward_list', 'status', 'true');
        $purchase = $this->main_model->countwhere('purchasement', 'id', 'member_id', $_SESSION['com_shop']['id']);
        
        foreach ($voucher as $key => $value) {
            if($purchase >= $value['transaction']){
                $check_voucher = $this->main_model->gda5p('reward', 'voucher', $value['voucher_code'], 'member_id', $_SESSION['com_shop']['id']);
                if(!$check_voucher){
                    $data['member_id'] = $_SESSION['com_shop']['id'];
                    $data['voucher'] = $value['voucher_code'];
                    $data['discount'] = $value['discount'];
                    $data['description'] = $value['description'];

                    $data['expired'] = $value['expired'];
                    $day =  $data['expired'] * 24;
                    $validity = '+'.$day.' hour';
                    $startTime = date('Y-m-d H:i:s');
                    $cenvertedTime = date('Y-m-d H:i:s', strtotime($validity, strtotime($startTime)));

                    $data['validity'] = $cenvertedTime;
                    $data['status'] = 'tersedia';

                    $this->main_model->store('reward', $data);
                }
                
            }
        }
    }

    public function check_reward(){
        
        $voucher_list = $this->main_model->gda1p('reward_list');
        $now = date('Y-m-d H:i:s');
        foreach ($voucher_list as $key => $vl) {
            if($vl['validity'] < $now){
                $reward_list['status'] = 'false';
                $this->main_model->update('reward_list', $reward_list, 'id', $vl['id']);
            }
        }

        $voucher = $this->main_model->gda1p('reward');
        foreach ($voucher as $key => $v) {
            if($v['validity'] < $now){
                $reward['status'] = 'kadaluarsa';
                $this->main_model->update('reward', $reward, 'id', $v['id']);
            }
        }

        $this->check_voucher();
    }

    public function change_profile($id)
    {
        $data['members'] = $this->main_model->gda3p('members', 'id', $id);
        $this->load->view('shop/content/change_profile', $data);
    }

    public function check_email_change()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $email = $obj->email;

        $check_old = $this->main_model->gdo4p('members', 'email', 'id', $id);

        if ($email == $check_old) {
            $status = array('status' => 'success');
            $this->mylib->setJSON();
            echo json_encode($status);
        } else {
            $check_users = $this->main_model->gda3p('users', 'email', $email);
            $check_members = $this->main_model->gda3p('members', 'email', $email);

            if (!$check_users && !$check_members) {
                $status = array('status' => 'success');
                $this->mylib->setJSON();
                echo json_encode($status);
            } else {
                $status = array('status' => 'error');
                $this->mylib->setJSON();
                echo json_encode($status);
            }
        }
    }

    public function save_profile()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $data['name'] = $obj->name;
        $data['email'] = $obj->email;
        if ($obj->password != '') {
            $data['password'] = md5($obj->password);
        }

        $update = $this->main_model->update('members', $data, 'id', $id);

        if ($update) {
            $status = array('status' => 'success', 'status_code' => 200, 'data' => $update);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function forgot_password()
    {
        $this->load->view('shop/forgot_password');
    }

    public function reset_password($token){
        $check_token = $this->main_model->gda3p('token_password', 'token', $token);
        if($check_token){
            $data['email'] = $check_token[0]['email'];
            $this->load->view('shop/reset_password', $data); 
        }else{
            print('Page Not Found');
        }
    }

    public function new_password()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;
        $data['password'] = md5($obj->password);
        
        $update = $this->main_model->update('members', $data, 'email', $email);

        if ($update) {
            $this->main_model->destroy('token_password', 'email', $email);
            $status = array('status' => 'success', 'status_code' => 200, 'data' => $update);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }
}
