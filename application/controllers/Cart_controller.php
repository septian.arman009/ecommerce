<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cart_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        $this->load->library('cart');
    }

    public function check_cart()
    {
        if ($this->session->userdata('com_shop')) {
            if ($_SESSION['com_shop']['role'] == '2') {
                $check_cart = $this->main_model->gda3p('cart', 'member_id', $_SESSION['com_shop']['id']);
                if ($check_cart) {
                    $total_items = count($check_cart);
                    $total = 0;
                    foreach ($check_cart as $key => $value) {
                        if ($total == 0) {
                            $total = $value['subtotal'];
                        } else {
                            $total += $value['subtotal'];
                        }
                    }

                    $cart['total_items'] = $total_items;
                    $cart['subtotal'] = $this->mylib->torp($total);

                    $status = array('status' => 'success', 'status_code' => '200', 'data' => $cart);
                    $this->mylib->setJSON();
                    echo json_encode($status);

                } else {
                    $cart_content = $this->cart->contents();
                    if ($cart_content) {

                        foreach ($cart_content as $key => $value) {
                            $check = $this->main_model->gda5p('cart', 'id', $value['id'], 'member_id', $_SESSION['com_shop']['id']);
                            if (!$check) {
                                $data_cart['product_id'] = $value['product_id'];
                                $data_cart['member_id'] = $_SESSION['com_shop']['id'];
                                $data_cart['name'] = $value['name'];
                                $data_cart['image'] = $value['image'];
                                $data_cart['condition'] = $value['condition'];
                                $data_cart['price'] = $value['price'];
                                $data_cart['qty'] = $value['qty'];
                                $data_cart['subtotal'] = $value['subtotal'];
                                $data_cart['buyer_notes'] = $value['buyer_notes'];
                                $store = $this->main_model->store('cart', $data_cart);
                            } else {
                                $data_cart['qty'] = $check[0]['qty'] + $value['qty'];
                                $data_cart['subtotal'] = $check[0]['price'] * $data_cart['qty'];
                                $update = $this->main_model->update('cart', $data_cart, 'id', $value['id']);
                            }
                        }

                        $check_cart = $this->main_model->gda3p('cart', 'member_id', $_SESSION['com_shop']['id']);

                        $total_items = count($check_cart);
                        $total = 0;
                        foreach ($check_cart as $key => $value) {
                            if ($total == 0) {
                                $total = $value['subtotal'];
                            } else {
                                $total += $value['subtotal'];
                            }
                        }

                        $cart['total_items'] = $total_items;
                        $cart['subtotal'] = $this->mylib->torp($total);

                        $cart = $this->cart->destroy();

                        $status = array('status' => 'success', 'status_code' => '200', 'data' => $cart);
                        $this->mylib->setJSON();
                        echo json_encode($status);
                    } else {
                        $status = array('status' => 'error');
                        $this->mylib->setJSON();
                        echo json_encode($status);
                    }

                }
            } else {
                $cart_content = $this->cart->contents();

                if ($cart_content) {
                    $total_items = count($cart_content);
                    $total = 0;
                    foreach ($cart_content as $key => $value) {
                        if ($total == 0) {
                            $total = $value['subtotal'];
                        } else {
                            $total += $value['subtotal'];
                        }
                    }

                    $cart['total_items'] = $total_items;
                    $cart['subtotal'] = $this->mylib->torp($total);

                    $status = array('status' => 'success', 'status_code' => '200', 'data' => $cart);
                    $this->mylib->setJSON();
                    echo json_encode($status);
                } else {
                    $status = array('status' => 'error');
                    $this->mylib->setJSON();
                    echo json_encode($status);
                }
            }
        } else {
            $cart_content = $this->cart->contents();

            if ($cart_content) {
                $total_items = count($cart_content);
                $total = 0;
                foreach ($cart_content as $key => $value) {
                    if ($total == 0) {
                        $total = $value['subtotal'];
                    } else {
                        $total += $value['subtotal'];
                    }
                }

                $cart['total_items'] = $total_items;
                $cart['subtotal'] = $this->mylib->torp($total);

                $status = array('status' => 'success', 'status_code' => '200', 'data' => $cart);
                $this->mylib->setJSON();
                echo json_encode($status);
            } else {
                $status = array('status' => 'error');
                $this->mylib->setJSON();
                echo json_encode($status);
            }
        }

    }

    public function add_to_cart($id)
    {
        $products = $this->main_model->gda3p('products', 'id', $id);

        $insert_data = array(
            'id' => $products[0]['id'],
            'name' => $products[0]['product_name'],
            'image' => $products[0]['image'],
            'condition' => $products[0]['condition'],
            'member_id' => $_SESSION['com_shop']['id'],
            'price' => $products[0]['unit_price'],
            'qty' => 1,
            'subtotal' => $products[0]['unit_price'],
            'buyer_notes' => '');

        if ($this->session->userdata('com_shop')) {
            if ($_SESSION['com_shop']['role'] == 2) {
                $insert_data_login = array(
                    'product_id' => $products[0]['id'],
                    'name' => $products[0]['product_name'],
                    'image' => $products[0]['image'],
                    'condition' => $products[0]['condition'],
                    'member_id' => $_SESSION['com_shop']['id'],
                    'price' => $products[0]['unit_price'],
                    'qty' => 1,
                    'subtotal' => $products[0]['unit_price'],
                    'buyer_notes' => '');

                $check_cart = $this->main_model->gda5p('cart', 'product_id', $products[0]['id'], 'member_id', $_SESSION['com_shop']['id']);

                if ($check_cart) {
                    $data_cart['qty'] = $check_cart[0]['qty'] + 1;
                    $data_cart['subtotal'] = $data_cart['qty'] * $products[0]['unit_price'];
                    $this->main_model->update('cart', $data_cart, 'id', $check_cart[0]['id']);
                } else {
                    $this->main_model->store('cart', $insert_data_login);
                }
            } else {
                $insert_cart = $this->cart->insert($insert_data);
            }
        } else {
            $insert_cart = $this->cart->insert($insert_data);
        }

    }

    public function add_to_cart_qty($id, $qty)
    {

        $products = $this->main_model->gda3p('products', 'id', $id);

        $insert_data = array(
            'id' => $products[0]['id'],
            'name' => $products[0]['product_name'],
            'image' => $products[0]['image'],
            'condition' => $products[0]['condition'],
            'member_id' => $_SESSION['com_shop']['id'],
            'price' => $products[0]['unit_price'],
            'qty' => $qty,
            'subtotal' => ($products[0]['unit_price'] * $qty),
            'buyer_notes' => '');

        if ($this->session->userdata('com_shop')) {
            if ($_SESSION['com_shop']['role'] == 2) {

                $check_cart = $this->main_model->gda5p('cart', 'product_id', $products[0]['id'], 'member_id', $_SESSION['com_shop']['id']);

                if ($check_cart) {
                    $data_cart['qty'] = $check_cart[0]['qty'] + $qty;
                    $data_cart['subtotal'] = $data_cart['qty'] * $products[0]['unit_price'];
                    $this->main_model->update('cart', $data_cart, 'id', $check_cart[0]['id']);
                } else {
                    $this->main_model->store('cart', $insert_data);
                }
            } else {
                $insert_cart = $this->cart->insert($insert_data);
            }
        } else {
            $insert_cart = $this->cart->insert($insert_data);
        }

    }

    public function detail_cart()
    {
        if ($this->session->userdata('com_shop')) {
            if ($_SESSION['com_shop']['role'] == 2) {
                $data['cart_session'] = 'member';
                $data['cart'] = $this->main_model->gda3p('cart', 'member_id', $_SESSION['com_shop']['id']);
                $total = 0;
                foreach ($data['cart'] as $key => $value) {
                    $data['stock'] = $this->main_model->gdo4p('products', 'stock', 'id', $value['product_id']);
                    if ($value['qty'] <= $data['stock']) {
                        $total++;
                    }
                }
            } else {
                $data['cart_session'] = 'guest';
                $data['cart'] = $this->cart->contents();
                $total = 0;
                foreach ($data['cart'] as $key => $value) {
                    $data['stock'] = $this->main_model->gdo4p('products', 'stock', 'id', $value['id']);
                    if ($value['qty'] <= $data['stock']) {
                        $total++;
                    }
                }
            }
        } else {
            $data['cart_session'] = 'guest';
            $data['cart'] = $this->cart->contents();
            $total = 0;
            foreach ($data['cart'] as $key => $value) {
                $data['stock'] = $this->main_model->gdo4p('products', 'stock', 'id', $value['id']);
                if ($value['qty'] <= $data['stock']) {
                    $total++;
                }
            }
        }

        $data['total_row'] = $total;
        $this->load->view('shop/content/detail_cart', $data);
    }

    public function update_cart()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $qty = $obj->qty;
        $product_id = $obj->product_id;
        $notes = $obj->notes;

        $stock = $this->main_model->gdo4p('products', 'stock', 'id', $product_id);

        if ($qty <= $stock) {
            if ($this->session->userdata('com_shop')) {
                if ($_SESSION['com_shop']['role'] == 2) {
                    $price = $this->main_model->gdo4p('cart', 'price', 'id', $id);
                    $data['qty'] = $qty;
                    $data['subtotal'] = $price * $qty;
                    $data['buyer_notes'] = $notes;

                    $this->main_model->update('cart', $data, 'id', $id);
                } else {
                    $data = array(
                        'rowid' => $id,
                        'qty' => $qty,
                        'buyer_notes' => $notes,
                    );

                    $this->cart->update($data);
                }
            } else {
                $data = array(
                    'rowid' => $id,
                    'qty' => $qty,
                    'buyer_notes' => $notes,
                );

                $this->cart->update($data);
            }
            $status = array('status' => 'success');
            $this->mylib->setJSON();
            echo json_encode($status);
        } else {
            $status = array('status' => 'error', 'data' => $stock);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function check_cart_qty()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $qty = $obj->qty;
        $product_id = $obj->product_id;

        $stock = $this->main_model->gdo4p('products', 'stock', 'id', $product_id);

        if ($qty <= $stock) {
            $status = array('status' => 'success');
            $this->mylib->setJSON();
            echo json_encode($status);
        } else {
            $status = array('status' => 'error', 'data' => $stock);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function del_cart()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;

        if ($this->session->userdata('com_shop')) {
            $delete = $this->main_model->destroy2where('cart', 'id', $id, 'member_id', $_SESSION['com_shop']['id']);
        } else {
            $data = array(
                'rowid' => $id,
                'qty' => 0,
            );

            $this->cart->update($data);
        }

        $status = array('status' => 'success');
        $this->mylib->setJSON();
        echo json_encode($status);

    }

    public function check_voucher()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $voucher = $obj->code;
        
        $data_voucher = $this->main_model->gda3p('reward', 'voucher', $voucher);
        if($data_voucher){
            if($data_voucher[0]['status'] == 'kadaluarsa'){
                $status = array('status' => 'unregister');
                $this->mylib->setJSON();
                echo json_encode($status);
            }else if($data_voucher[0]['status'] == 'terpakai'){
                $status = array('status' => 'used');
                $this->mylib->setJSON();
                echo json_encode($status);
            }else if($data_voucher[0]['status'] == 'tersedia'){
                $status = array('status' => 'valid', 'discount' => $data_voucher[0]['discount']);
                $this->mylib->setJSON();
                echo json_encode($status);
            }
        }else{
            $status = array('status' => 'unregister');
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    
    }

}
