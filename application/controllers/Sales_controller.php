<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sales_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        $this->load->model('d_table');
        $this->perPage = 10;
        $this->auth();
    }

    public function auth()
    {
        if ($this->session->userdata('com_in')) {
            return true;
        } else {
            redirect('signin');
        }
    }

    public function billing()
    {
        $this->load->view('content/admin/sales/billing');
    }

    public function billing_detail($id)
    {
        $billing = $this->main_model->gda3p('billing', 'id', $id);
        $data['discount'] = $billing[0]['discount'];
        $data['discount_ammount'] = $billing[0]['discount_ammount'];
        $data['billing_transfer'] = $billing[0]['billing_transfer'];
        foreach ($billing as $key => $value) {
            $data['billing_inv'] = $value['billing_inv'];
            $data['product_id'] = unserialize($value['product_id']);
            $data['product_qty'] = unserialize($value['product_qty']);
            $data['product_price'] = unserialize($value['product_price']);
            $data['product_subtotal'] = unserialize($value['product_subtotal']);
            $data['product_image'] = unserialize($value['product_image']);
            $data['buyer_notes'] = unserialize($value['buyer_notes']);
            $data['billing_transfer'] = $value['billing_transfer'];
            $data['address'] = $this->main_model->gda3p('address', 'id', $value['address_id']);
            $data['receiver_bank'] = $this->main_model->gda3p('bank', 'id', $value['bank_id']);
            $data['p_o_pay'] = $value['p_o_pay'];
        }

        foreach ($data['product_id'] as $key => $value) {
            $data['name'][] = $this->main_model->gdo4p('products', 'product_name', 'id', $value);
        }

        $data['id'] = $id;
        $data['billing_inv'] = $billing[0]['billing_inv'];

        $data['province'] = $this->main_model->gdo4p('province', 'name', 'id', $data['address'][0]['province_id']);
        $data['city'] = $this->main_model->gdo4p('city', 'name', 'id', $data['address'][0]['city_id']);
        $data['district'] = $this->main_model->gdo4p('district', 'name', 'id', $data['address'][0]['district_id']);

        $this->load->view('content/admin/sales/billing_detail', $data);
    }

    public function purchasement()
    {
        $this->load->view('content/admin/sales/purchasement');
    }

    public function purchasement_detail($id)
    {
        $purchasement = $this->main_model->gda3p('purchasement', 'id', $id);
        $data['discount'] = $purchasement[0]['discount'];
        $data['discount_ammount'] = $purchasement[0]['discount_ammount'];
        $data['transfer_ammount'] = $purchasement[0]['transfer_ammount'];
        foreach ($purchasement as $key => $value) {
            $data['purchasement_inv'] = $value['purchasement_inv'];
            $data['product_id'] = unserialize($value['product_id']);
            $data['product_qty'] = unserialize($value['product_qty']);
            $data['product_price'] = unserialize($value['product_price']);
            $data['product_subtotal'] = unserialize($value['product_subtotal']);
            $data['product_image'] = unserialize($value['product_image']);
            $data['buyer_notes'] = unserialize($value['buyer_notes']);
            $data['transfer_ammount'] = $value['transfer_ammount'];
            $data['address'] = unserialize($value['address']);
            $data['bank'] = unserialize($value['bank']);
            $data['p_o_pay'] = $value['p_o_pay'];
        }

        foreach ($data['product_id'] as $key => $value) {
            $data['name'][] = $this->main_model->gdo4p('products', 'product_name', 'id', $value);
        }

        $data['id'] = $id;
        $data['purchasement_inv'] = $purchasement[0]['purchasement_inv'];

        $data['province'] = $this->main_model->gdo4p('province', 'name', 'id', $data['address'][0]['province_id']);
        $data['city'] = $this->main_model->gdo4p('city', 'name', 'id', $data['address'][0]['city_id']);
        $data['district'] = $this->main_model->gdo4p('district', 'name', 'id', $data['address'][0]['district_id']);

        $this->load->view('content/admin/sales/purchasement_detail', $data);
    }

    public function show_billing()
    {
        if (
            isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ) {

            $datatables = $_POST;
            $datatables['d'] = 'detail';
            $datatables['table'] = 'billing';
            $datatables['id-table'] = 'id';
            $datatables['col-display'] = array(
                'id',
                'billing_inv',
                'billing_transfer',
                'status',
                'expired',
            );

            $this->d_table->Datatables($datatables);
        }
        return;
    }

    public function show_purchasement()
    {
        if (
            isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ) {

            $datatables = $_POST;
            $datatables['d'] = 'detail';
            $datatables['table'] = 'purchasement';
            $datatables['id-table'] = 'id';
            $datatables['col-display'] = array(
                'id',
                'purchasement_inv',
                'transfer_ammount',
                'status',
            );

            $this->d_table->Datatables($datatables);
        }
        return;
    }

    public function purchase_confirmation()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;

        $billing = $this->main_model->gda3p('billing', 'id', $id);
        foreach ($billing as $key => $value) {
            $data['member_id'] = $value['member_id'];
            $data['purchasement_inv'] = $this->generate_inv();
            $data['product_id'] = $value['product_id'];
            $data['product_qty'] = $value['product_qty'];
            $data['product_price'] = $value['product_price'];
            $data['product_subtotal'] = $value['product_subtotal'];
            $data['product_image'] = $value['product_image'];
            $data['buyer_notes'] = $value['buyer_notes'];
            $data['transfer_ammount'] = $value['billing_transfer'];
            $data['address'] = serialize($this->main_model->gda3p('address', 'id', $value['address_id']));
            $data['bank'] = serialize($this->main_model->gda3p('bank', 'id', $value['bank_id']));
            $data['p_o_pay'] = $value['p_o_pay'];
            $data['status'] = 'purchased';
            $data['created_at'] = $value['created_at'];
            $data['discount'] = $value['discount'];
            $data['discount_ammount'] = $value['discount_ammount'];

            $qty = unserialize($value['product_qty']);
            $total_qty = 0;
            foreach ($qty as $key => $value) {
                $total_qty += $value;
            }

            $data['qty_count'] = $total_qty;
        }

        $store = $this->main_model->store('purchasement', $data);
        if ($store) {
            $activity['user_id'] = $_SESSION['com_in']['id'];
            $activity['name'] = $_SESSION['com_in']['name'];
            $activity['activity'] = 'Mengkonfirmasi Pembayaran dengan nomor invoice : '. $data['purchasement_inv'] .' pada '. date('d/m/Y H:i:s');

            $this->main_model->store('log_activity', $activity);
            $this->main_model->destroy('billing', 'id', $id);
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $store);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function save_resi()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $data['receipt_number'] = $obj->resi;
        $data['status'] = 'sent';

        $inv = $this->main_model->gdo4p('purchasement','purchasement_inv','id',$id);
        $store = $this->main_model->update('purchasement', $data, 'id', $id);
        if ($store) {
            $activity['user_id'] = $_SESSION['com_in']['id'];
            $activity['name'] = $_SESSION['com_in']['name'];
            $activity['activity'] = 'Menambahkan Resi : '.$data['receipt_number'].' untuk transaksi : '. $inv .' pada '. date('d/m/Y H:i:s');

            $this->main_model->store('log_activity', $activity);
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $store);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function generate_inv()
    {
        $date = explode('-', date('y-m-d'));
        $max = $this->main_model->max('purchasement', 'purchasement_inv');
        if ($max) {
            $inv = explode('/', $max);
            $id = $inv[3] + 1;
            return 'INV/P/' . $date[0] . '/' . $id;
        } else {
            return 'INV/P/' . $date[0] . '/1';
        }
    }

    public function track_order($page, $key_pass)
    {
        $total_row = $this->main_model->count('purchasement', 'id');
        $data['all'] = ceil($total_row / 10);

        $key = str_replace('-', '/', $key_pass);

        if ($page != 1) {

            $end = $this->perPage * $page;
            $start = $end - 10;
            $data['page'] = $page;

            if ($start > 0) {
                if ($key != 'null') {

                    $data['order'] = $this->main_model->limitWhere($start, $end, 'purchasement', 'purchasement_inv', $key);
                } else {
                    $data['order'] = $this->main_model->limit($start, $end, 'purchasement');
                }
            }

        } else {
            $data['page'] = $page;

            if ($key != 'null') {
                $data['order'] = $this->main_model->limitWhere(0, $this->perPage, 'purchasement', 'purchasement_inv', $key);
            } else {
                $data['order'] = $this->main_model->limit(0, $this->perPage, 'purchasement');
            }
        }

        $data['key_pass'] = $key;
        $this->load->view('content/admin/sales/track_order', $data);
    }

    public function delivered()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $data['status'] = 'delivered';
        $data['satisfaction'] = 'unreview';

        $member_id = $this->main_model->gdo4p('purchasement', 'member_id', 'id', $id);
        $purchase = $this->main_model->countwhere('purchasement', 'id', 'member_id', $member_id);

        if($purchase == 1){
            $reward['member_id'] = $member_id;
            $reward['discount'] = 10;

            $reward['expired'] = 365;
            $day = $reward['expired'] * 24;
            $validity = '+'.$day.' hour';
            $startTime = date("Y-m-d H:i:s");
            $cenvertedTime = date('Y-m-d H:i:s', strtotime($validity, strtotime($startTime)));
            
            $reward['voucher'] = 'BELANJAPERTAMA';
            $reward['validity'] = $cenvertedTime;
            $reward['description'] = 'Rewar transaki pertama kamu :)';
            $reward['status'] = 'tersedia';
            
            $this->main_model->store('reward', $reward);
        }

        $purchased = $this->main_model->gda3p('purchasement', 'id', $id);
        $delivered = $this->main_model->update('purchasement', $data, 'id', $id);

        if ($delivered) {
            $activity['user_id'] = $_SESSION['com_in']['id'];
            $activity['name'] = $_SESSION['com_in']['name'];
            $activity['activity'] = 'Mengubah Status Resi : '.$purchased[0]['receipt_number'].' untuk transaksi : '. $purchased[0]['purchasement_inv'] .' pada '. date('d/m/Y H:i:s');

            $this->main_model->store('log_activity', $activity);
            $status = array('status' => 'success');
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

}
