<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        $this->load->model('d_table');
        $this->auth();
    }

    public function auth()
    {
        if ($this->session->userdata('com_in')) {
            return true;
        } else {
            redirect('signin');
        }
    }

    public function monthly($month, $year)
    {
        // $data['month'] = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $sales = $this->main_model->monthly($month, $year);
        if ($sales) {
            foreach ($sales as $key => $value) {
                $data['daily'][] = $this->mylib->to_date($value['created_at']) . ' (' . $value['total_row'] . ') ' . $this->mylib->torp($value['total_ammount']);
            }
        } else {
            $data['daily'] = array();
        }

        $data['month'] = $month;
        $data['year'] = $year;

        $this->load->view('content/admin/report/monthly', $data);
    }

    public function g_monthly($month, $year)
    {
        $sales = $this->main_model->monthly($month, $year);

        foreach ($sales as $key => $value) {
            $row['data'][] = $value['total_ammount'];
        }

        $row['name'] = 'Total Pembayaran Masuk';
        $result = array();
        array_push($result, $row);
        print json_encode($result, JSON_NUMERIC_CHECK);
    }

    public function yearly($year)
    {
        $month = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

        $sales = $this->main_model->yearly($year);

        if ($sales) {
            foreach ($month as $key => $vm) {
                foreach ($sales as $key => $vs) {
                    if ($this->mylib->to_month($vs['month']) == $vm) {
                        $data['month'][] = $vm . ' (' . $vs['total_row'] . ') ' . $this->mylib->torp($vs['total_ammount']);
                        break;
                    } else {
                        $data['month'][] = $vm;
                    }
                }
            }
        } else {
            $data['month'] = array();
        }

        $data['year'] = $year;

        $this->load->view('content/admin/report/yearly', $data);
    }

    public function g_yearly($year)
    {
        $month = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

        $sales = $this->main_model->yearly($year);

        foreach ($month as $key => $vm) {
            foreach ($sales as $key => $vs) {
                if ($this->mylib->to_month($vs['month']) == $vm) {
                    $row['data'][] = $vs['total_ammount'];
                    break;
                } else {
                    $row['data'][] = 0;
                }
            }
        }

        $row['name'] = 'Total Pembayaran Masuk';
        $result = array();
        array_push($result, $row);
        print json_encode($result, JSON_NUMERIC_CHECK);
    }

    public function print_yearly($year)
    {
        $month = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

        $sales = $this->main_model->yearly($year);

        $total = 0;
        if ($sales) {
            foreach ($month as $key => $vm) {
                foreach ($sales as $key => $vs) {
                    if ($this->mylib->to_month($vs['month']) == $vm) {
                        $data['month'][] = $vm;
                        $data['transaction'][] = $vs['total_row'];
                        $data['qty'][] = $vs['qty'];
                        $data['total_ammount'][] = $vs['total_ammount'];
                        $total += $vs['total_ammount'];
                        break;
                    } else {
                        $data['month'][] = $vm;
                        $data['transaction'][] = 0;
                        $data['qty'][] = 0;
                        $data['total_ammount'][] = 0;
                        break;
                    }
                }
            }
        } else {
            foreach ($month as $key => $vm) {

                $data['month'][] = $vm;
                $data['transaction'][] = 0;
                $data['qty'][] = 0;
                $data['total_ammount'][] = 0;

            }
        }

        $data['year'] = $year;

        $this->load->library('fpdf');
        $this->load->view('content/admin/report/print_yearly', $data);
    }

    public function print_monthly($month, $year)
    {
        $data['sales'] = $this->main_model->monthly_all($month, $year);

        foreach ($data['sales'] as $key => $value) {
            $data['qty'][] = array_sum(unserialize($value['product_qty']));
        }

        $data['month'] = $month;
        $data['year'] = $year;

        $this->load->library('fpdf');
        $this->load->view('content/admin/report/print_monthly', $data);
    }

    public function print_daily($day, $month, $year)
    {
        $data['sales'] = $this->main_model->daily($day, $month, $year);

        foreach ($data['sales'] as $key => $value) {
            $data['qty'][] = array_sum(unserialize($value['product_qty']));
        }

        $data['day'] = $day;
        $data['month'] = $month;
        $data['year'] = $year;

        $this->load->library('fpdf');
        $this->load->view('content/admin/report/print_daily', $data);
    }
}
