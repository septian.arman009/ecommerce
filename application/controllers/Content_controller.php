<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Content_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        $this->load->model('d_table');
        $this->perPage = 6;
        $this->auth();
    }

    public function auth()
    {
        if ($this->session->userdata('com_in')) {
            return true;
        } else {
            redirect('signin');
        }
    }

    public function form($id, $page, $key, $categori)
    {   
        $data['id'] = $id;
        $data['page'] = $page;
        $data['key_pass'] = $key;
        $data['categori'] = $categori;
        $data['categories'] = $this->main_model->gda1p('categories');
        $this->load->view('content/admin/products/form', $data);
    }

    public function products($page, $key, $categories)
    {   

        $total_row = $this->main_model->count('products', 'id');
        $data['all'] = ceil($total_row / 6);

        if ($page != 1) {

            $end = $this->perPage * $page;
            $start = $end - 6;
            $data['page'] = $page;

            if ($start > 0) {
                if ($key != 'null' && $categories !='null') {
                    $data['products'] = $this->main_model->limit2Where($start, $end, 'products', 'product_name', $key, 'product_categories', $categories);
                } else if ($key != 'null' && $categories =='null') {
                    $data['products'] = $this->main_model->limitWhere($start, $end, 'products', 'product_name', $key);
                } else if ($key == 'null' && $categories !='null') {
                    $data['products'] = $this->main_model->limitWhereNotLike($start, $end, 'products', 'product_categories', $categories);
                } else {
                    $data['products'] = $this->main_model->limit($start, $end, 'products');
                }
            } 

        } else {
            $data['page'] = $page;

            if ($key != 'null' && $categories !='null') {
                $data['products'] = $this->main_model->limit2Where(0, $this->perPage, 'products', 'product_name', $key, 'product_categories', $categories);
            } else if ($key != 'null' && $categories =='null') {
                $data['products'] = $this->main_model->limitWhere(0, $this->perPage, 'products', 'product_name', $key);
            } else if ($key == 'null' && $categories !='null') {
                $data['products'] = $this->main_model->limitWhereNotLike(0, $this->perPage, 'products', 'product_categories', $categories);
            } else {
                $data['products'] = $this->main_model->limit(0, $this->perPage, 'products');
            }
        }

        $data['key_pass'] = $key;
        $data['categori'] = $categories;
        $data['categories'] = $this->main_model->gda1p('categories');
        $this->load->view('content/admin/products/products', $data);
    }

    public function save_product()
    {   
        $id = $this->input->post('id');
        $data['product_name'] = $this->input->post('product_name');
        $data['product_categories'] = $this->input->post('product_categories');
        $price = str_replace('Rp. ','',$this->input->post('unit_price'));
        $fix_price = str_replace('.','',$price);
        $data['unit_price'] = $fix_price;
        $data['weight'] = $this->input->post('weight');
        $data['stock'] = $this->input->post('stock');
        $data['condition'] = $this->input->post('condition');
        $data['description'] = $this->input->post('description');
        $data['created_by'] = $_SESSION['com_in']['id'];

        $data['color'] = $this->input->post('color');

        $data['size'] = $this->input->post('size');

        if (!empty($_FILES)) {
            $config['upload_path'] = "./assets/products";
            $config['allowed_types'] = 'jpg|png|jpeg';

            $file_name = $_FILES["file"]["name"];
            $file_ext = pathinfo($file_name, PATHINFO_EXTENSION);

            $new_name = $this->mylib->random_string(30) . '.' . $file_ext;

            $config['file_name'] = $new_name;

            $this->load->library('upload', $config);
            $this->load->library('image_lib');

            $this->upload->do_upload("file");

            $upload = $this->upload->data();

            if ($upload) {
                $configer = array(
                    'image_library' => 'gd2',
                    'source_image' => $upload['full_path'],
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'quality' => '100%',
                    'width' => 278,
                    'height' => 257,
                );
                $this->image_lib->clear();
                $this->image_lib->initialize($configer);
                $this->image_lib->resize();

                $data['image'] = $new_name;

                if($id == 'null'){
                    $insert = $this->main_model->store('products', $data);

                    if ($insert) {
                        $activity['user_id'] = $_SESSION['com_in']['id'];
                        $activity['name'] = $_SESSION['com_in']['name'];
                        $activity['activity'] = 'Menambahkan produk baru dengan Nama : '.$data['product_name'].' pada '. date('d/m/Y H:i:s');

                        $this->main_model->store('log_activity', $activity);
                        echo "Success";
                    }
                }else{
                    $getFileName = $this->main_model->gda3p('products', 'id', $id);
                    $update = $this->main_model->update('products', $data, 'id', $id);

                    if ($update) {
                        if ($getFileName[0]['image'] != '') {
                            unlink("./assets/products/" . $getFileName[0]['image']);
                        }
                        $activity['user_id'] = $_SESSION['com_in']['id'];
                        $activity['name'] = $_SESSION['com_in']['name'];
                        $activity['activity'] = 'Mengubah data produk dengan ID : '. $id.' dan Nama Produk: '.$data['product_name'].' pada '. date('d/m/Y H:i:s');

                        $this->main_model->store('log_activity', $activity);
                        echo "Success";
                    }
                }  

            } else {
                echo "File cannot be uploaded";
            }

        } else {
            if($id == 'null'){
                $insert = $this->main_model->store('products', $data);

                if ($insert) {
                    $activity['user_id'] = $_SESSION['com_in']['id'];
                    $activity['name'] = $_SESSION['com_in']['name'];
                    $activity['activity'] = 'Menambahkan produk baru dengan Nama : '.$data['product_name'].' pada '. date('d/m/Y H:i:s');

                    $this->main_model->store('log_activity', $activity);
                    echo "Success";
                }
            }else{
                $update = $this->main_model->update('products', $data, 'id', $id);

                if ($update) {
                    $activity['user_id'] = $_SESSION['com_in']['id'];
                    $activity['name'] = $_SESSION['com_in']['name'];
                    $activity['activity'] = 'Mengubah data produk dengan ID : '. $id.' dan Nama Produk: '.$data['product_name'].' pada '. date('d/m/Y H:i:s');

                    $this->main_model->store('log_activity', $activity);
                    echo "Success";
                }
            }
        
        }
    }

    public function categories()
    {   
        $data['categories'] = $this->main_model->gda1po('categories', 'id', 'desc');
        $this->load->view('content/admin/products/categories', $data);
    }

    public function save_categories($id)
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['categories'] = $obj->categories;

        if($id == 'null'){
            $save = $this->main_model->store('categories', $data);
            $activity['user_id'] = $_SESSION['com_in']['id'];
            $activity['name'] = $_SESSION['com_in']['name'];
            $activity['activity'] = 'Menambahkan Kategori : '.$data['categories'].' pada '. date('d/m/Y H:i:s');
            $this->main_model->store('log_activity', $activity);
        }else{
            $save = $this->main_model->update('categories', $data, 'id', $id);
            $activity['user_id'] = $_SESSION['com_in']['id'];
            $activity['name'] = $_SESSION['com_in']['name'];
            $activity['activity'] = 'Mengubah Kategori dengan ID: '.$id.' pada '. date('d/m/Y H:i:s');
            $this->main_model->store('log_activity', $activity);
        }

        if ($save) {
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $save);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function receiver_bank()
    {
        $this->load->view('content/admin/bank/receiver_bank');
    }

    public function bank_form($id)
    {   
        $data['id'] = $id;
        $this->load->view('content/admin/bank/bank_form', $data);
    }

    public function save_bank()
    {
        $id = $this->input->post('id');
        $data['bank_name'] = $this->input->post('bank_name');
        $data['owner'] = $this->input->post('owner');
        $data['account'] = $this->input->post('account');
        $data['created_by'] = $_SESSION['com_in']['id'];

        if (!empty($_FILES)) {
            $config['upload_path'] = "./assets/banks";
            $config['allowed_types'] = 'jpg|png|jpeg';

            $file_name = $_FILES["file"]["name"];
            $file_ext = pathinfo($file_name, PATHINFO_EXTENSION);

            $new_name = $this->mylib->random_string(30) . '.' . $file_ext;

            $config['file_name'] = $new_name;

            $this->load->library('upload', $config);
            $this->load->library('image_lib');

            $this->upload->do_upload("file");

            $upload = $this->upload->data();

            if ($upload) {
                $configer = array(
                    'image_library' => 'gd2',
                    'source_image' => $upload['full_path'],
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'quality' => '100%',
                    'width' => 278,
                    'height' => 257,
                );
                $this->image_lib->clear();
                $this->image_lib->initialize($configer);
                $this->image_lib->resize();

                $data['image'] = $new_name;

                if($id == 'null'){
                    $insert = $this->main_model->store('bank', $data);

                    if ($insert) {
                        $activity['user_id'] = $_SESSION['com_in']['id'];
                        $activity['name'] = $_SESSION['com_in']['name'];
                        $activity['activity'] = 'Menambahkan Bank Penerima Dana dengan Nama Bank : '.$data['bank_name'].' No Rek : '.$data['account'].' pada '. date('d/m/Y H:i:s');

                        $this->main_model->store('log_activity', $activity);
                        echo "Success";
                    }
                }else{
                    $getFileName = $this->main_model->gda3p('bank', 'id', $id);
                    $update = $this->main_model->update('bank', $data, 'id', $id);

                    if ($update) {
                        if ($getFileName[0]['image'] != '') {
                            unlink("./assets/banks/" . $getFileName[0]['image']);
                        }
                        $activity['user_id'] = $_SESSION['com_in']['id'];
                        $activity['name'] = $_SESSION['com_in']['name'];
                        $activity['activity'] = 'Mengubah data Bank Penerima Dana dengan ID : '. $id.' dan Nama Bank : '.$data['bank_name'].' No Rek : '.$data['account'].' pada '. date('d/m/Y H:i:s');

                        $this->main_model->store('log_activity', $activity);
                        echo "Success";
                    }
                }  

            } else {
                echo "File cannot be uploaded";
            }

        } else {
            if($id == 'null'){
                $insert = $this->main_model->store('bank', $data);

                if ($insert) {
                    $activity['user_id'] = $_SESSION['com_in']['id'];
                    $activity['name'] = $_SESSION['com_in']['name'];
                    $activity['activity'] = 'Menambahkan Bank Penerima Dana dengan Nama Bank : '.$data['bank_name'].' No Rek : '.$data['account'].' pada '. date('d/m/Y H:i:s');

                    $this->main_model->store('log_activity', $activity);
                    echo "Success";
                }
            }else{
                $update = $this->main_model->update('bank', $data, 'id', $id);

                if ($update) {
                    $activity['user_id'] = $_SESSION['com_in']['id'];
                    $activity['name'] = $_SESSION['com_in']['name'];
                    $activity['activity'] = 'Mengubah data Bank Penerima Dana dengan ID : '. $id.' dan Nama Bank : '.$data['bank_name'].' No Rek : '.$data['account'].' pada '. date('d/m/Y H:i:s');

                    $this->main_model->store('log_activity', $activity);
                    echo "Success";
                }
            }
        
        }

    }

    public function show_bank()
    {
        if (
            isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ) {

            $datatables = $_POST;
            $datatables['e'] = 'edit';
            $datatables['d'] = 'destroy';
            $datatables['table'] = 'bank';
            $datatables['id-table'] = 'id';
            $datatables['col-display'] = array(
                'id',
                'bank_name',
                'owner',
                'account'
            );

            $this->d_table->Datatables($datatables);
        }
        return;
    }

    public function slider_image()
    {
        $data['slider'] = $this->main_model->gda1po('slider', 'id', 'desc');
        
        $this->load->view('content/admin/slider/slider_image', $data);
    }

    public function save_slide()
    {   
        $id = $this->input->post('id');
        $data['slide_name'] = $this->input->post('slide_name');
        $data['description'] = $this->input->post('description');
        $data['created_by'] = $_SESSION['com_in']['id'];

        if (!empty($_FILES)) {
            $config['upload_path'] = "./assets/slider";
            $config['allowed_types'] = 'jpg|png|jpeg';

            $file_name = $_FILES["file"]["name"];
            $file_ext = pathinfo($file_name, PATHINFO_EXTENSION);

            $new_name = $this->mylib->random_string(30) . '.' . $file_ext;

            $config['file_name'] = $new_name;

            $this->load->library('upload', $config);
            $this->load->library('image_lib');

            $this->upload->do_upload("file");

            $upload = $this->upload->data();

            if ($upload) {
                $configer = array(
                    'image_library' => 'gd2',
                    'source_image' => $upload['full_path'],
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'quality' => '100%',
                    'width' => 700,
                    'height' => 330,
                );
                $this->image_lib->clear();
                $this->image_lib->initialize($configer);
                $this->image_lib->resize();

                $data['image'] = $new_name;

                if($id == 'null'){
                    $insert = $this->main_model->store('slider', $data);

                    if ($insert) {
                        $activity['user_id'] = $_SESSION['com_in']['id'];
                        $activity['name'] = $_SESSION['com_in']['name'];
                        $activity['activity'] = 'Menambahkan Slide baru dengan Nama : '.$data['slide_name'].' pada '. date('d/m/Y H:i:s');

                        $this->main_model->store('log_activity', $activity);
                        echo "Success";
                    }
                }else{
                    $getFileName = $this->main_model->gda3p('slider', 'id', $id);
                    $update = $this->main_model->update('slider', $data, 'id', $id);

                    if ($update) {
                        if ($getFileName[0]['image'] != '') {
                            unlink("./assets/slider/" . $getFileName[0]['image']);
                        }
                        $activity['user_id'] = $_SESSION['com_in']['id'];
                        $activity['name'] = $_SESSION['com_in']['name'];
                        $activity['activity'] = 'Mengubah slide baru dengan ID : '. $id.' dan Nama Slide: '.$data['slide_name'].' pada '. date('d/m/Y H:i:s');

                        $this->main_model->store('log_activity', $activity);
                        echo "Success";
                    }
                }  

            } else {
                echo "File cannot be uploaded";
            }

        } else {
            if($id == 'null'){
                $insert = $this->main_model->store('slider', $data);

                if ($insert) {
                    $activity['user_id'] = $_SESSION['com_in']['id'];
                    $activity['name'] = $_SESSION['com_in']['name'];
                    $activity['activity'] = 'Menambahkan Slide baru dengan Nama : '.$data['slide_name'].' pada '. date('d/m/Y H:i:s');

                    $this->main_model->store('log_activity', $activity);
                    echo "Success";
                }
            }else{
                $update = $this->main_model->update('slider', $data, 'id', $id);

                if ($update) {
                    $activity['user_id'] = $_SESSION['com_in']['id'];
                    $activity['name'] = $_SESSION['com_in']['name'];
                    $activity['activity'] = 'Mengubah slide baru dengan ID : '. $id.' dan Nama Slide: '.$data['slide_name'].' pada '. date('d/m/Y H:i:s');

                    $this->main_model->store('log_activity', $activity);
                    echo "Success";
                }
            }
        
        }
    }

}
