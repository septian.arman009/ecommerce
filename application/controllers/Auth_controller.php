<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        $this->auth();
    }

    public function auth()
    {
        if ($this->session->userdata('com_in')) {
            redirect('admin');
        } else {
            return true;
        }
    }

    public function index()
    {
        $this->load->view('auth/signin');
    }

    public function forgot_password()
    {
        $this->load->view('auth/forgot_password');
    }

    public function signin_proccess()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;
        $password = md5($obj->password);

        $user = $this->main_model->gda5p('users', 'email', $email, 'password', $password);

        if ($user) {
            $session_array = array();
            foreach ($user as $key => $val) {
                $session_array = array(
                    'id' => $val['id'],
                    'name' => $val['name'],
                    'email' => $val['email'],
                    'role' => $val['role'],
                    'role_name' => $this->main_model->gdo4p('roles','display_name','id',$val['role']),
                );
            }
            $this->session->set_userdata('com_in', $session_array);
            $status = array('status' => 'success');
            $this->mylib->setJSON();
            echo json_encode($status);
        } else {
            $status = array('status' => 'errors');
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function send_token()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;

        $data['token'] = $this->mylib->random_string(40);
        $data['email'] = $email;

        $url = base_url() . 'password_reset/' . $data['token'];

        $message = 'Berikut ini adalah link reset password anda : ' . $url;

        $send_mail = $this->send_mail($email, $message, 'Password reset Admin Rumah Tiedye');

        if ($send_mail) {
            $insert = $this->main_model->store('token_password', $data);
            $status = array('status' => 'success');
            $this->mylib->setJSON();
            echo json_encode($status);
        } else {
            $status = array('status' => 'errors');
            $this->mylib->setJSON();
            echo json_encode($status);
        }

    }

    public function password_reset($token)
    {
        $check = $this->main_model->gda3p('token_password', 'token', $token);
        if (!$check) {
            $this->load->view('auth/token_error');
        } else {
            $data['email'] = $check[0]['email'];
            $this->load->view('auth/password_reset', $data);
        }
    }

    public function reset()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;
        $data['password'] = md5($obj->password);

        $reset = $this->main_model->update('users', $data, 'email', $email);

        if ($reset) {
            $this->main_model->destroy('token_password', 'email', $email);
            $status = array('status' => 'success');
            $this->mylib->setJSON();
            echo json_encode($status);
        } else {
            $status = array('status' => 'errors');
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function check_email()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;

        $check = $this->main_model->gda3p('users', 'email', $email);

        if ($check) {
            $status = array('status' => 'success');
            $this->mylib->setJSON();
            echo json_encode($status);
        } else {
            $status = array('status' => 'errors');
            $this->mylib->setJSON();
            echo json_encode($status);
        }

    }

    public function send_mail($email, $message, $subject)
    {
        $setting = $this->main_model->gda1p('setting');
        //Load email library
        $this->load->library('email');

        //SMTP & mail configuration
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.hostinger.co.id',
            'smtp_port' => 587,
            'smtp_user' => $setting[0]['send_mail'],
            'smtp_pass' => $setting[0]['send_pass'],
            'mailtype' => 'html',
            'charset' => 'utf-8',
        );
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        $this->email->to($email);
        $this->email->from($setting[0]['send_mail'], 'Admin Rumah Tiedye');
        $this->email->subject($subject);
        $this->email->message($message);

        //Send email
        $status = $this->email->send();

        return $status;
    }
}
