<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Chat_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        $this->auth();
    }

    public function auth()
    {
        if ($this->session->userdata('com_shop')) {
            return true;
        } else {
            redirect(base_url());
        }
    }

    public function discussion()
    {      
        $discussion_chat = $this->main_model->gda1pg('discussion', 'discussion_code', 'subject', 'discussion');

        $total_1 = 0;
        foreach ($discussion_chat as $key => $value) {
            if($value['from_email'] == $_SESSION['com_shop']['email'] || $value['to_email'] == $_SESSION['com_shop']['email']){
                $total_1++;
            }
        }

        $complain_chat = $this->main_model->gda1pg('discussion', 'discussion_code', 'subject', 'complain');

        $total_2 = 0;
        foreach ($complain_chat as $key => $value) {
            if($value['from_email'] == $_SESSION['com_shop']['email'] || $value['to_email'] == $_SESSION['com_shop']['email']){
                $total_2++;
            }
        }

        $data['reward'] = $this->main_model->count2where('reward','id', 'status', 'tersedia','member_id', $_SESSION['com_shop']['id']);

        $data['discussion_chat'] = $total_1;
        $data['complain_chat'] = $total_2;

        $this->load->view('shop/content/discussion', $data);
    }

    public function list_chat()
    {   
        $data['admin'] = $this->main_model->gda3p('users', 'role', 3);
        $users = $this->main_model->gda3p('users', 'role', 3);

        foreach ($users as $key => $value) {
            $data['chat'] = $this->main_model->get_chat($_SESSION['com_shop']['email'], $value['email'], 'discussion');
        }

        if(!empty($data['chat'])){
            foreach ($data['chat'] as $key => $value) {
                $data['check_unread'][] = $this->main_model->count4where('discussion', 'discussion_code', 'discussion_code', $value['discussion_code'], 'status', 'unread', 'to_email', $_SESSION['com_shop']['email']);
            }
        }else{
            $data['chat'] = array();
        }

        $this->load->view('shop/content/list_chat', $data);
    }

    public function list_complain()
    {   
        $data['chat'] = $this->main_model->get_chat_complain($_SESSION['com_shop']['email'], 'complain');
        foreach ($data['chat'] as $key => $value) {
            $data['check_unread'][] = $this->main_model->count4where('discussion', 'discussion_code', 'discussion_code', $value['discussion_code'], 'status', 'unread', 'to_email', $_SESSION['com_shop']['email']);
        }
        $this->load->view('shop/content/list_complain', $data);
    }

    public function discussion_chat($id)
    {   
        $chat = $this->main_model->gda3p('discussion', 'id', $id);
        $data['discussion'] = $this->main_model->discussion_chat($chat[0]['discussion_code'], $chat[0]['from_email'], $chat[0]['to_email'], 'discussion');
        $data['id'] = $id;
        $data['title'] = $chat[0]['title'];
        $data['status'] = $chat[0]['status'];
        $data['code'] = $chat[0]['discussion_code'];
        if($chat[0]['to_email'] != $_SESSION['com_shop']['email']){
            $to_email = $chat[0]['to_email'];
        }else{
            $to_email = $chat[0]['from_email'];
        }
        if( $data['status'] != 'close'){
            $data_read['status'] = 'aktif';
            $update_read = $this->main_model->update6p('discussion', $data_read, 'discussion_code', $chat[0]['discussion_code'], 'to_email', $_SESSION['com_shop']['email']);
        }
        $data['admin'] = $to_email;
        $this->load->view('shop/content/discussion_chat', $data);
    }

    public function add_discussion()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['title'] = $obj->title;
        $data['from_email'] = $_SESSION['com_shop']['email'];
        $data['to_email'] = $obj->crm_admin;
        $data['message'] = 'Memulai chat untuk Judul Diskusi : '.$data['title'];
        $data['subject'] = 'discussion';
        $data['status'] = 'unread';

        $data['discussion_code'] = $this->mylib->random_string(120);
       
        $store = $this->main_model->store('discussion', $data);
        if($store){
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $store);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function reply()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['title'] = $obj->title;
        $data['from_email'] = $_SESSION['com_shop']['email'];
        $data['to_email'] = $obj->crm_admin;
        $data['message'] = $obj->message;
        $data['discussion_code'] = $obj->code;
        $data['subject'] = 'discussion';
        $data['status'] = 'unread';
        
        $store = $this->main_model->store('discussion', $data);
        if($store){
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $store);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function add_complain()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['title'] = $obj->title;
        $data['inv'] = $obj->inv;
        $data['from_email'] = $_SESSION['com_shop']['email'];
        $data['message'] = $obj->message;
        $data['discussion_code'] = $this->mylib->random_string(120);
        $data['subject'] = 'complain';
        $data['status'] = 'unread';
       
        $store = $this->main_model->store('discussion', $data);
        if($store){
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $store);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function complain_chat($id)
    {   
        $chat = $this->main_model->gda3p('discussion', 'id', $id);

        if($chat[0]['from_email'] == $_SESSION['com_shop']['email']){
            $email = $chat[0]['from_email'];
        }else{
            $email = $chat[0]['to_email'];
        }

        $data['discussion'] = $this->main_model->complain_chat($chat[0]['discussion_code'], $email, 'complain');
        $data['id'] = $id;
        $data['title'] = $chat[0]['title'];
        $data['status'] = $chat[0]['status'];
        $data['code'] = $chat[0]['discussion_code'];
        
        if( $data['status'] != 'close'){
            $data_read['status'] = 'aktif';
            $update_read = $this->main_model->update6p('discussion', $data_read, 'discussion_code', $chat[0]['discussion_code'], 'to_email', $_SESSION['com_shop']['email']);
        }
        $this->load->view('shop/content/complain_chat', $data);
    }

    public function complain_reply()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['title'] = $obj->title;
        $data['from_email'] = $_SESSION['com_shop']['email'];
        $data['message'] = $obj->message;
        $data['discussion_code'] = $obj->code;
        $data['subject'] = 'complain';
        $data['status'] = 'unread';
        
        $store = $this->main_model->store('discussion', $data);
        if($store){
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $store);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function reward_list()
    {   
        $data['reward'] = $this->main_model->gda3p('reward','member_id', $_SESSION['com_shop']['id']);
        $this->load->view('shop/content/reward_list', $data);
    }


}
