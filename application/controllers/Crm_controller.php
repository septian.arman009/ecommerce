<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Crm_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        $this->load->model('d_table');
        $this->auth();
    }

    public function auth()
    {
        if ($this->session->userdata('com_in')) {
            return true;
        } else {
            redirect('signin');
        }
    }

    public function member()
    {
        $this->load->view('content/admin/member/member');
    }

    public function show_member()
    {
        if (
            isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ) {

            $datatables = $_POST;
            $datatables['e'] = 'edit';
            $datatables['d'] = 'destroy';
            $datatables['table'] = 'members';
            $datatables['id-table'] = 'id';
            $datatables['col-display'] = array(
                'id',
                'name',
                'email',
            );

            $this->d_table->Datatables($datatables);
        }
        return;
    }

    public function form($id)
    {
        $data['id'] = $id;
        $data['roles'] = $this->main_model->gda1p('roles');
        $this->load->view('content/admin/member/form', $data);
    }

    public function save()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $data['name'] = $obj->name;
        $data['email'] = $obj->email;
        $data['role'] = $obj->role;

        $update = $this->main_model->update('members', $data, 'id', $id);

        if ($update) {
            $activity['user_id'] = $_SESSION['com_in']['id'];
            $activity['name'] = $_SESSION['com_in']['name'];
            $activity['activity'] = 'Mengubah data Member dengan ID : ' . $id . ', Email : ' . $data['email'] . ' pada ' . date('d/m/Y H:i:s');
            $this->main_model->store('log_activity', $activity);
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $update);
            $this->mylib->setJSON();
            echo json_encode($status);
        }

    }

    public function review($year)
    {
        $data['month'] = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

        $data['year'] = $year;

        $yes = $this->main_model->countwherega('purchasement', 'member_id', 'satisfaction', 'yes', 'member_id');
        $data['yes'] = count($yes);
        $data['yes_transaction'] = $this->main_model->countwhere('purchasement', 'id', 'satisfaction', 'yes');

        $no = $this->main_model->countwherega('purchasement', 'member_id', 'satisfaction', 'no', 'member_id');
        $data['no'] = count($no);
        $data['no_transaction'] = $this->main_model->countwhere('purchasement', 'id', 'satisfaction', 'no');

        $unreview = $this->main_model->countwherega('purchasement', 'member_id', 'satisfaction', 'unreview', 'member_id');
        $data['unreview'] = count($unreview);
        $data['unreview_transaction'] = $this->main_model->countwhere('purchasement', 'id', 'satisfaction', 'unreview');

        $this->load->view('content/admin/review/review', $data);
    }

    public function g_review($year)
    {
        $month = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $total = $this->main_model->count('purchasement', 'id');

        $yes = $this->main_model->review($year, 'yes');

        foreach ($month as $key => $vm) {
            foreach ($yes as $key => $vy) {
                if ($this->mylib->to_month($vy['month']) == $vm) {
                    $row['data'][] = ($vy['total_row'] / $total) * 100;
                    $row_yes[] = $vy['total_row'];
                    break;
                } else {
                    $row['data'][] = 0;
                }
            }
        }

        $no = $this->main_model->review($year, 'no');

        foreach ($month as $key => $vm) {
            foreach ($no as $key => $vn) {
                if ($this->mylib->to_month($vn['month']) == $vm) {
                    $row1['data'][] = ($vn['total_row'] / $total) * 100;
                    break;
                } else {
                    $row1['data'][] = 0;
                }
            }
        }

        $unreview = $this->main_model->review($year, 'unreview');

        foreach ($month as $key => $vm) {
            foreach ($unreview as $key => $vu) {
                if ($this->mylib->to_month($vu['month']) == $vm) {
                    $row2['data'][] = ($vu['total_row'] / $total) * 100;
                    break;
                } else {
                    $row2['data'][] = 0;
                }
            }
        }

        $row['name'] = 'Total Pelanggan Puas';
        $row1['name'] = 'Total Pelanggan Tidak Puas';
        $row2['name'] = 'Total Pelanggan Tidak Memberikan Ulasan';
        $result = array();
        array_push($result, $row);
        array_push($result, $row1);
        array_push($result, $row2);
        print json_encode($result, JSON_NUMERIC_CHECK);
    }

    public function discussion()
    {
        if (!isset($_COOKIE['tab_active'])) {
            $data['tab_active'] = '#tab-first';
        } else {
            $data['tab_active'] = $_COOKIE['tab_active'];
        }

        $discussion_chat = $this->main_model->gda1pg('discussion', 'discussion_code', 'subject', 'discussion');

        $total_1 = 0;
        foreach ($discussion_chat as $key => $value) {
            if ($value['from_email'] == $_SESSION['com_in']['email'] || $value['to_email'] == $_SESSION['com_in']['email']) {
                $total_1++;
            }
        }

        $complain_chat = $this->main_model->gda1pg('discussion', 'discussion_code', 'subject', 'complain');

        $total_2 = 0;
        foreach ($complain_chat as $key => $value) {
            if ($value['from_email'] == 'NULL' || $value['to_email'] == 'NULL') {
                $total_2++;
            }
        }

        $data['discussion_chat'] = $total_1;
        $data['complain_chat'] = $total_2;
        $this->load->view('content/admin/discussion/discussion', $data);
    }

    public function list_chat()
    {
        $data['members'] = $this->main_model->gda1p('members');
        $members = $this->main_model->gda1p('members');

        foreach ($members as $key => $value) {
            $data['chat'] = $this->main_model->get_chat_crm($_SESSION['com_in']['email'], 'discussion');
        }

        foreach ($data['chat'] as $key => $value) {
            $data['check_unread'][] = $this->main_model->count4where('discussion', 'discussion_code', 'discussion_code', $value['discussion_code'], 'status', 'unread', 'to_email', $_SESSION['com_in']['email']);
        }

        $this->load->view('content/admin/discussion/list_chat', $data);

    }

    public function list_complain()
    {
        $data['chat'] = $this->main_model->get_chat_complain('NULL', 'complain');
        foreach ($data['chat'] as $key => $value) {
            $data['check_unread'][] = $this->main_model->count4where('discussion', 'discussion_code', 'discussion_code', $value['discussion_code'], 'status', 'unread', 'to_email', 'NULL');
        }

        $this->load->view('content/admin/discussion/list_complain', $data);
    }

    public function discussion_chat($id)
    {
        $chat = $this->main_model->gda3p('discussion', 'id', $id);
        $data['discussion'] = $this->main_model->discussion_chat($chat[0]['discussion_code'], $chat[0]['from_email'], $chat[0]['to_email'], 'discussion');
        $data['id'] = $id;
        $data['title'] = $chat[0]['title'];
        $data['status'] = $chat[0]['status'];
        $data['code'] = $chat[0]['discussion_code'];
        if ($chat[0]['to_email'] != $_SESSION['com_in']['email']) {
            $to_email = $chat[0]['to_email'];
        } else {
            $to_email = $chat[0]['from_email'];
        }
        if ($data['status'] != 'close') {
            $data_read['status'] = 'aktif';
            $update_read = $this->main_model->update6p('discussion', $data_read, 'discussion_code', $chat[0]['discussion_code'], 'to_email', $_SESSION['com_in']['email']);
        }
        $data['members'] = $to_email;
        $this->load->view('content/admin/discussion/discussion_chat', $data);
    }

    public function reply()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['title'] = $obj->title;
        $data['from_email'] = $_SESSION['com_in']['email'];
        $data['to_email'] = $obj->member;
        $data['message'] = $obj->message;
        $data['discussion_code'] = $obj->code;
        $data['subject'] = 'discussion';
        $data['status'] = 'unread';

        $store = $this->main_model->store('discussion', $data);
        if ($store) {
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $store);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function add_discussion()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['title'] = $obj->title;
        $data['from_email'] = $_SESSION['com_in']['email'];
        $data['to_email'] = $obj->member;
        $data['message'] = 'Memulai chat untuk Judul Diskusi : ' . $data['title'];
        $data['subject'] = 'discussion';
        $data['status'] = 'unread';

        $data['discussion_code'] = $this->mylib->random_string(120);

        $store = $this->main_model->store('discussion', $data);
        if ($store) {
            $activity['user_id'] = $_SESSION['com_in']['id'];
            $activity['name'] = $_SESSION['com_in']['name'];
            $activity['activity'] = 'Menambahkan Diskusi baru dengan Member : '. $data['to_email'].' pada '. date('d/m/Y H:i:s');

            $this->main_model->store('log_activity', $activity);
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $store);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function complain_chat($id)
    {
        $chat = $this->main_model->gda3p('discussion', 'id', $id);

        if ($chat[0]['from_email'] != '') {
            $email = $chat[0]['from_email'];
        } else {
            $email = $chat[0]['to_email'];
        }

        $data['discussion'] = $this->main_model->complain_chat($chat[0]['discussion_code'], $email, 'complain');
        $data['id'] = $id;
        $data['title'] = $chat[0]['title'];
        $data['inv'] = $chat[0]['inv'];
        $data['status'] = $chat[0]['status'];
        $data['code'] = $chat[0]['discussion_code'];
        $data['members'] = $email;
        if ($data['status'] != 'close') {
            $data_read['status'] = 'aktif';
            $update_read = $this->main_model->update6p('discussion', $data_read, 'discussion_code', $chat[0]['discussion_code'], 'to_email', 'NULL');
        }
        $this->load->view('content/admin/discussion/complain_chat', $data);
    }

    public function reply_complain()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['title'] = $obj->title;
        $data['inv'] = $obj->inv;
        $data['to_email'] = $obj->member;
        $data['message'] = $obj->message;
        $data['discussion_code'] = $obj->code;
        $data['subject'] = 'complain';
        $data['status'] = 'unread';

        $store = $this->main_model->store('discussion', $data);
        if ($store) {
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $store);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function close_complain()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $code = $obj->code;
        $data['status'] = 'close';

        $complain = $this->main_model->gda3p('discussion', 'discussion_code', $code);
        $update = $this->main_model->update('discussion', $data, 'discussion_code', $code);
        if ($update) {
            $activity['user_id'] = $_SESSION['com_in']['id'];
            $activity['name'] = $_SESSION['com_in']['name'];
            $activity['activity'] = 'Menutup Komplen dengan Judul : '. $complain[0]['title'].' pada '. date('d/m/Y H:i:s');

            $this->main_model->store('log_activity', $activity);
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $update);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function close()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $code = $obj->code;
        $data['status'] = 'close';

        $discussion = $this->main_model->gda3p('discussion', 'discussion_code', $code);
        $update = $this->main_model->update6p('discussion', $data, 'discussion_code', $code, 'subject', 'discussion');
        if ($update) {
            $activity['user_id'] = $_SESSION['com_in']['id'];
            $activity['name'] = $_SESSION['com_in']['name'];
            $activity['activity'] = 'Menutup Diskusi dengan Judul : '. $discussion[0]['title'].' pada '. date('d/m/Y H:i:s');

            $this->main_model->store('log_activity', $activity);
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $update);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function reward()
    {   
        if (!isset($_COOKIE['tab_reward'])) {
            $data['tab_reward'] = '#tab-first';
        } else {
            $data['tab_reward'] = $_COOKIE['tab_reward'];
        }
        $this->load->view('content/admin/reward/reward', $data);
    }

    public function general()
    {   
        $data['general'] = $this->main_model->gda1p('reward_list');
        $this->load->view('content/admin/reward/general', $data);
    }

    public function special()
    {   
        $this->load->view('content/admin/reward/special');
    }

    public function reward_form($id)
    {   
        $data['member'] = $this->main_model->gda3p('members', 'id', $id);
        $this->load->view('content/admin/reward/reward_form', $data);
    }

    public function save_general()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['voucher_code'] = $obj->code;
        $data['discount'] = $obj->discount;
        $data['description'] = $obj->description;
        $data['transaction'] = $obj->transaction;
        $day = $obj->validity * 24;
        $validity = '+'.$day.' hour';
        $startTime = date("Y-m-d H:i:s");
        $cenvertedTime = date('Y-m-d H:i:s', strtotime($validity, strtotime($startTime)));
        $data['expired'] = $obj->validity;
        $data['validity'] = $cenvertedTime;
        $data['status'] = 'true';

        $store = $this->main_model->store('reward_list', $data);
        if ($store) {
            $activity['user_id'] = $_SESSION['com_in']['id'];
            $activity['name'] = $_SESSION['com_in']['name'];
            $activity['activity'] = 'Menambahkan Reward Umum dengan Kode Voucher : '. $data['voucher_code'] .' berlaku sampai '.$this->mylib->to_date_time($cenvertedTime).' pada '. date('d/m/Y H:i:s');

            $this->main_model->store('log_activity', $activity);
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $store);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function update_general()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $data['voucher_code'] = $obj->code;
        $data['discount'] = $obj->discount;
        $data['description'] = $obj->description;
        $data['transaction'] = $obj->transaction;

        $day = $obj->validity * 24;
        $validity = '+'.$day.' hour';
        $old_time = $this->main_model->gdo4p('reward_list', 'created_at', 'id', $id);
        $startTime = $old_time;
        $cenvertedTime = date('Y-m-d H:i:s', strtotime($validity, strtotime($startTime)));

        $data['expired'] = $obj->validity;
        $data['validity'] = $cenvertedTime;

        if($cenvertedTime > date('Y-m-d H:i:s')){
            $data['status'] = 'true';
        }

        $update = $this->main_model->update('reward_list', $data, 'id', $id);
        if ($update) {
            $activity['user_id'] = $_SESSION['com_in']['id'];
            $activity['name'] = $_SESSION['com_in']['name'];
            $activity['activity'] = 'Mengubah Reward Umum dengan Kode Voucher : '. $data['voucher_code'] .' berlaku sampai '.$this->mylib->to_date_time($cenvertedTime).' pada '. date('d/m/Y H:i:s');

            $this->main_model->store('log_activity', $activity);
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $update);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function member_detail($name){
        $member_name = str_replace('-', ' ', $name);
        $member = $this->main_model->gda3plike('members', 'name', $member_name, 'name');

        if($member){
            foreach ($member as $key => $value) {
                $data['member_id'][] = $value['id'];
                $data['name'][] = $value['name'];
                $data['transaction'][] = $this->main_model->countwhere('purchasement', 'member_id', 'member_id', $value['id']);
                $data['total'][] = $this->mylib->torp($this->main_model->sumwhere('purchasement', 'transfer_ammount', 'member_id', $value['id']));
                $data['join'][] = $this->mylib->to_date_time($value['created_at']);
                $data['last'][] = $this->mylib->to_date_time($this->main_model->maxwhere('purchasement', 'created_at', 'member_id', $value['id']));
            }
        }else{
            $data['name'] = array();
        }
       

        $this->load->view('content/admin/reward/member_list', $data);
    }

    public function save_special()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['member_id'] = $obj->member_id;
        $data['voucher'] = $obj->code;
        $data['discount'] = $obj->discount;
        $day = $obj->expired * 24;
        $validity = '+'.$day.' hour';
        $startTime = date("Y-m-d H:i:s");
        $cenvertedTime = date('Y-m-d H:i:s', strtotime($validity, strtotime($startTime)));
        $data['expired'] = $obj->expired;
        $data['validity'] = $cenvertedTime;
        $data['description'] =  $obj->description;
        $data['status'] =  'tersedia';

        $store = $this->main_model->store('reward', $data);
        if ($store) {
            $activity['user_id'] = $_SESSION['com_in']['id'];
            $activity['name'] = $_SESSION['com_in']['name'];
            $activity['activity'] = 'Menambahkan Reward Khusus dengan Kode Voucher : '. $data['voucher'] .' berlaku sampai '.$this->mylib->to_date_time($cenvertedTime).' pada '. date('d/m/Y H:i:s');

            $this->main_model->store('log_activity', $activity);
            $status = array('status' => 'success', 'status_code' => '200', 'data' => $store);
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function check_voucher()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $voucher = $obj->code;
      
        $check = $this->main_model->gda5p('reward_list', 'voucher_code', $voucher, 'status', 'true');
        $check1 = $this->main_model->gda5p('reward', 'voucher', $voucher, 'status', 'tersedia');
        if (!$check && !$check1) {
            $status = array('status' => 'success');
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function check_voucher_special()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $voucher = $obj->code;
        
        $check = $this->main_model->gda5p('reward_list', 'voucher_code', $voucher, 'status', 'true');        
        $check1 = $this->main_model->gda5p('reward', 'voucher', $voucher, 'status', 'tersedia');
        if (!$check && !$check1) {
            $status = array('status' => 'success');
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

}
