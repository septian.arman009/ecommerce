<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        $this->load->model('d_table');
        $this->auth();
    }

    public function auth()
    {
        if ($this->session->userdata('com_in')) {
            return true;
        } else {
            redirect('signin');
        }
    }

    public function admin()
    {
        if ($_SESSION['com_in']['role'] == '4') {
            return true;
        } else {
            $status = array('status' => 'errors');
            $this->mylib->setJSON();
            echo json_encode($status);
            die();
        }
    }

    public function index()
    {
        $this->load->view('content/admin/user/user');
    }

    public function show()
    {
        if (
            isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ) {

            $datatables = $_POST;
            $datatables['e'] = 'edit';
            $datatables['d'] = 'destroy';
            $datatables['table'] = 'users';
            $datatables['id-table'] = 'id';
            $datatables['col-display'] = array(
                'id',
                'name',
                'email',
            );

            $this->d_table->Datatables($datatables);
        }
        return;
    }

    public function form($id)
    {
        $data['id'] = $id;
        $data['roles'] = $this->main_model->gda1p('roles');
        $this->load->view('content/admin/user/form', $data);
    }

    public function save()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $data['name'] = $obj->name;
        $data['email'] = $obj->email;
        $data['role'] = $obj->role;    
       
        if ($id == 'null') {
            $password = $this->mylib->random_string(8);
            $data['password'] = md5($password);
            $message = 'Berikut ini adalah password untuk akun ' . $data['email'] . ' : ' . $password;
            $subject = ' Password acak untuk akun ' . $data['email'];

            $send_mail = $this->send_mail($data['email'], $message, $subject);

            if ($send_mail) {
                $store = $this->main_model->store('users', $data);

                $activity['user_id'] = $_SESSION['com_in']['id'];
                $activity['name'] = $_SESSION['com_in']['name'];
                $activity['activity'] = 'Menambahkan Pengguna baru dengan email : ' . $data['email'] . ' pada ' . date('d/m/Y H:i:s') . '	perika email untuk melihat password acak dari sistem !';

                $this->main_model->store('log_activity', $activity);

                if ($store) {
                    $status = array('status' => 'success', 'status_code' => '200', 'data' => $store);
                    $this->mylib->setJSON();
                    echo json_encode($status);
                }

            }
        } else {
            $update = $this->main_model->update('users', $data, 'id', $id);

            if ($update) {
                $activity['user_id'] = $_SESSION['com_in']['id'];
                $activity['name'] = $_SESSION['com_in']['name'];
                $activity['activity'] = 'Mengubah data Pengguna dengan ID : ' . $id . ', Email : ' . $data['email'] . ' pada ' . date('d/m/Y H:i:s');
                $this->main_model->store('log_activity', $activity);
                $status = array('status' => 'success', 'status_code' => '200', 'data' => $update);
                $this->mylib->setJSON();
                echo json_encode($status);
            }
        }

    }

    public function send_mail($email, $message, $subject)
    {
        $setting = $this->main_model->gda1p('setting');
        //Load email library
        $this->load->library('email');

        //SMTP & mail configuration
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.hostinger.co.id',
            'smtp_port' => 587,
            'smtp_user' => $setting[0]['send_mail'],
            'smtp_pass' => $setting[0]['send_pass'],
            'mailtype' => 'html',
            'charset' => 'utf-8',
        );
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        $this->email->to($email);
        $this->email->from($setting[0]['send_mail'], 'Admin Rumah Tiedye');
        $this->email->subject($subject);
        $this->email->message($message);

        //Send email
        $status = $this->email->send();

        return $status;
    }

}
