<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Rumah Tiedye - Shop </title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<link href="<?php echo base_url() ?>assets/shop/css/bootstrap.css" rel="stylesheet" />

	<link href="<?php echo base_url() ?>assets/shop/style.css" rel="stylesheet" />

	<link href="<?php echo base_url() ?>assets/shop/font-awesome/css/font-awesome.css" rel="stylesheet">

	<link rel="shortcut icon" href="<?php echo base_url() ?>assets/shop/ico/favicon.ico">
</head>

<body id="body-reset">

	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="topNav">
			<div class="container">
				<div class="alignR">
					<div class="pull-left socialNw">
						<a href="#">
							<span class="icon-twitter"></span>
						</a>
						<a href="#">
							<span class="icon-facebook"></span>
						</a>
						<a href="#">
							<span class="icon-youtube"></span>
						</a>
						<a href="#">
							<span class="icon-tumblr"></span>
						</a>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div id="gototop"> </div>
		<header id="header">
			<div class="row">
				<div class="span4">
					<h1>
						<a class="logo">
							<img src="<?php echo base_url() ?>assets/shop/img/logo1.png" alt="bootstrap sexy shop">
						</a>
					</h1>
				</div>
				<div class="span4">

				</div>
				<div class="span4 alignR">
					<p>
						<br>
						<strong> Support : 089517227009 </strong>
						<br>
						<br>
					</p>
				</div>
			</div>
		</header>
		<div class="span12">
			<ul class="breadcrumb">
				<li class="active">Reset Password</li>
			</ul>
			<div id="success" class="alert alert-success" style="display: none">
				<a class="close" onclick="$('#success').hide()">×</a>
				<strong>Sukses !</strong> Link reset password berhasil dikirim, silakan check email anda.
			</div>

			<div class="well">
				<form class="form-horizontal">
					<h3>Masukan Password baru</h3>

					<div class="control-group">
						<label class="control-label">Password
						</label>
						<div class="controls">
							<input onkeyup="check()" id="password" type="password">
							<p id="passless" style="color:red;display:none;">Minimal password 8 karakter</p>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Konfirmasi Password
						</label>
						<div class="controls">
							<input onkeyup="check()" id="confirm" type="password">
							<p id="conless" style="color:red;display:none;">Minimal password 8 karakter</p>
							<p id="notmatch" style="color:red;display:none;">Password tidak sama</p>
						</div>
					</div>

					<div class="control-group">
						<div class="controls">
							<a onclick="reset_password()" id='save' style="display:none" disbaled class="exclusive shopBtn"> Reset Password </a>
							<a id='save-loading' style="display:none" class="exclusive shopBtn"> Memproses .. </a>
							<a id='save-error' class="exclusive shopBtn"> Isi Data </a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>


	<script src="<?php echo base_url() ?>assets/shop/js/jquery.js"></script>
	<script src="<?php echo base_url() ?>assets/shop/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>assets/shop/js/jquery.easing-1.3.min.js"></script>
	<script src="<?php echo base_url() ?>assets/shop/js/jquery.scrollTo-1.4.3.1-min.js"></script>
	<script src="<?php echo base_url() ?>assets/shop/js/shop.js"></script>
	<script src="<?php echo base_url() ?>assets/admin/myjs/shop.js"></script>

</body>
<script>
	check();

	function check() {
		var password = $("#password").val();
		var confirm = $("#confirm").val();

		var passlen = password.length;
		var conlen = confirm.length;

		if (passlen != '') {
			if (passlen < 8) {
				$("#passless").show();
			} else {
				$("#passless").hide();
			}
		} else {
			$("#passless").hide();
		}

		if (confirm != '') {
			if (conlen < 8) {
				$("#conless").show();
			} else {
				$("#conless").hide();
			}
		} else {
			$("#conless").hide();
			$("#notmatch").hide();
		}


		if (password != '' && confirm != '') {

			if (password == confirm) {
				$("#notmatch").hide();

				if (password.length >= 8 && confirm.length >= 8) {
					$('#save').show();
					$('#save-error').hide();
				} else {
					$("#save").hide();
					$("#save-error").show();
				}
			} else {
				$("#notmatch").show();
				$("#save").hide();
				$("#save-error").show();
			}

		} else {
			$("#save").hide();
			$("#save-error").show();
		}
	}

	function reset_password() {
		$('#save').hide();
		$('#save-loading').show();
        var email = '<?php echo $email ?>';
        var password = $("#password").val();
		var data = {
			email: email,
			password: password
		}

		postData('<?php echo base_url() ?>shop_controller/new_password', data, function (err, response) {
			if (response) {
				console.log("berhasil : ", response);
				var status = response.status;
				if (status == 'success') {
					signin(email, password);
				} else {
					$('#save').show();
					$('#save-loading').hide();
					$('#error').show();
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

    function signin(email, password) {
	
		var data = {
			email: email,
			password: password
		};

		console.log("data :", data);
		postData("<?php echo base_url() ?>shop_controller/signin_proccess", data, function (err, response) {
			if (response) {
				console.log("response : ", response);
				if (response.status == "success") {
					setTimeout(() => {
                        window.location = '<?php echo base_url() ?>';
					}, 500);
				}
			}
		});
	}

	$("#body-reset").attr("style", 'background: url("<?php echo base_url() ?>assets/shop/img/white_leather.png") repeat 0 0;');
</script>

<style>

	a {
		cursor: pointer;
	}

</style>

</html>