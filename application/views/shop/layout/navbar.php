<div class="container">
	<div id="gototop"> </div>
	<header id="header">
		<div class="row">
			<div class="span4">
				<h1>
					<a class="logo">
						<img src="<?php echo base_url() ?>assets/shop/img/logo1.png" alt="bootstrap sexy shop">
					</a>
				</h1>
			</div>
			<div class="span4">

			</div>
			<div class="span4 alignR">
				<p>
					<br>
					<strong> Support : 089517227009 </strong>
					<br>
					<br>
				</p>
			</div>
		</div>
	</header>

	<div class="navbar">
		<div class="navbar-inner">
			<div class="container">
				<a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<div class="nav-collapse">
					<ul class="nav">
						<li class="">

						</li>
						<li class="">

						</li>
						<li class="">

						</li>
						<li class="">

						</li>
						<li class="">

						</li>
						<li class="">

						</li>
					</ul>
					<form action="#" class="navbar-search pull-left">
						<select id="product_categories" class="form-control">
							<option value="">- Pilih Kategori -</option>
							<?php foreach ($categories as $key => $value) { ?>
							<option value="<?php echo $value['id'] ?>">
								<?php  echo $value['categories']?>
							</option>
							<?php }?>
						</select>
						<input onchange="search_product()" id="search" type="text" placeholder="Cari Produk" class="form-control">
					</form>
					<?php if(!$this->session->userdata('com_shop')){ ?>
					<ul class="nav pull-right">
						<li class="dropdown">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<span class="icon-lock"></span> Login
								<b class="caret"></b>
							</a>
							<div class="dropdown-menu">
								<form class="form-horizontal loginFrm" style="width:245px;">
									<div class="control-group">
										<div id="lognav-error" class="alert alert-danger" style="display: none">
											<a class="close" onclick="$('.alert').hide()">×</a>
											<strong>Gagal Login !</strong> Cek username dan password.
										</div>
										<div id="notactive" class="alert alert-warning" style="display: none">
											<a class="close" onclick="$('.alert').hide()">×</a>
											<strong>Gagal Login !</strong> Akun anda belum terverifikasi.
										</div>
									</div>
									<div class="control-group">
										<input autocomplete="new-email" onkeyup="check_login_nav()" id="email_nav" type="text" id="inputEmail" placeholder="Email">
									</div>
									<div class="control-group">
										<input autocomplete="new-password" onkeyup="check_login_nav()" id="password_nav" type="password" id="inputPassword" placeholder="Password">
									</div>
									<div class="control-group">
										<a onclick="signin_nav()" id='sign-nav' style="display:none" class="exclusive shopBtn"> Login </a>
										<a id='sign-loading-nav' style="display:none" class="exclusive shopBtn"> Memproses .. </a>
										<a id='sign-error-nav' class="exclusive shopBtn"> Login </a> 
										<a onclick="hs_menu('#','shop_controller/forgot_password','#sidebar','#')">Lupas Password ?</a>
									</div>
								</form>
							</div>
						</li>
					</ul>
					<?php }else{?>
					<ul class="nav pull-right">
						<li class="dropdown">
							<a onclick="logout_shop()" data-toggle="dropdown" class="dropdown-toggle" href="#">
								<span class="icon-unlock"></span> Logout (
								<?php echo $_SESSION['com_shop']['name'] ?>)
							</a>
						</li>
					</ul>
					<?php }?>
				</div>
			</div>
		</div>
	</div>