<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Rumah Tiedye - Shop	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<link href="<?php echo base_url() ?>assets/shop/css/bootstrap.css" rel="stylesheet" />

	<link href="<?php echo base_url() ?>assets/shop/style.css" rel="stylesheet" />

	<link href="<?php echo base_url() ?>assets/shop/font-awesome/css/font-awesome.css" rel="stylesheet">

	<link rel="shortcut icon" href="<?php echo base_url() ?>assets/shop/ico/favicon.ico">
</head>

<body>

	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="topNav">
			<div class="container">
				<div class="alignR">
					<div class="pull-left socialNw">
						<a href="#">
							<span class="icon-twitter"></span>
						</a>
						<a href="#">
							<span class="icon-facebook"></span>
						</a>
						<a href="#">
							<span class="icon-youtube"></span>
						</a>
						<a href="#">
							<span class="icon-tumblr"></span>
						</a>
					</div>

					<a id="home" onclick="hs_menu('#home','shop_controller/home/1/null/null','#','#sidebar')">
						<span class="icon-home"></span> Halam Utama
					</a>

					<?php if($this->session->userdata('com_shop')){ ?>
						<?php if($_SESSION['com_shop']['role'] == 2){ ?>
							<a id="myaccount" onclick="hs_menu('#myaccount','shop_controller/myaccount','#sidebar','#')">
								<span class="icon-user"></span> Akun Saya
							</a>
						
							<a id="payment" onclick="hs_menu('#payment','shop_controller/payment','#sidebar','#')">
								<span class="icon-money"></span> Pembayaran
							</a>

							<a id="discussion" onclick="hs_menu('#discussion','chat_controller/discussion','#sidebar','#')">
								<span class="icon-group"></span> Diskusi (<?php echo $check_unread_main ?>) & Reward (<?php echo $reward ?>)
							</a>

						<?php }?>
					<?php }?>

					<?php if(!$this->session->userdata('com_shop')){ ?>

					<a id="register" onclick="hs_menu('#register','shop_controller/register_form','#sidebar','#')">
						<span class="icon-edit"></span> Free Registrasi 
					</a>

					<?php }?>

					<a id="checkout" onclick="hs_menu('#checkout','cart_controller/detail_cart','#sidebar','#')">
						<span id="total_items" class="icon-shopping-cart"></span>
						<span id="subtotal" class="badge badge-warning"></span>
					</a>

				</div>
			</div>
		</div>
	</div>