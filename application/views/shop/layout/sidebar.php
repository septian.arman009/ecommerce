<div class="row">
	<div id="sidebar" class="span3">
		<div class="well well-small">
			<ul class="nav nav-list">
				<li id="side">
					<a onclick='hs_menu("#side","shop_controller/home/1/null/null","#","#sidebar")'>
						<span class="icon-chevron-right"> Tampilkan Semua</span>
					</a>
				</li>
				<?php foreach ($categories as $key => $value) { ?>
				<li id="side<?php echo $value['id'] ?>">
					<a onclick='hs_menu("#side<?php echo $value['id'] ?>","shop_controller/home/1/null/<?php echo $value['id'] ?>","#","#sidebar")'>
						<span class="icon-chevron-right"></span>
						<?php echo $value['categories'] ?>
					</a>
				</li>
				<?php } ?>

				<li style="border:0"> &nbsp;</li>
			</ul>
		</div>

		<div class="well well-small alert alert-warning cntr">
			<h2>Diskon 10%</h2>
			<p>
				Dapatkan kode voucher untuk potongan belanja sebesar 10% setelah melakukan transaksi pertamamu :)
				<br>
				<p style="color:red">Kode voucher dapat dilihat pada menu Reward</p>
				<br>
			</p>
		</div>

	</div>
	<div class="content" style="min-height:75vh"></div>
</div>
</div>
