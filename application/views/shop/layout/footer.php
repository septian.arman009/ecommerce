<div class="copyright" style="bottom:0;">
	<div class="container">
		<span>Copyright &copy; 2018
			<br> Rumah Tiedye Shop</span>
	</div>
</div>

<!-- <button style="" style="m-review" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"></button> -->

<div style="display:none" id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 id="m-h-text" class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<p id="m-b-text"></p>
				<div id="mr-content"></div>
			</div>
			<div class="modal-footer">
				<button id="m-f-btn-yes" type="button" class="btn btn-default"></button>
				<button id="m-f-btn-no" type="button" class="btn btn-default" data-dismiss="modal"></button>
			</div>
		</div>

	</div>
</div>

<div style="display:none" id="myReview" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 id="mr-h-text" class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<p id="mr-b-text"></p>
				<textarea id="review-text" style="width:95%" id="review_field" cols="30" rows="10"></textarea>
			</div>
			<div class="modal-footer">
				<button id="mr-f-btn-yes" type="button" class="btn btn-default">
					<i id="mr-f-btn-icon-yes" class="icon-thumbs-up"></i>
				</button>
				<button id="mr-f-btn-no" type="button" class="btn btn-default">
					<i id="mr-f-btn-icon-no" class="icon-thumbs-down"></i>
				</button>
			</div>
		</div>

	</div>
</div>

<script src="<?php echo base_url() ?>assets/shop/js/jquery.js"></script>
<script src="<?php echo base_url() ?>assets/shop/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/shop/js/jquery.easing-1.3.min.js"></script>
<script src="<?php echo base_url() ?>assets/shop/js/jquery.scrollTo-1.4.3.1-min.js"></script>
<script src="<?php echo base_url() ?>assets/shop/js/shop.js"></script>
<script src="<?php echo base_url() ?>assets/admin/myjs/shop.js"></script>

</body>

<script>
	var shop_url = document.cookie.replace(/(?:(?:^|.*;\s*)shop_url\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	if (shop_url) {
		var sidebar = document.cookie.replace(/(?:(?:^|.*;\s*)sidebar\s*\=\s*([^;]*).*$)|^.*$/, "$1");
		var sidebar = document.cookie.replace(/(?:(?:^|.*;\s*)sidebar\s*\=\s*([^;]*).*$)|^.*$/, "$1");
		var d_hide = document.cookie.replace(/(?:(?:^|.*;\s*)d_hide\s*\=\s*([^;]*).*$)|^.*$/, "$1");
		var d_show = document.cookie.replace(/(?:(?:^|.*;\s*)d_show\s*\=\s*([^;]*).*$)|^.*$/, "$1");
		if (sidebar) {
			hs_menu(sidebar, shop_url, d_hide, d_show);
		} else {
			var search = document.cookie.replace(/(?:(?:^|.*;\s*)search\s*\=\s*([^;]*).*$)|^.*$/, "$1");
			var product_categories = document.cookie.replace(/(?:(?:^|.*;\s*)product_categories\s*\=\s*([^;]*).*$)|^.*$/, "$1");
			$("#search").val(search);
			$("#product_categories").val(product_categories);
			hs_menu('#', shop_url, '#', '#sidebar');
		}
	} else {
		hs_menu("#home", "shop_controller/home/1/null/null", '#', '#sidebar');
	}

	check_cart();

	function check_cart() {
		data = {

		}
		console.log("data : ", data);
		postData('cart_controller/check_cart', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					$("#total_items").html(' (' + response.data.total_items + ') ');
					$("#subtotal").html(response.data.subtotal);
				} else {
					$("#total_items").html(' (0)');
					$("#subtotal").html('Rp. 0');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function check_login_nav() {
		var email = $("#email_nav").val();
		var password = $("#password_nav").val();

		if (password != '' && email != '') {
			$("#sign-nav").show();
			$("#sign-error-nav").hide();
		} else {
			$("#sign-nav").hide();
			$("#sign-error-nav").show();
		}
	}

	function signin_nav() {
		$("#sign-nav").hide();
		$("#sign-loading-nav").show();
		var data = {
			email: $("#email_nav").val(),
			password: $("#password_nav").val()
		};

		console.log("data :", data);
		postData("shop_controller/signin_proccess", data, function (err, response) {
			if (response) {
				console.log("response : ", response);
				if (response.status == "success") {
					hs_menu("#home", "shop_controller/home/1/null/null", '#', '#sidebar');
					setTimeout(() => {
						location.reload();
					}, 500);

				} else if (response.status == "notactive") {
					$("#notactive").show();
				} else {
					$("#lognav-error").show();
					$("#sign-nav").show();
					$("#sign-loading-nav").hide();
				}
			}
		});
	}

	function search_product() {
		var key = $("#search").val();
		var key_pass = key.replace(/[^A-Za-z]/g, "");
		if (key_pass == '') {
			key_pass = 'null';
		}

		var categories = $("#product_categories").val();
		if (categories == '') {
			categories = 'null';
		}

		var old_sidebar = document.cookie.replace(/(?:(?:^|.*;\s*)sidebar\s*\=\s*([^;]*).*$)|^.*$/, "$1");
		$(old_sidebar).removeClass("active");

		var url = 'shop_controller/home/1/' + key_pass + '/' + categories;

		document.cookie = "sidebar=";
		document.cookie = "shop_url=" + url;

		document.cookie = "search=" + key_pass;
		document.cookie = "product_categories=" + categories;

		$("#sliding").show();

		hs_menu('#home', url, '#', '#sidebar');

	}
</script>

<style>
	body {
		background: url('assets/shop/img/white_leather.png') repeat 0 0;
	}

	a {
		cursor: pointer;
	}

	#sidebar .nav a:hover {
		color: #5d615c;
	}

	.nav-list>.active>a,
	.nav-list>.active>a:hover,
	.nav-list>.active>a:focus {
		background-color: #f89406;
	}
</style>

</html>