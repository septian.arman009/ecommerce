<div class="span12">
	<ul class="breadcrumb">
		<li class="active">Registrasi</li>
	</ul>
	<div id="success" class="alert alert-success" style="display: none">
		<a class="close" onclick="$('#success').hide()">×</a>
		<strong>Sukses !</strong> Pendaftaran berhasil cek email untuk aktivasi.
	</div>
	<div id="error" class="alert alert-danger" style="display: none">
		<a class="close" onclick="$('#error').hide()">×</a>
		<strong>Terjadi Kesalahan !</strong> Pendaftaran gagal, silakan coba lagi.
	</div>
	<div class="well">
		<form class="form-horizontal">
			<h3>Data Pribadi Anda</h3>
			<div class="control-group">
				<label class="control-label" for="inputFname">Nama Lengkap
				</label>
				<div class="controls">
					<input onkeyup="check()" id="name" type="text" id="inputFname">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputEmail">Email
				</label>
				<div class="controls">
					<input onkeyup="check()" id="email" type="email">
					<p id="invalid_email" style="color:red;display:none;">Masukan format email yang benar (Contoh : @gmail.com) !</p>
					<p id="registered" style="color:red;display:none;">Email sudah terdaftar !</p>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Password
				</label>
				<div class="controls">
					<input onkeyup="check()" id="password" type="password">
					<p id="passless" style="color:red;display:none;">Minimal password 8 karakter</p>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Konfirmasi Password
				</label>
				<div class="controls">
					<input onkeyup="check()" id="confirm" type="password">
					<p id="conless" style="color:red;display:none;">Minimal password 8 karakter</p>
					<p id="notmatch" style="color:red;display:none;">Password tidak sama</p>
				</div>
			</div>

			<div class="control-group">
				<div class="controls">
					<a onclick="register()" id='save' style="display:none" disbaled class="exclusive shopBtn"> Daftar </a>
					<a id='save-loading' style="display:none" class="exclusive shopBtn"> Memproses .. </a>
					<a id='save-error' class="exclusive shopBtn"> Lengkapi Data </a>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
	check();

	function check() {
		var name = $("#name").val();
		var email = $("#email").val();
		var password = $("#password").val();
		var confirm = $("#confirm").val();

		var passlen = password.length;
		var conlen = confirm.length;

		if (passlen != '') {
			if (passlen < 8) {
				$("#passless").show();
			} else {
				$("#passless").hide();
			}
		} else {
			$("#passless").hide();
		}

		if (confirm != '') {
			if (conlen < 8) {
				$("#conless").show();
			} else {
				$("#conless").hide();
			}
		} else {
			$("#conless").hide();
			$("#notmatch").hide();
		}


		if (password != '' && confirm != '') {

			if (password == confirm) {
				$("#notmatch").hide();

				if (password.length >= 8 && confirm.length >= 8) {
					if (name != '' && email != '') {
						if (ValidateEmail(email, '#save', "#save-error", '#invalid_email')) {
							check_email();
						}
					} else {
						$("#save").hide();
						$("#save-error").show();
						$("#registered").hide();
					}

				} else {
					$("#save").hide();
					$("#save-error").show();
				}
			} else {
				$("#notmatch").show();
				$("#save").hide();
				$("#save-error").show();
			}

		} else {
			$("#save").hide();
			$("#save-error").show();
		}
	}

	function check_email() {

		var data = {
			email: $("#email").val()
		}

		postData('shop_controller/check_email', data, function (err, response) {
			if (response) {
				console.log("berhasil : ", response);
				var status = response.status;
				if (status == 'success') {
					$("#registered").hide();
					$('#save').show();
					$('#save-error').hide();
				} else {
					$("#registered").show();
					$('#save').hide();
					$('#save-error').show();
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function ValidateEmail(mail, btn, btn2, id_msg) {
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
			$(id_msg).hide();
			$(btn).show();
			$(btn2).hide();
			return true;
		}
		$(id_msg).show();
		$(btn).hide();
		$(btn2).show();
	}

	function register() {
		$('#save').hide();
		$('#save-loading').show();
		var data = {
			name: $("#name").val(),
			email: $("#email").val(),
			password: $("#password").val()
		}

		postData('shop_controller/register', data, function (err, response) {
			if (response) {
				console.log("berhasil : ", response);
				var status = response.status;
				if (status == 'success') {
					hs_menu('#register', 'shop_controller/register_form','#sidebar','#');
					setTimeout(() => {
						$('#success').show();
						setTimeout(() => {
							check_cart();
						}, 500);
					}, 1000);
				} else {
					$('#save').show();
					$('#save-loading').hide();
					$('#error').show();
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>