<div class="span12">
	<ul class="breadcrumb">
		<li class="active">Login</li>
	</ul>
    <div id="error" class="alert alert-danger" style="display: none"> 
		<a class="close" onclick="$('#error').hide()">×</a>  
		<strong>Terjadi Kesalahan !</strong> Login gagal, silakan coba lagi.  
	</div>
	<div class="well">
		<form class="form-horizontal">
			<h3>Login Untuk Melanjutkan Belanja</h3>
			
			<div class="control-group">
				<label class="control-label" for="inputEmail">Email
				</label>
				<div class="controls">
					<input onkeyup="check()" autocomplete="new-email" id="email" type="email" placeholder="Email">
					<p id="invalid_email" style="color:red;display:none;">Masukan format email yang benar (Contoh : @gmail.com) !</p>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Password
				</label>
				<div class="controls">
					<input onkeyup="check()" autocomplete="new-password" id="password" type="password" placeholder="Password">
				</div>
			</div>

			<div class="control-group">
				<div class="controls">
					<a onclick="signin()" id='sign' style="display:none" class="exclusive shopBtn"> Login </a>		
					<a id='sign-loading' style="display:none" class="exclusive shopBtn"> Memproses .. </a>
					<a id='sign-error' style="display:none" class="exclusive shopBtn"> Login </a>	
					<a onclick="hs_menu('#register','shop_controller/register_form','#sidebar','#')" class="exclusive shopBtn"> Register </a>	
				</div>
			</div>
		</form>
	</div>
</div>

<script>
    check();
	function check() {
		var email = $("#email").val();
		var password = $("#password").val();

		if (password != '' && email != '') {
				$("#sign").show();
				$("#sign-error").hide();
		}else{
			$("#sign").hide();
			$("#sign-error").show();
		}
	}

	function signin() {
		$("#sign").hide();
		$("#sign-loading").show();
		var data = {
			email: $("#email").val(),
			password: $("#password").val()
		};

		console.log("data :", data);
		postData("shop_controller/signin_proccess", data, function (err, response) {
			if (response) {
				console.log("response : ", response);
				if (response.status == "success") {
					hs_menu("#home","shop_controller/home/1/null/null",'#','#sidebar');
					setTimeout(() => {
						location.reload();
					}, 500);
				} else {
					$("#sign").show();
					$("#sign-loading").hide();
					$("#error").show();
				}
			}
		});
	}
</script>