<div class="control-group">
	<label class="control-label" for="inputFname">Judul Komplen
	</label>
	<div class="controls">
		<input onkeyup="check_complain()" id="complain_title" type="text">
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="inputFname">No Invoice Pembayaran
	</label>
	<div class="controls">
		<input onkeyup="check_complain()" id="inv_number" type="text">
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="inputFname">Pesan Komplen
	</label>
	<div class="controls">
		<textarea style="height:30vh;" onkeyup="check_complain()" id="message_complain"></textarea>
	</div>
</div>

<div class="control-group">
	<div class="controls">
    <button id="add-btn-complain" onclick="add_complain()" class="btn btn-warning">Tambah Komplen</button>
	</div>
</div>

<hr>
<?php $index = 0; foreach ($chat as $key => $value) { ?>
<?php if($check_unread[$index] == 0){ ?>
    <div onclick="complain_chat(<?php echo $value['id'] ?>)" class="alert" style="background:white;border:1px solid #f89406;border-radius:5px;cursor:pointer">
<?php }else{?>
    <div onclick="complain_chat(<?php echo $value['id'] ?>)" class="alert" style="background:white;border:1px solid red;border-radius:5px;cursor:pointer">
<?php }?>
    <strong>Judul Komplen (<?php if($value['total_chat']){echo $value['total_chat'];}else{ echo 0;} ?> Chat) </strong> :
	<?php echo $value['title'] ?>
	<br>
	<strong>Nomor Invoice Pembayaran </strong> :
	<?php echo strtoupper($value['inv']) ?>
    <br>
	<?php if($value['status'] == 'close'){ ?>
	<strong>Status </strong> :
	<?php echo strtoupper($value['status']) ?>
	<?php }else{?>
		<strong>Status </strong> :
		AKTIF
	<?php }?>
</div>
<?php $index++; }?>

<script>
	check_complain();

	function complain_chat(id) {
        var chat_url = 'chat_controller/complain_chat/'+id;
        document.cookie = "complain_chat=" + chat_url;
        loadView(chat_url, '.complain');
	}

	function check_complain(){
		var title = $("#complain_title").val();
        var inv = $("#inv_number").val();
        var message = $("#message_complain").val();
		if(title != '' && inv != '' && message != ''){
			$("#add-btn-complain").removeAttr('disabled');
		}else{
			$("#add-btn-complain").attr('disabled', 'disabled');
		}
	}

	function add_complain() {
		data = {
			title: $("#complain_title").val(),
            inv: $("#inv_number").val(),
            message: $("#message_complain").val()
		}

		console.log("data : ", data);
		postData('chat_controller/add_complain', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					hs_menu('#discussion', 'chat_controller/discussion', '#sidebar', '#')
					setTimeout(() => {
						$("#add-complain").show();
					}, 500);
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>