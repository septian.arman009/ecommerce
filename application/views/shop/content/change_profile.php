<div class="span12">
	<ul class="breadcrumb">
		<li class="active">Registrasi</li>
	</ul>
	<div class="well">
		<form class="form-horizontal">
			<h3>Data Pribadi Anda</h3>
			<div class="control-group">
				<label class="control-label" for="inputFname">Nama Lengkap
				</label>
				<div class="controls">
					<input value="<?php echo $members[0]['name'] ?>" onkeyup="check()" id="name" type="text" id="inputFname">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputEmail">Email
				</label>
				<div class="controls">
					<input value="<?php echo $members[0]['email'] ?>" onkeyup="check()" id="email" type="email">
					<p id="invalid_email" style="color:red;display:none;">Masukan format email yang benar (Contoh : @gmail.com) !</p>
					<p id="registered" style="color:red;display:none;">Email sudah terdaftar !</p>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputEmail">
				</label>
				<div class="controls">
					<b>
						<i>Kosongkan jika tidak ingin mengubah password</i>
					</b>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputEmail">Password
				</label>
				<div class="controls">
					<input onkeyup="check_password()" id="password" type="password">
					<p id="passless" style="color:red;display:none;">Minimal password 8 karakter</p>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputEmail">Konfirmasi Password
				</label>
				<div class="controls">
					<input onkeyup="check_password()" id="confirm" type="password">
					<p id="conless" style="color:red;display:none;">Minimal password 8 karakter</p>
					<p id="notmatch" style="color:red;display:none;">Password tidak sama</p>
				</div>
			</div>

			<div class="control-group">
				<div class="controls">
					<a class="exclusive shopBtn" onclick="save()" id="save" style="display:none">Simpan</a>
					<a class="exclusive shopBtn" id="save-error" style="display:none">Lenkapi Data</a>
					<a style="display:none" class="exclusive shopBtn" id="loading">Memproses..</a>
				</div>
			</div>
		</form>

	</div>
</div>

<script>
	check();

	function check() {
		var name = $("#name").val();
		var email = $("#email").val();
		if (name != '' && email != '') {
			if (ValidateEmail(email, '#save', "#save-error", '#invalid_email')) {
				check_email();
			}
		} else {
			$("#save").hide();
			$("#save-error").show();
		}
	}

	function check_password() {
		var password = $("#password").val();
		var confirm = $("#confirm").val();
		if (password == '' && confirm == '') {
			$("#save").show();
            $("#save-error").hide();
            $("#passless").hide();
            $("#conless").hide();
            $("#notmatch").hide();
		} else {
			$("#save").hide();
			$("#save-error").show();
			var passlen = password.length;
			var conlen = confirm.length;

			if (passlen != '') {
				if (passlen < 8) {
					$("#passless").show();
				} else {
					$("#passless").hide();
				}
			} else {
				$("#passless").hide();
			}

			if (confirm != '') {
				if (conlen < 8) {
					$("#conless").show();
				} else {
					$("#conless").hide();
				}
			} else {
                $("#conless").hide();
                $("#notmatch").hide();
            }
            
			if (password == confirm) {
				$("#notmatch").hide();

				if (password.length >= 8 && confirm.length >= 8) {
					$("#save").show();
					$("#save-error").hide();
				} else {
					$("#save").hide();
					$("#save-error").show();
				}
			} else {
				$("#notmatch").show();
				$("#save").hide();
				$("#save-error").show();
			}

		}
	}

	function check_email() {

		var data = {
			id: <?php echo $members[0]['id'] ?>,
			email: $("#email").val()
		}

		postData('shop_controller/check_email_change', data, function (err, response) {
			if (response) {
				console.log("berhasil : ", response);
				var status = response.status;
				if (status == 'success') {
					$("#registered").hide();
					check_password();
				} else {
					$("#registered").show();
					$('#save').hide();
					$('#save-error').show();
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function save() {
		var data = {
            id: <?php echo $members[0]['id'] ?>,
            name: $("#name").val(),
            email: $("#email").val(),
            password: $("#password").val()
		}
		postData('shop_controller/save_profile', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
                    hs_menu('#myaccount','shop_controller/myaccount','#sidebar','#')
                    setTimeout(() => {
                        $("#success-change").show();
                    }, 200);
				}
			} else {
				console.log('ini error : ', err);
			}
		});

	}
</script>