<div class="span9">
	<div class="well np">
		<div id="myCarousel" class="carousel slide homCar">
			<div class="carousel-inner">
				<?php if($slider){?>
				<?php $no = 1; foreach ($slider as $key => $value) { ?>
				<?php if($no ==1){ ?>
				<div class="item active">
					<img style="width:100%" src="<?php echo base_url() ?>assets/slider/<?php echo $value['image'] ?>" alt="bootstrap ecommerce templates">
					<div class="carousel-caption">
						<h4><?php echo $value['description'] ?></h4>
					</div>
				</div>
				<?php }else{?>
					<div class="item">
					<img style="width:100%" src="<?php echo base_url() ?>assets/slider/<?php echo $value['image'] ?>" alt="bootstrap ecommerce templates">
					<div class="carousel-caption">
						<h4><?php echo $value['description'] ?></h4>
					</div>
				</div>
				<?php }?>
				<?php $no++; }}else{?>
					<div class="item active">
					<img style="width:700px;height:330px;" src="<?php echo base_url() ?>assets/no_image.png ?>" alt="bootstrap ecommerce templates">
					<div class="carousel-caption">
						<h4>Tidak ada deskripsi</h4>
					</div>
				</div>
				<?php }?>
				
			</div>
			<a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
		</div>
	</div>
	<div class="alert alert-success" style="display: none"> 
		<a class="close" onclick="$('.alert').hide()">×</a>  
		<strong>Sukses !</strong> Produk berhasil ditambahkan ke keranjang.  
	</div>
	<div class="well well-small">
		<h3>Produk Kami </h3>

		<div class="row-fluid">
			<ul class="thumbnails">
				<?php if($products){ ?>
				<?php $no = 1; foreach ($products as $key => $value) { ?>
				<?php if($no == 4){ ?>
				<li style="margin-left:0%;" class="span4">
					<div class="thumbnail">
						<a>
							<img onclick="shop_menu('#','shop_controller/product_detail/<?php echo $value['id'] ?>')" src="<?php echo base_url() ?>assets/products/<?php echo $value['image'] ?>"
							alt="">
						</a>
						<div class="caption cntr">
							<p>
								<?php echo $value['product_name'] ?>
							</p>
							<p>
								<strong>
									<?php echo $this->mylib->torp($value['unit_price']) ?>
								</strong>
							</p>
							
							<h4>
								<?php if($value['condition'] == 'tersedia' && $value['stock'] > 0){ ?>
								<span class="icon-shopping-cart"></span>
								<a onclick="add_to_cart(<?php echo $value['id'] ?>)" class="shopBtn" title="add to cart"><span class="icon-shopping-cart"></span> Tambah Ke Keranjang </a>
								<?php }else{?>
									<a class="shopBtn" style="background:#fff;color:#F86706;border:1px solid #ccc;" title="add to cart"> Produk Tidak Tersedia </a>
								<?php }?>
							</h4>
							<br class="clr">
						</div>
					</div>
				</li>
				<?php }else{?>
				<li class="span4">
					<div class="thumbnail">
						<a>
							<img onclick="shop_menu('#','shop_controller/product_detail/<?php echo $value['id'] ?>')" src="<?php echo base_url() ?>assets/products/<?php echo $value['image'] ?>"
							alt="">
						</a>
						<div class="caption cntr">
							<p>
								<?php echo $value['product_name'] ?>
							</p>
							<p>
								<strong>
									<?php echo $this->mylib->torp($value['unit_price']) ?>
								</strong>
							</p>
							<h4>
								<?php if($value['condition'] == 'tersedia' && $value['stock'] > 0){ ?>
								<a onclick="add_to_cart(<?php echo $value['id'] ?>)" class="shopBtn" title="add to cart"> <span class="icon-shopping-cart"></span> Tambah Ke Keranjang </a>
								<?php }else{?>
									<a class="shopBtn" style="background:#fff;color:#F86706;border:1px solid #ccc;" title="add to cart"> Produk Tidak Tersedia </a>
								<?php }?>
							</h4>
			
							<br class="clr">
						</div>
					</div>
				</li>
				<?php } $no++;}}else{?>
				<li class="span4">
					<div class="thumbnail">
						<a>
							<img src="<?php echo base_url() ?>assets/no_image.png" alt="">
						</a>
						<div class="caption cntr">
							<p>
								Tidak ada data
							</p>
							<p>
								<strong>
									Rp. 0
								</strong>
							</p>
							<h4>
								<a class="shopBtn" title="add to cart"> Tidak ada data </a>
							</h4>
						
							<br class="clr">
						</div>
					</div>
				</li>
				<?php }?>

			</ul>
			<div class="pull-right">
				<a onclick="prev()" class="btn btn-primary">Sebelumnya</a>
				<?php echo 'Halaman '.$page.' Dari '.$all ?>
				<a onclick="next()" class="btn btn-primary">Selanjutnya</a>
			</div>
		</div>
	</div>
</div>

<script>
	function next() {
		var page = <?php echo $page + 1 ?>;
		var key = $("#search").val();
		key_pass = key.replace(/[^A-Za-z]/g, "");
		if (key_pass == '') {
			key_pass = 'null';
		}

		var categories = $("#product_categories").val();
		if (categories == '') {
			categories = 'null';
		}

		document.cookie = "search=" + key_pass;
		document.cookie = "product_categories=" + categories;

		if (page >= 0) {
			loadView('shop_controller/home/' + page + '/' + key_pass + '/' + categories, '.content');
		}
	}

	function prev() {
		var page = <?php echo $page - 1 ?>;
		var key = $("#search").val();
		key_pass = key.replace(/[^A-Za-z]/g, "");
		if (key_pass == '') {
			key_pass = 'null';
		}

		var categories = $("#product_categories").val();
		if (categories == '') {
			categories = 'null';
		}

		document.cookie = "search=" + key_pass;
		document.cookie = "product_categories=" + categories;

		if (page > 0) {
			loadView('shop_controller/home/' + page + '/' + key_pass + '/' + categories, '.content');
		}
	}

	function add_to_cart(id)
	{	
		do_something('cart_controller/add_to_cart/'+id);
		$('.alert').show();
		setTimeout(() => {
			check_cart();
		}, 500);
	}

</script>