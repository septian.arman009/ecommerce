<div class="span12">
	<ul class="breadcrumb">
		<li class="active">Pilih Bank Transfer</li>
	</ul>
	<div class="well well-small">

		<form class="form-horizontal">
            
            <div class="image">
				<?php foreach ($bank as $key => $value) { ?>
                	<img style="cursor:pointer;background:#ccc;padding:10px;border-radius:10px;" id="bank<?php echo $value['id'] ?>" onclick="choose_bank('#bank<?php echo $value['id'] ?>','<?php echo $value['id'] ?>')" width="200px" style="cursor:pointer;"  src="<?php echo base_url() ?>assets/banks/<?php echo $value['image'] ?>"><br><br>
				<?php }?>
			</div>
    
		</form>

		<div class="row" style="min-height:15vh"></div>

		<a onclick='hs_menu("#checkout","shop_controller/checkout","#sidebar","#")' class="shopBtn btn-large">
			<span class="icon-arrow-left"></span> Sebelumnya</a>
        <a id="hold" class="shopBtn btn-large pull-right">Kili Pada Logo Bank
			<span class="icon-arrow-right"></span>
		</a>
		<a id="next" style="display:none" onclick='shop_menu("#checkout","shop_controller/transfer_detail")' class="shopBtn btn-large pull-right">Selanjutnya
			<span class="icon-arrow-right"></span>
		</a>

	</div>
</div>
<style>
.image{
    margin: auto;
    text-align:center;
}

</style>

<script>
var bank = document.cookie.replace(/(?:(?:^|.*;\s*)bank\s*\=\s*([^;]*).*$)|^.*$/, "$1");
var id_bank = document.cookie.replace(/(?:(?:^|.*;\s*)id_bank\s*\=\s*([^;]*).*$)|^.*$/, "$1");
if(bank && id_bank){
	choose_bank(bank, id_bank);
}

function choose_bank(bank, id_bank){
	var old_bank = document.cookie.replace(/(?:(?:^|.*;\s*)bank\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	document.cookie = "bank="+bank;
	document.cookie = "id_bank="+id_bank;
	$(old_bank).attr('style', 'cursor:pointer;background:#ccc;padding:10px;border-radius:10px;');
    $(bank).attr('style', 'cursor:pointer;background:#f89406;border-radius:10px;padding:10px;');
    $("#hold").hide();
    $("#next").show();
}
</script>
