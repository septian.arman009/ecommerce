<div class="span12">
	<ul class="breadcrumb">
		<li class="active">Diskui, Komplen & Reward</li>
	</ul>
	<div id="add-success" class="alert alert-success" style="display: none">
		<a class="close" onclick="$('#add-success').hide()">×</a>
		<strong>Sukses !</strong> Berhasil menambah diskusi.
	</div>
	<div id="add-complain" class="alert alert-success" style="display: none">
		<a class="close" onclick="$('#add-success').hide()">×</a>
		<strong>Sukses !</strong> Berhasil menambah komplen.
	</div>
	<div class="well well-small">

		<form class="form-horizontal">

			<ul id="productDetail" class="nav nav-tabs">
				<li onclick="save_tab('#discussion-btn')" id="discussion-btn">
					<a href="#discussion-tab" data-toggle="tab">Diskusi (<?php echo $discussion_chat ?>)</a>
				</li>
				<li onclick="save_tab('#complain-btn')" id="complain-btn">
					<a href="#complain-tab" data-toggle="tab">Komplen (<?php echo $complain_chat ?>)</a>
				</li>
				<li onclick="save_tab('#reward-btn')" id="reward-btn">
					<a href="#reward-tab" data-toggle="tab">Reward (<?php echo $reward ?>)</a>
				</li>
			</ul>
			<div id="myTabContent" class="tab-content tabWrapper">

				<div class="tab-pane" id="discussion-tab">
					<div class="discussion"></div>
				</div>

				<div class="tab-pane" id="complain-tab">
					<div class="complain"></div>
				</div>

				<div class="tab-pane" id="reward-tab">
					<div class="reward_list"></div>
				</div>

			</div>

		</form>

		<div class="row" style="min-height:15vh"></div>

	</div>
</div>

<script>
	check_chat();
	check_complain();
	reward();
	var tab_ = document.cookie.replace(/(?:(?:^|.*;\s*)tab_\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	if (tab_) {
		if (tab_ == '#discussion-btn') {
			$(tab_).addClass('active');
			$("#discussion-tab").addClass('active');
			$("#discussion-tab").addClass('fade in');
		} else if (tab_ == '#complain-btn') {
			$(tab_).addClass('active');
			$("#complain-tab").addClass('active');
			$("#complain-tab").addClass('fade in');
		} else if (tab_ == '#reward-btn') {
			$(tab_).addClass('active');
			$("#reward-tab").addClass('active');
			$("#reward-tab").addClass('fade in');
		}
	} else {
		$("#discussion-btn").addClass('active');
		$("#discussion-tab").addClass('active');
		$("#discussion-tab").addClass('fade in');
	}

	function save_tab(id) {
		document.cookie = "tab_=" + id;
	}

	function check_chat() {
		var chat = document.cookie.replace(/(?:(?:^|.*;\s*)chat\s*\=\s*([^;]*).*$)|^.*$/, "$1");
		if (chat) {
			loadView(chat, '.discussion');
		} else {
			loadView('chat_controller/list_chat', '.discussion');
		}
	}

	function check_complain() {
		var complain_chat = document.cookie.replace(/(?:(?:^|.*;\s*)complain_chat\s*\=\s*([^;]*).*$)|^.*$/, "$1");
		if (complain_chat) {
			loadView(complain_chat, '.complain');
		} else {
			loadView('chat_controller/list_complain', '.complain');
		}
	}

	function reward() {
		loadView('chat_controller/reward_list', '.reward_list');
	}
</script>