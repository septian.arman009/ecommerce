<div class="span12">
	<ul class="breadcrumb">
		<li class="active">Pembayaran</li>
	</ul>
	<div id="billing-success" class="alert alert-success" style="display: none">
		<a class="close" onclick="$('.alert').hide()">×</a>
		<strong>Sukses !</strong> Data tagihan belanja berhasil disimpan.
	</div>
	<div id="billing-error" class="alert alert-success" style="display: none">
		<a class="close" onclick="$('.alert').hide()">×</a>
		<strong>Terjadi Kesalahan !</strong> Ada beberapa produk tidak masuk kedalam tagihan karena stock habis, silakan cek keranjang
		belanja.
	</div>

	<div class="well well-small">

		<form class="form-horizontal">

			<ul id="productDetail" class="nav nav-tabs">
				<li onclick="save_tab('#bill-btn')" id="bill-btn">
					<a href="#bill-tab" data-toggle="tab">Tagihan (
						<?php echo $bill ?>)</a>
				</li>
				<li onclick="save_tab('#order-btn')" id="order-btn">
					<a href="#order-tab" data-toggle="tab">Dibayar (
						<?php echo $purchase ?>)</a>
				</li>
			</ul>
			<div id="myTabContent" class="tab-content tabWrapper">
				<div class="tab-pane" id="bill-tab">
					<p>Tagihan akan terhapus secara otomatis bila anda tidak melakukan pembayaran sampai dengan batas waktu yang ditentukan</p>
					<table class="table table-bordered table-condensed">
						<thead>
							<tr>
								<th>No</th>
								<th>Nomor Tagihan</th>
								<th>Total Tagihan</th>
								<th>Kadaluarsa</th>
								<th>Bank Tujuan Transfer</th>
								<th>Upload Bukti Transaksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; $index = 0; foreach ($billing as $key => $value) {?>
							<?php if($value['status'] == 'active'){ ?>
							<tr>
								<td>
									<?php echo $no++; ?>
								</td>
								<td>
									<a style="text-decoration:none;" onclick="detail_inv(<?php echo $value['id'] ?>)">
										<?php echo $value['billing_inv'] ?>
									</a>
								</td>
								<td>
									<?php echo $this->mylib->torp($value['billing_transfer']) ?>
								</td>
								<td>
									<?php echo $value['expired'] ?>
								</td>
								<td>
									<?php echo $receiver_bank[$index][0]['bank_name'] ?>
									<br> a/n
									<?php echo $receiver_bank[$index][0]['owner'] ?>
									<br>No Rek :
									<?php echo $receiver_bank[$index][0]['account'] ?>
								</td>
								<td>
									<input onchange="upload_file(<?php echo $value['id'] ?>)" type="file" name="file<?php echo $value['id'] ?>">
									<br>
									<hr>
									<div class="image" style="cursor:pointer;margin:auto;text-align:center;">
										<?php if($value['p_o_pay'] != ''){ ?>
										<a href="<?php echo base_url() ?>assets/payments/<?php echo $value['p_o_pay'] ?>" target="_blank">
											<img width="100px" src="<?php echo base_url() ?>assets/payments/<?php echo $value['p_o_pay'] ?>" alt="">
										</a>
										<?php }else{?>
										<img width="100px" src="<?php echo base_url() ?>assets/no_image.png" alt="">
										<?php }?>
									</div>
									<br>
									<div id="message<?php echo $value['id'] ?>" class="alert alert-success" style="display:none">
										<a class="close" onclick="$('#message<?php echo $value['id'] ?>').hide()">×</a>
										<strong>Sukses !</strong> Simpan bukti pembayaran berhasil.
									</div>
								</td>
							</tr>
							<?php }else{?>
							<tr>
								<td style="background-color:#ccc;color:red;font-weight:bold;">
									<?php echo $no++; ?>
								</td>
								<td style="text-decoration:none;background-color:#ccc;color:red;font-weight:bold;">
									<a onclick="detail_inv(<?php echo $value['id'] ?>)">
										<?php echo $value['billing_inv'] ?>
									</a>
								</td>
								<td style="background-color:#ccc;color:red;font-weight:bold;">
									<?php echo $value['billing_transfer'] ?>
								</td>
								<td style="background-color:#ccc;color:red;font-weight:bold;">
									<?php echo $value['expired'] ?>
								</td>
								<td style="background-color:#ccc;color:red;font-weight:bold;">
									<?php echo $receiver_bank[$index]['bank_name'] ?>
								</td>
								<td style="background-color:#ccc;color:red;font-weight:bold;">
									<div class="image" style="margin:auto;text-align:center;">

										<img width="200px" src="<?php echo base_url() ?>assets/expired.png" alt="">
									</div>
									<br>

								</td>
							</tr>
							<?php }?>
							<?php  $index++; }?>

						</tbody>
					</table>

				</div>
				<div class="tab-pane" id="order-tab">
					<p>Klik tombol <span style="background:green;color:black;border:none;shadow:none;border-radius:30px;" class="btn btn-default"><span class="icon-dropbox"></span></span> jika barang sudah diterima untuk melakukan konfirmasi</p>
								
					<table class="table table-bordered table-condensed">
						<thead>
							<tr>
								<th>No</th>
								<th>Nomor Invoice Pembayaran</th>
								<th>Jumlah Transfer</th>
								<th>Alamat Pengiriman</th>
								<th style="width:330px">Status</th>
							</tr>
						</thead>
						<tbody>
								<?php $no = 1; $index = 0; foreach ($purchased as $key => $value) { ?>
									<tr>
										<td><?php echo $no++; ?></td>
										<td><?php echo $value['purchasement_inv'] ?></td>
										<td><?php echo $this->mylib->torp($value['transfer_ammount']) ?></td>
										<td><?php echo $address[0]['address'] ?></td>
										<td>
											<div class="center" style="margin:auto;text-align:center;">
											<?php if($value['status'] == 'purchased'){ ?>
												<span style="background:#f89406;color:black;border:none;shadow:none;border-radius:30px;" class="btn btn-warning"><span class="icon-money"></span></span>
												<span style="background:transparent;color:black;border:none;shadow:none;border-radius:30px;" class="btn btn-default"><span class="icon-truck"></span></span>
												<span style="background:transparent;color:black;border:none;shadow:none;border-radius:30px;" class="btn btn-default"><span class="icon-dropbox"></span></span>
												<span style="background:transparent;color:black;border:none;shadow:none;border-radius:30px;" class="btn btn-default"><span class="icon-check"></span></span>
											<?php }else if($value['status'] == 'sent'){?>
												<span style="background:#f89406;color:black;border:none;shadow:none;border-radius:30px;" class="btn btn-default"><span class="icon-money"></span></span>
												<span style="background:#f89406;color:black;border:none;shadow:none;border-radius:30px;" class="btn btn-default"><span class="icon-truck"></span></span>
												<span onclick="confirm(<?php echo $value['id'] ?>)" style="background:green;color:black;border:none;shadow:none;border-radius:30px;" class="btn btn-default"><span class="icon-dropbox"></span></span>
												<span style="background:transparent;color:black;border:none;shadow:none;border-radius:30px;" class="btn btn-default"><span class="icon-check"></span></span>
												<br>
												<p style="margin-top:3%;font-weight:bold;">Resi : <?php echo $value['receipt_number'] ?></p>
											<?php }else if($value['status'] == 'delivered'){?>
												<span style="background:#f89406;color:black;border:none;shadow:none;border-radius:30px;" class="btn btn-default"><span class="icon-money"></span></span>
												<span style="background:#f89406;color:black;border:none;shadow:none;border-radius:30px;" class="btn btn-default"><span class="icon-truck"></span></span>
												<span style="background:#f89406;color:black;border:none;shadow:none;border-radius:30px;" class="btn btn-default"><span class="icon-dropbox"></span></span>
												<span style="background:#f89406;color:black;border:none;shadow:none;border-radius:30px;" class="btn btn-default"><span class="icon-check"></span></span>
												
												<?php if($value['review'] == ''){ ?>	
												<span onclick="review_confirm(<?php echo $value['id'] ?>)" style="font-weight:bold;background:transparent;color:black;border:none;shadow:none;width:303px;margin-top:2%;" class="btn btn-default"><span> Berikan Ulasan</span></span>
												<?php }else{?>
													<?php if($value['satisfaction'] == 'yes'){ ?>
														<span style="background:transparent;color:black;border:none;shadow:none;width:303px;margin-top:2%;" class="btn btn-default"><span class="icon-smile"> Anda memerikan ulasan positif</span></span>
													<?php }else{?>
														<span style="background:transparent;color:red;border:none;shadow:none;width:303px;margin-top:2%;" class="btn btn-default"><span class="icon-thumbs-down"> Anda memerikan ulasan negatif</span></span>														
													<?php }?>
													Ulasan : <?php echo $value['review'] ?>
												<?php }?>
										
												<div id="confirm-success<?php echo $value['id'] ?>" class="alert alert-success" style="display: none">
													<a class="close" onclick="$('.alert').hide()">×</a>
													<strong>Sukses !</strong> Konfirmasi Terima Barang Berhasil, silakan berikan ulasan, Terimakasih :)
												</div>
												<div id="review-success<?php echo $value['id'] ?>" class="alert alert-success" style="display: none">
													<a class="close" onclick="$('.alert').hide()">×</a>
													<strong>Sukses !</strong> Ulasan telah disimpan, Terimakasih :)
												</div>
											<?php }?>
											</div>
											
										</td>
										
									</tr>
								<?php }?>

						</tbody>
					</table>

				</div>
			</div>

		</form>

		<div class="row" style="min-height:15vh"></div>

	</div>
</div>

<script>

	var tab = document.cookie.replace(/(?:(?:^|.*;\s*)tab\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	if(tab){
		if(tab == '#bill-btn'){
			$(tab).addClass('active');
			$("#bill-tab").addClass('active');
			$("#bill-tab").addClass('fade in');
		}else{
			$(tab).addClass('active');
			$("#order-tab").addClass('active');
			$("#order-tab").addClass('fade in');
		}
	}else{
		$("#bill-btn").addClass('active');
		$("#bill-tab").addClass('active');
		$("#bill-tab").addClass('fade in');
	}

	function save_tab(id){
		document.cookie = "tab="+id;
	}

	function confirm(id){	
		$("#myModal").modal('show');
		$("#m-h-text").html('Konfirmasi Terima Barang');
		$("#m-b-text").html('Apakah anda telah mengecek barang ?, jika sudah silakan klik Konfirmasi Terima Barang untuk menyelesaikan transaksi :)');
		$("#m-f-btn-yes").html('Konfirmasi Terima Barang');
		$("#m-f-btn-no").html('No');
		$("#m-f-btn-yes").attr('onclick', 'delivered('+id+')');
	}

	function review_confirm(id){
		$("#myReview").modal('show');
		$("#mr-h-text").html('Ulasan');
		$("#mr-b-text").html('Silakan berikan ulasan barang yang baru saja anda beli untuk mendapatkan banyak keuntungan !');
		$("#mr-f-btn-icon-yes").html(' Puas');
		$("#mr-f-btn-icon-no").html(' Kecewa');
		$("#mr-f-btn-yes").attr('onclick', 'save_review('+id+',"yes")');
		$("#mr-f-btn-no").attr('onclick', 'save_review('+id+',"no")');
	}

	function save_review(id, satisfaction){
		$("#myReview").modal('hide');
		data = {
			id: id,
			satisfaction: satisfaction,
			review: $("#review-text").val()

		}

		console.log("data : ", data);
		postData('shop_controller/save_review', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					hs_menu('#payment', 'shop_controller/payment', '#sidebar', '#');
					setTimeout(() => {
						$("#review-success"+id).show();
					}, 500);
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function delivered(id){
		$("#myModal").modal('hide');
		data = {
			id: id
		}

		console.log("data : ", data);
		postData('shop_controller/delivered', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					hs_menu('#payment', 'shop_controller/payment', '#sidebar', '#');
					setTimeout(() => {
						$("#confirm-success"+id).show();
						setTimeout(() => {
							review_confirm(id);
						}, 500);
					}, 500);
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function review(){
		$("#myModal").modal('show');
	}
	function upload_file(id) {

		var inputFile = $('input[name=file' + id + ']');
		var fileToUpload = inputFile[0].files[0];

		var formData = new FormData();
		formData.append("id", id);
		formData.append("file", fileToUpload);

		$.ajax({
			url: 'shop_controller/save_p_o_pay	',
			type: 'post',
			data: formData,
			processData: false,
			contentType: false,
			success: function () {
				hs_menu('#payment', 'shop_controller/payment', '#sidebar', '#');
				setTimeout(() => {
					$('#message' + id).show();
				}, 500);
			}
		});

	}

	function detail_inv(id) {
		hs_menu("#payment", "shop_controller/detail_inv/" + id, "#sidebar", "#");
	}
</script>