<div class="span12">
	<ul class="breadcrumb">
		<li class="active">Detail Invoice Tagihan</li>
	</ul>
	<div class="well well-small">
    <h3>Alamat Pengiriman</h3>
		<table id="address">
			<?php foreach ($address as $key => $value) { ?>
			<tr>
				<td>Nama Penerima</td>
				<td>:</td>
				<td>
					<?php echo $value['name'] ?>
				</td>
			</tr>

			<tr>
				<td>Telepon</td>
				<td>:</td>
				<td>
					<?php echo $value['phone'] ?>
				</td>
			</tr>

			<tr>
				<td>Alamat</td>
				<td>:</td>
				<td>
					<?php echo $value['address'] ?>
				</td>
			</tr>

			<tr>
				<td>Provinsi</td>
				<td>:</td>
				<td>
					<?php echo $province ?>
				</td>
			</tr>

			<tr>
				<td>Kota</td>
				<td>:</td>
				<td>
					<?php echo $city ?>
				</td>
			</tr>

			<tr>
				<td>Kecamatan</td>
				<td>:</td>
				<td>
					<?php echo $district ?>
				</td>
			</tr>

			<tr>
				<td>Kode Pos</td>
				<td>:</td>
				<td>
					<?php echo $value['postcode'] ?>
				</td>
			</tr>

			<?php }?>
		</table>
		<table class="table table-bordered table-condensed">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Produk</th>
					<th>Qty</th>
					<th>Harga Satuan</th>
					<th>Catatan Pembeli</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<?php $no = 1; $index= 0; foreach ($product_id as $key => $value) { ?>
				<tr>
					<td>
						<?php echo $no++; ?>
					</td>
					<td>
						<div class="image" style="margin:auto;text-align:center;">
							<?php echo $name[$index] ?>
							<br>
							<img width="100px" src="<?php echo base_url() ?>assets/products/<?php echo $product_image[$index] ?>" alt="">
						</div>
					</td>
					<td>
						<?php echo $product_qty[$index] ?>
					</td>
					<td>
						<?php echo $this->mylib->torp($product_price[$index]) ?>
					</td>
					<td>
						<?php echo $buyer_notes[$index] ?>
					</td>
					<td>
						<?php echo $this->mylib->torp($product_subtotal[$index]) ?>
					</td>
				</tr>
				<?php }?>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>
						<b>Total Belanja</b>
					</td>
					<td>
						<?php echo $this->mylib->torp($billing_transfer+$discount_ammount) ?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>
						<b>Diskon (<?php echo $discount ?>%)</b>
					</td>
					<td>
						<?php echo $this->mylib->torp($discount_ammount) ?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>
						<b>Total Transfer</b>
					</td>
					<td>
						<?php echo $this->mylib->torp($billing_transfer) ?>
					</td>
				</tr>
			</tbody>
		</table>

		<a onclick='hs_menu("#payment","shop_controller/payment","#sidebar","#")' class="shopBtn btn-large">
			<span class="icon-arrow-left"></span> Kembali
		</a>

	</div>
</div>

<style>
	#address td {
		padding: 10px;
	}
</style>