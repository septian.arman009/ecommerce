<div class="span12">
	<ul class="breadcrumb">
		<li class="active">Keranjang Belanja</li>
	</ul>
	<div id="del_cart" class="alert alert-success" style="display: none">
		<a class="close" onclick="$('.alert').hide()">×</a>
		<strong>Sukses !</strong> Berhasl hapus dari keranjang.
	</div>
	<div class="well well-small">

		<table class="table table-bordered table-condensed">
			<thead>
				<tr>
					<th>No</th>
					<th>Produk</th>
					<th>Status</th>
					<th>Harga Satuan</th>
					<th>Jumlah</th>
					<th>Catatan Pembeli</th>
					<th>Total</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php  ?>
				<?php $no=1; $total = 0; foreach ($cart as $key => $value) { ?>
				<?php 
                    if($total == 0){
                        $total = $value['subtotal'];
                    }else{
                        $total += $value['subtotal'];
                    }
                    
                    ?>
				<tr>
					<td>
						<?php echo $no++; ?>
					</td>
					<td>
						<img width="100px" src="<?php echo base_url() ?>assets/products/<?php echo $value['image'] ?>">
						<br>
						<?php echo $value['name'] ?>
					</td>
					<td>
						<?php echo $value['condition'] ?>
					</td>
					<td>
						<?php echo $this->mylib->torp($value['price']) ?>
					</td>
					<td>
						<select style="width:80px;" onchange="update_cart($(this).val(), '<?php if($cart_session == 'member'){echo $value['id'];}else{ echo $value['rowid'];} ?>','<?php if($cart_session == 'member'){echo $value['product_id'];}else{ echo $value['id'];} ?>')"
						id="qty<?php if($cart_session == 'member'){echo $value['id'];}else{ echo $value['rowid'];} ?>" class="form-control">
							<?php for($i=1;$i<=20;$i++){ ?>
							<?php if($i == $value['qty']){ ?>
							<option selected value="<?php echo $i ?>">
								<?php echo $i ?>
							</option>
							<?php }else{?>
							<option value="<?php echo $i ?>">
								<?php echo $i ?>
							</option>
							<?php }?>
							<?php }?>
						</select>
						<div id="error<?php if($cart_session == 'member'){echo $value['id'];}else{ echo $value['rowid'];} ?>" class="alert alert-danger"
						style="display: none">
							<a class="close" onclick="$('#error<?php if($cart_session == 'member'){echo $value['id'];}else{ echo $value['rowid'];} ?>').hide()">×</a>
							<strong id="message<?php if($cart_session == 'member'){echo $value['id'];}else{ echo $value['rowid'];} ?>"></strong>
						</div>
					</td>
					<td>
						<textarea onchange="update_cart($('#qty<?php if($cart_session == 'member'){echo $value['id'];}else{ echo $value['rowid'];} ?>').val(), '<?php if($cart_session == 'member'){echo $value['id'];}else{ echo $value['rowid'];} ?>','<?php if($cart_session == 'member'){echo $value['product_id'];}else{ echo $value['id'];} ?>')"
						id="buyer_notes<?php if($cart_session == 'member'){echo $value['id'];}else{ echo $value['rowid'];} ?>" type="text"
						style="width:80%;height:25vh;"><?php if($cart){echo $value['buyer_notes'];} ?></textarea>
					</td>

					<td>
						<?php echo $this->mylib->torp($value['subtotal']) ?>
					</td>
					<td>
						<?php if($this->session->userdata('com_shop')){ ?>
						<a onclick="del_cart(<?php echo $value['id'] ?>)" class="shopBtn">
							<span class="icon-trash shopBtn"></span>
						</a>
						<?php }else{?>
						<a onclick="del_cart('<?php echo $value['rowid'] ?>')" class="shopBtn">
							<span class="icon-trash shopBtn"></span>
						</a>
						<?php }?>
					</td>
				</tr>
				<?php }?>
				<tr>
					<td></td>
					<td>
						<b>Diskon</b>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>
						<div id="discount">Rp. 0</div>
					</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td>
						<b>Total Belanja</b>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>
						<div id="total"><?php echo $this->mylib->torp($total) ?></div>
					</td>
					<td></td>
				</tr>

			</tbody>
		</table>
		<br>
		<table class="table table-bordered">
			<tbody>
				<tr>
					<td>
						<div class="form-inline">
							<?php if($cart){ ?>
							<input onchange="check_voucher()" id="voucher_in" type="text" class="input-medium" placeholder="Kode Voucher">
							<a onclick="check_voucher()" class="shopBtn"> Gunakan</a>
							<p id="unregister" style="color:red;display:none;">Kode voucher tidak terdaftar !</p>
							<p id="expired" style="color:red;display:none;">Kode voucher sudah kadaluarsa !</p>
							<p id="used" style="color:red;display:none;">Kode voucher sudah digunakan !</p>
							<p id="valid"style="color:green;display:none;">Kode voucher valid !</p>
							<?php }?>
						</div>
					</td>
				</tr>

			</tbody>
		</table>

		<div class="row" style="min-height:15vh"></div>

		<a onclick='hs_menu("#side","shop_controller/home/1/null/null","#","#sidebar")' class="shopBtn btn-large">
			<span class="icon-arrow-left"></span> Lanjutkan Belanja
		</a>
		<?php if($cart){ ?>
		<a id="next" onclick='hs_menu("#checkout","shop_controller/checkout","#sidebar","#")' class="shopBtn btn-large pull-right">Selanjutnya
			<span class="icon-arrow-right"></span>
		</a>
		<?php }?>

	</div>
</div>



<script>
	<?php foreach ($cart as $key => $value) { ?>
	check_cart_qty($("#qty<?php if($cart_session == 'member'){echo $value['id'];}else{ echo $value['rowid'];} ?>").val(),
		<?php if($cart_session == 'member'){echo $value['id'];}else{ echo '"'.$value['rowid'].'"';} ?>,
		<?php if($cart_session == 'member'){echo $value['product_id'];}else{ echo $value['id'];} ?>);
	<?php } ?>

	var total = '<?php echo $total_row ?>';
	var row = '<?php echo count($cart) ?>';

	if (total == row) {
		$("#next").show();
	} else {
		$("#next").hide();
	}

	function update_cart(qty, id, product_id) {
		var data = {
			id: id,
			qty: qty,
			product_id: product_id,
			notes: $("#buyer_notes" + id).val()
		}
		console.log('ini data :', data);
		postData('cart_controller/update_cart', data, function (err, response) {
			if (response) {
				console.log('ini berhasil : ', response);
				if (response.status == 'success') {
					hs_menu('#checkout', 'cart_controller/detail_cart', '#sidebar', '#');
					check_cart();
					$("#error" + id).hide();
					$("#message" + id).hide();
					$("#next").show();
				} else {
					$("#error" + id).show();
					$("#message" + id).show();
					$("#message" + id).html('Stock : ' + response.data);
					$("#next").hide();
				}
			} else {
				console.log("ini error : ", err);
			}
		});
	}

	function check_cart_qty(qty, id, product_id) {

		var data = {
			id: id,
			qty: qty,
			product_id: product_id
		}
		console.log('ini data :', data);
		postData('cart_controller/check_cart_qty', data, function (err, response) {
			if (response) {
				console.log('ini berhasil : ', response);
				if (response.status == 'success') {
					return true;
				} else {
					$("#error" + id).show();
					$("#message" + id).show();
					$("#message" + id).html('Stock : ' + response.data);
					return false;
				}
			} else {
				console.log("ini error : ", err);
			}
		});
	}

	var voucher = document.cookie.replace(/(?:(?:^|.*;\s*)voucher\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	if(voucher){
		$("#voucher_in").val(voucher);
		setTimeout(() => {
			check_voucher();
		}, 500);
	}

	function check_voucher() {
		var code =  $("#voucher_in").val();
		var data = {
			code: code
		}
		console.log('ini data :', data);
		postData('cart_controller/check_voucher', data, function (err, response) {
			if (response) {
				console.log('ini berhasil : ', response);
				if (response.status == 'valid') {
					document.cookie = "voucher=" + code;
					var total = <?php echo $total ?>;
					var discount = (total/100)*response.discount;

					var total_fix = total-discount;

					var number_string = discount.toString(),
					sisa    = number_string.length % 3,
					rupiah  = number_string.substr(0, sisa),
					ribuan  = number_string.substr(sisa).match(/\d{3}/g);

					if (ribuan) {
						separator = sisa ? '.' : '';
						rupiah += separator + ribuan.join('.');
					}

					var number_string1 = total_fix.toString(),
					sisa1    = number_string1.length % 3,
					rupiah1  = number_string1.substr(0, sisa1),
					ribuan1  = number_string1.substr(sisa1).match(/\d{3}/g);

					if (ribuan1) {
						separator1 = sisa1 ? '.' : '';
						rupiah1 += separator1 + ribuan1.join('.');
					}
		
					$("#discount").html('Rp. '+rupiah);
					$("#total").html('Rp. '+rupiah1);
					$("#valid").show();
					$("#unregister").hide();
					$("#expired").hide();
					$("#used").hide();

				} else if (response.status == 'unregister') {
					document.cookie = "voucher=";
					var total = <?php echo $total ?>;

					var number_string = total.toString(),
					sisa    = number_string.length % 3,
					rupiah  = number_string.substr(0, sisa),
					ribuan  = number_string.substr(sisa).match(/\d{3}/g);

					if (ribuan) {
						separator = sisa ? '.' : '';
						rupiah += separator + ribuan.join('.');
					}
		
					$("#discount").html('Rp. 0');
					$("#total").html('Rp. '+rupiah);
					$("#valid").hide();
					$("#unregister").show();
					$("#expired").hide();
					$("#used").hide();
					
				} else if (response.status == 'used') {
					document.cookie = "voucher=";
					var total = <?php echo $total ?>;

					var number_string = total.toString(),
					sisa    = number_string.length % 3,
					rupiah  = number_string.substr(0, sisa),
					ribuan  = number_string.substr(sisa).match(/\d{3}/g);

					if (ribuan) {
						separator = sisa ? '.' : '';
						rupiah += separator + ribuan.join('.');
					}
					$("#valid").hide();
					$("#unregister").hide();
					$("#expired").hide();
					$("#used").show();
				} else if (response.status == 'expired') {
					document.cookie = "voucher=";
					var total = <?php echo $total ?>;

					var number_string = total.toString(),
					sisa    = number_string.length % 3,
					rupiah  = number_string.substr(0, sisa),
					ribuan  = number_string.substr(sisa).match(/\d{3}/g);

					if (ribuan) {
						separator = sisa ? '.' : '';
						rupiah += separator + ribuan.join('.');
					}
					$("#valid").hide();
					$("#unregister").hide();
					$("#expired").show();
					$("#used").hide();
				} 
			} else {
				console.log("ini error : ", err);
			}
		});
	}

	function del_cart(id) {

		var data = {
			id: id
		}
		console.log('ini data :', data);
		postData('cart_controller/del_cart', data, function (err, response) {
			if (response) {
				console.log('ini berhasil : ', response);
				if (response.status == 'success') {
					hs_menu('#checkout', 'cart_controller/detail_cart', '#sidebar', '#');
					setTimeout(() => {
						$("#del_cart").show();
						setTimeout(() => {
							check_cart();
						}, 100);
					}, 500);
				}
			} else {
				console.log("ini error : ", err);
			}
		});
	}
</script>