<table class="table table-bordered table-condensed">
	<thead>
		<tr>
			<th>No</th>
			<th>Kode Voucher</th>
			<th>Diskon</th>
			<th>Keterangan</th>
			<th>Berlaku Selama</th>
			<th width="20%">Tanggal Kadaluarsa</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; foreach ($reward as $key => $value) { ?>
		<tr>
			<td>
				<?php echo $no++; ?>
			</td>
			<td>
				<?php echo $value['voucher'] ?>
			</td>
			<td>
				<?php echo $value['discount'] ?>%
			</td>
			<td>
				<?php echo $value['description'] ?>
			</td>
			<td>
				<?php echo $value['expired'] ?> Hari</td>
			<td>
				<?php echo $value['validity'] ?>
			</td>
			<td>
				<?php if($value['status'] == 'terpakai'){ ?>
				<p style="font-weight:bold;color:red;">Sudah Digunakan</p>
				<?php }else if($value['status'] == 'kadaluarsa'){?>
				<p style="font-weight:bold;color:orange;">Kadaluarsa</p>
				<?php }else if($value['status'] == 'tersedia'){?>
				<p style="font-weight:bold;color:green;">Tersedia</p>
				<?php }?>
			</td>
		</tr>
		<?php }?>
	</tbody>
</table>