<div class="span12">
	<ul class="breadcrumb">
		<li class="active">Detail Order</li>
	</ul>
	<div class="well well-small">

		<table class="table table-bordered table-condensed">
			<thead>
				<tr>
					<th>No</th>
					<th>Produk</th>
					<th>Status</th>
                    <th>Harga Satuan</th>
                    <th>Jumlah</th>
					<th>Catatan Pembeli</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<?php  ?>
                <?php $no=1; foreach ($cart as $key => $value) { ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $value['name'] ?></td>
                        <td><?php echo $value['condition'] ?></td>
                        <td><?php echo $this->mylib->torp($value['price']) ?></td>
                        <td><?php echo $value['qty'] ?></td>
						<td><?php echo $value['buyer_notes'] ?></td>
                        <td><?php echo $this->mylib->torp($value['subtotal']) ?></td>
                    </tr>
                <?php }?>
				<tr>
					<td></td>
					<td><b>Kode Unik</b></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td><?php echo $this->mylib->torp($_COOKIE['unik']) ?></td>
                </tr>
				<tr>
					<td></td>
					<td><b>Diskon</b></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td><?php echo $this->mylib->torp($_COOKIE['discount']) ?></td>
                </tr>
                <tr>
					<td></td>
					<td><b>Total Belanja</b></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td><?php echo $_COOKIE['billing_transfer'] ?></td>
                </tr>
				
			</tbody>
		</table>
		<br>
	
		<div class="row"style="min-height:15vh"></div>
		
		<a onclick='hs_menu("#","shop_controller/transfer_detail","#sidebar","#")' class="shopBtn btn-large">
			<span class="icon-arrow-left"></span> Sebelumnya </a>
		<a onclick='save_billing()' class="shopBtn btn-large pull-right">Selesai & Simpan
			<span class="icon-arrow-right"></span>
		</a>

	</div>
</div>

<script>
    function save_billing() {
        
        data = {
			
		}

		console.log("data : ", data);
		postData('shop_controller/save_billing', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					shop_menu("#payment",'shop_controller/payment');
					document.cookie = "bank=";
					document.cookie = "id_bank=";
					document.cookie = "billing_transfer=";
					document.cookie = "unik=0";
					document.cookie = "id_address=";
					document.cookie = "percentage=";
					document.cookie = "discount=";
					document.cookie = "voucher=";
				
					setTimeout(() => {
						check_cart();
						$("#billing-success").show();
					}, 500);
				}else{
					shop_menu("#payment",'shop_controller/payment');
					document.cookie = "bank=";
					document.cookie = "id_bank=";
					document.cookie = "billing_transfer=";
					document.cookie = "unik=0";
					document.cookie = "id_address=";
				
					setTimeout(() => {
						check_cart();
						$("#billing-error").show();
					}, 500);
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>