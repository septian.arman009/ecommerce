<div class="span12">
	<ul class="breadcrumb">
		<li class="active">Transfer Detail</li>
	</ul>
	<div class="well well-small">

		<form class="form-horizontal">
			<div class="detail">
				<p>SILAKAN TRANSFER KE REKENING :</p>
				<p><?php echo  strtoupper($bank[0]['bank_name']) ?></p>
				<p><?php echo $bank[0]['account'] ?> a/n <?php echo $bank[0]['owner'] ?></p>
				<P>SEBESAR</P>
				<p style="font-weight:bold;font-size:32px;"><?php echo $this->mylib->torp($total) ?></p>
			</div>
		</form>

		<div class="row" style="min-height:15vh"></div>

		<a onclick='hs_menu("#checkout","shop_controller/choose_bank","#sidebar","#")' class="shopBtn btn-large">
			<span class="icon-arrow-left"></span> Sebelumnya 
        </a>
  
		<a id="next" onclick='hs_menu("#checkout","shop_controller/detail_order","#sidebar","#")' class="shopBtn btn-large pull-right">Selanjutnya
			<span class="icon-arrow-right"></span>
		</a>

	</div>
</div>

<style>
.detail{
    margin: auto;
    text-align:center;
    color: #f89406;
	font-size: 24px;
}

</style>

<script>
	document.cookie = "percentage="+'<?php echo $percentage ?>';
	document.cookie = "discount="+'<?php echo $discount ?>';
	document.cookie = "billing_transfer="+'<?php echo $this->mylib->torp($total) ?>';
	document.cookie = "unik="+'<?php echo $unik ?>';
</script>
