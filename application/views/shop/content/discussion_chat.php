<div class="chat" style="padding:20px;overflow:auto;overflow-y:scroll;height:50vh;margin-bottom:2%;border:1px solid #f89406;border-radius:5px;">
	<?php foreach ($discussion as $key => $value) { ?>
	<?php if($value['from_email'] == $_SESSION['com_shop']['email']){ ?>
	<div class="alert alert-info">
		<img src="<?php echo base_url() ?>assets/member_image.png" width="30px;">

		<strong> : </strong>
		<?php echo $value['message'] ?>
	</div>
	<?php }else{?>
	<div class="alert alert-warning" style="margin-left:5%;">
		<img src="<?php echo base_url() ?>assets/admin_image.png" width="30px;">
		<strong> : </strong>
		<?php echo $value['message'] ?>
	</div>
	<?php }?>
	<?php } ?>
</div>

<div class="center" style="margin:auto;text-align:center;">

	<div class="control-group">
		<input readonly id="admin" style="width:40%" type="text" value="<?php echo $admin ?>">
	</div>
	<div class="control-group">
		<textarea id="message" style="width:40%;height:20vh;" id="message"></textarea>
    </div>
    <div class="control-group">
	<?php if($status == 'aktif' || $status == 'unread'){ ?>
		<a onclick="return_discussion()" class="btn btn-warning">Kembali</a>
        <a onclick="loadView('chat_controller/discussion_chat/<?php echo $id ?>', '.discussion')" class="btn btn-warning">Refresh</a>
		<a onclick="reply()" class="btn btn-warning">Kirim Pesan</a>
	</div>
	<?php }else{?>
		<a onclick="return_discussion()" class="btn btn-warning">Kembali</a>
		<a class="btn btn-danger">Diskusi Ditutup</a>
	<?php }?>
</div>

<script>
    $(".chat").scrollTop($(".chat")[0].scrollHeight - $(".chat")[0].clientHeight);

	function reply() {
		data = {
			title: '<?php echo $title ?>',
			code: '<?php echo $code ?>',
            crm_admin: $("#admin").val(),
            message: $("#message").val()
		}

		console.log("data : ", data);
		postData('chat_controller/reply', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					loadView('chat_controller/discussion_chat/<?php echo $id ?>', '.discussion');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function return_discussion() {
        var chat_url = 'chat_controller/list_chat';
        document.cookie = "chat=" + chat_url;
        loadView(chat_url, '.discussion');
	}
</script>

