<div class="span12">
	<ul class="breadcrumb">
		<li class="active">Tambah Alamat</li>
	</ul>
	<div class="well">
		<form class="form-horizontal">
			<h3>Alamat Baru</h3>
			<div class="control-group">
				<label class="control-label" for="inputFname">Nama Penerima
				</label>
				<div class="controls">
					<input onkeyup="check()" id="name" type="text" id="inputFname">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputFname">No Telpon
				</label>
				<div class="controls">
					<input onkeyup="check()" id="phone" type="number" id="inputFname">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputFname">Alamat
				</label>
				<div class="controls">
					<textarea onkeyup="check()" id="address" id="inputFname"></textarea>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputFname">Provinsi
				</label>
				<div class="controls">
					<select onchange="get_province($(this).val())" id="province" id="inputFname">
						<option value="">- Pilih Provinsi -</option>
						<?php foreach ($province as $key => $value) { ?>
						<option value="<?php echo $value['id'] ?>">
							<?php echo $value['name'] ?>
						</option>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputFname">Kota / Kabupaten
				</label>
				<div class="controls">
					<select onchange="get_city($(this).val())" id="city" id="inputFname">
						<option value="">- Pilih Kota / Kabupaten -</option>
					</select>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputFname">Kecamatan
				</label>
				<div class="controls">
					<select onkeyup="check()" id="district" id="inputFname">
						<option value="">- Pilih Kecamatan -</option>
					</select>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputFname">Kode Pos
				</label>
				<div class="controls">
					<input onkeyup="check()" id="postcode" type="number" id="inputFname">
				</div>
			</div>

			<div class="control-group">
				<div class="controls">
					<a id="add-btn" style="display:none" onclick="add_address()" class="exclusive shopBtn"> Simpan Alamat </a>
					<a id="add-loading" style="display:none" class="exclusive shopBtn"> Memproses .. </a>
					<a id="add-error" style="display:none" class="exclusive shopBtn"> Isi Form </a>
				</div>
			</div>
		</form>
		<a onclick='hs_menu("#myaccount","shop_controller/myaccount","#sidebar","#")' class="shopBtn btn-large">
			<span class="icon-arrow-left"></span> Kembali
		</a>

	</div>

</div>

<script>
	var id = '<?php echo $id ?>';

	if (id == 'null') {
		check();
	} else {
		detail_address(id);
	}

	function check() {
		var name = $("#name").val();
		var phone = $("#phone").val();
		var address = $("#address").val();
		var province = $("#province").val();
		var city = $("#city").val();
		var district = $("#district").val();
		var postcode = $("#postcode").val();

		if (name != '' && phone != '' && address != '' && province != '' &&
			city != '' && district != '' && postcode != '') {
			$("#add-btn").show();
			$("#add-error").hide();
		} else {
			$("#add-btn").hide();
			$("#add-error").show();
		}

	}

	function get_province(province_id) {
		var url = "<?php echo base_url() ?>shop_controller/add_ajax_city/" + province_id;
		$('#city').load(url);
		return false;
		check();
	}

	function get_city(city_id) {
		var url = "<?php echo base_url() ?>shop_controller/add_ajax_district/" + city_id;
		$('#district').load(url);
		return false;
		chec();
	}


	function add_address() {
		$('#add-btn').hide();
		$('#add-loading').show();
		var data = {
			id: id,
			name: $("#name").val(),
			phone: $("#phone").val(),
			address: $("#address").val(),
			province: $("#province").val(),
			city: $("#city").val(),
			district: $("#district").val(),
			postcode: $("#postcode").val()
		}

		postData('shop_controller/save_address', data, function (err, response) {
			if (response) {
				console.log("berhasil : ", response);
				var status = response.status;
				if (status == 'success') {
					shop_menu('#myaccount', 'shop_controller/myaccount');
					setTimeout(() => {
						$("#success").show();
					}, 500);
				} else {
					$('#add-btn').show();
					$('#add-loading').hide();
					$("#error").show();
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function detail_address(id) {
		check();
		data = {
			id: id,
			table: 'address'
		}
		console.log("data : ", data);
		postData('shop_controller/detail', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					$("#name").val(response.data[0].name);
					$("#phone").val(response.data[0].phone);
					$("#address").val(response.data[0].address);
					$("#postcode").val(response.data[0].postcode);
					$("#province").val(response.data[0].province_id);
					setTimeout(() => {
						get_province($("#province").val());
						setTimeout(() => {
							$("#city").val(response.data[0].city_id);
							setTimeout(() => {
								get_city($("#city").val());
								setTimeout(() => {
									$("#district").val(response.data[0].district_id);
									check();
								}, 500);
							}, 500);
						}, 500);
					}, 500);
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>