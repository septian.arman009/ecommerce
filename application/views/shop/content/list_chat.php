<div class="control-group">
	<label class="control-label" for="inputFname">Judul Diskusi
	</label>
	<div class="controls">
		<input onkeyup="check()" id="discussion_title" type="text">
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="inputFname">Pilih Admin
	</label>
	<div class="controls">
		<select onchange="check()" id="crm_admin">
		<option value="">- Pilih Admin -</option>
			<?php foreach ($admin as $key => $value) {?>
			<option value="<?php echo $value['email'] ?>">
				<?php echo $value['name'] ?>
			</option>
			<?php }?>
		</select>
	</div>
</div>

<div class="control-group">
	<div class="controls">
    <button id="add-btn" onclick="add_discussion()" class="btn btn-warning">Tambah Diskusi</button>
	</div>
</div>

<hr>
<?php $index = 0; foreach ($chat as $key => $value) { ?>
<?php if($check_unread[$index] == 0){ ?>
	<div onclick="discussion_chat(<?php echo $value['id'] ?>)" class="alert" style="background:white;border:1px solid #f89406;border-radius:5px;cursor:pointer">
<?php }else{?>
	<div onclick="discussion_chat(<?php echo $value['id'] ?>)" class="alert" style="background:white;border:1px solid red;border-radius:5px;cursor:pointer">
<?php }?>
	<strong>Judul Diskusi (<?php if($value['total_chat']){echo $value['total_chat'];}else{ echo 0;} ?> Chat) </strong> :
	<?php echo $value['title'] ?>
	<br>
	<?php if($value['status'] == 'close'){ ?>
		<strong>Status </strong> :
		<?php echo strtoupper($value['status']) ?>
	<?php }else{?>
		<strong>Status </strong> :
		AKTIF
	<?php }?>
</div>
<?php $index++; }?>

<script>
	check();
	
	function discussion_chat(id) {
        var chat_url = 'chat_controller/discussion_chat/'+id;
        document.cookie = "chat=" + chat_url;
        loadView(chat_url, '.discussion');
	}

	function check(){
		var title = $("#discussion_title").val();
		var admin = $("#crm_admin").val();
		if(title != '' && admin != ''){
			$("#add-btn").removeAttr('disabled');
		}else{
			$("#add-btn").attr('disabled', 'disabled');
		}
	}

	function add_discussion() {
		data = {
			title: $("#discussion_title").val(),
			crm_admin: $("#crm_admin").val()
		}

		console.log("data : ", data);
		postData('chat_controller/add_discussion', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					hs_menu('#discussion', 'chat_controller/discussion', '#sidebar', '#')
					setTimeout(() => {
						$("#add-success").show();
					}, 500);
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>