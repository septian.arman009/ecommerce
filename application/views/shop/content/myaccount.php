<div class="span12">
	<ul class="breadcrumb">
		<li class="active">Registrasi</li>
	</ul>
	<div id="success" class="alert alert-success" style="display: none">
		<a class="close" onclick="$('#success').hide()">×</a>
		<strong>Sukses !</strong> Simpan alamat berhasil.
	</div>
	<div id="success-change" class="alert alert-success" style="display: none">
		<a class="close" onclick="$('#success-change').hide()">×</a>
		<strong>Sukses !</strong> Ubah profile berhasil.
	</div>
    <div id="deleted" class="alert alert-success" style="display: none">
		<a class="close" onclick="$('#success').hide()">×</a>
		<strong>Sukses !</strong> Hapus alamat berhasil.
	</div>
	<div id="error" class="alert alert-danger" style="display: none">
		<a class="close" onclick="$('#error').hide()">×</a>
		<strong>Terjadi Kesalahan !</strong> Gagal menambahkan alamat.
	</div>
	<div class="well">
		<form class="form-horizontal">
			<h3>Data Pribadi Anda</h3>
			<div class="control-group">
				<label class="control-label" for="inputFname">Nama Lengkap
				</label>
				<div class="controls">
					<input disabled value="<?php echo $members[0]['name'] ?>" onkeyup="check()" id="name" type="text" id="inputFname">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputEmail">Email
				</label>
				<div class="controls">
					<input disabled value="<?php echo $members[0]['email'] ?>" onkeyup="check()" id="email" type="email">
				</div>
			</div>

			<div class="control-group">
				<div class="controls">
					<a onclick="hs_menu('#myaccount','shop_controller/add_adress/null','#sidebar','#')" disbaled class="exclusive shopBtn"> Tambah Alamat </a>
					<a onclick="hs_menu('#myaccount','shop_controller/change_profile/<?php echo $_SESSION['com_shop']['id'] ?>','#sidebar','#')" disbaled class="exclusive shopBtn"> Ubah Profile </a>
				</div>
			</div>
		</form>
		<table class="table table-bordered table-condensed">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Penerima</th>
					<th>Telepon</th>
					<th>Alamat</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
                <?php $no=1; foreach ($address as $key => $value) { ?>
                   <tr> 
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $value['name'] ?></td>
                        <td><?php echo $value['phone'] ?></td>
                        <td><?php echo $value['address'] ?></td>
                        <td><a onclick="shop_menu('#myaccount', 'shop_controller/add_adress/<?php echo $value['id'] ?>')" class="icon-pencil"></a> | <a onclick="del_modal(<?php echo $value['id'] ?>)" class="icon-trash"></a></td>
                   </tr>
                <?php } ?>
			</tbody>
		</table>
	</div>

</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="m-h-text" class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <p id="m-b-text"></p>
      </div>
      <div class="modal-footer">
		<button id="m-f-btn-yes" type="button" class="btn btn-default"></button>
		<button id="m-f-btn-no" type="button" class="btn btn-default" data-dismiss="modal"></button>
      </div>
    </div>

  </div>
</div>

<script>

function del_modal(id){
	$("#myModal").modal('show');
	$("#m-h-text").html('Hapus Alamat');
	$("#m-b-text").html('Apakah anda yakin ingin menghapus alamat ?');
	$("#m-f-btn-yes").html('Ya');
	$("#m-f-btn-no").html('No');
	$("#m-f-btn-yes").attr('onclick', 'destroy('+id+')');
}

function destroy(id){
		$("#myModal").modal('hide');
        var data = {
			id: id
		}
		postData('shop_controller/destroy/address/id', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					
                    hs_menu('#myaccount', 'shop_controller/myaccount','#sidebar','#');
                    setTimeout(() => {
                        $("#deleted").show();
                    }, 500);
				}
			} else {
				console.log('ini error : ', err);
			}
		});
    
}
</script>