<div class="span12">
	<ul class="breadcrumb">
		<li class="active">Alamat Pengiriman</li>
	</ul>
	<div class="well well-small">

		<form class="form-horizontal">
            <div class="control-group">
				<label class="control-label" for="inputFname">Alamat
				</label>
				<div class="controls">
					<select onchange="detail_address($(this).val())" id="address_choosed" id="inputFname">
                        <option value="">- Pilih Alamat -</option>
                        <?php foreach ($address as $key => $value) { ?>
                            <option value="<?php echo $value['id'] ?>"><?php echo $value['name'] ?></option>
                        <?php } ?>
                    </select>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputFname">Nama Penerima
				</label>
				<div class="controls">
					<input disabled id="name" type="text" id="inputFname">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputFname">No Telpon
				</label>
				<div class="controls">
					<input disabled id="phone" type="number" id="inputFname">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputFname">Alamat
				</label>
				<div class="controls">
					<textarea disabled id="address" id="inputFname"></textarea>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputFname">Provinsi
				</label>
				<div class="controls">
                    <input disabled id="province" type="text" id="inputFname">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputFname">Kota / Kabupaten
				</label>
				<div class="controls">
                    <input disabled id="city" type="text" id="inputFname">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputFname">Kecamatan
				</label>
				<div class="controls">
                    <input disabled id="district" type="text" id="inputFname">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="inputFname">Kode Pos
				</label>
				<div class="controls">
					<input disabled onkeyup="check()" id="postcode" type="number" id="inputFname">
				</div>
			</div>

		</form>

		<div class="row" style="min-height:15vh"></div>

		<a onclick='hs_menu("#checkout","cart_controller/detail_cart","sidebar","#")' class="shopBtn btn-large">
			<span class="icon-arrow-left"></span> Sebelumnya 
		</a>
		<a id="hold" class="shopBtn btn-large pull-right">Pilih Alamat
			<span class="icon-arrow-right"></span>
		</a>
		<a id="next" style="display:none" onclick='hs_menu("#checkout","shop_controller/choose_bank","#sidebar","#")' class="shopBtn btn-large pull-right">Selanjutnya
			<span class="icon-arrow-right"></span>
		</a>

	</div>
</div>

<script>
check();
function check(){
    if($("#address_choosed").val() == ''){
		$("#hold").show();
		$("#next").hide();
	}else{
		$("#hold").hide();
		$("#next").show();
	}
}

var id_address = document.cookie.replace(/(?:(?:^|.*;\s*)id_address\s*\=\s*([^;]*).*$)|^.*$/, "$1");
if(id_address){
	detail_address(id_address);
	$("#hold").hide();
	$("#next").show();
}

function detail_address(id) {
	document.cookie = "id_address="+id;
		data = {
			id: id,
			table: 'address'
		}
		console.log("data : ", data);
		postData('shop_controller/detail_address', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					$("#name").val(response.data[0].name);
					$("#phone").val(response.data[0].phone);
                    $("#address").val(response.data[0].address);
                    $("#province").val(response.data[0].province);
                    $("#city").val(response.data[0].city);
                    $("#district").val(response.data[0].district);
					$("#postcode").val(response.data[0].postcode);
					$("#address_choosed").val(id);
					$("#hold").hide();
					$("#next").show();
                }
			} else {
				console.log('ini error : ', err);
			}
		});
	}</script>