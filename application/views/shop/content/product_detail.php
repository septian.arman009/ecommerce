<div class="span9">
	<ul class="breadcrumb">
		<li class="active">Detail Produk</li>
	</ul>
	<div class="alert alert-success" style="display: none">
		<a class="close" onclick="$('.alert').hide()">×</a>
		<strong>Sukses !</strong> Produk berhasil ditambahkan ke keranjang.
	</div>
	<div class="well well-small">
		<div class="row-fluid">
			<div class="span5">
				<div id="myCarousel" class="carousel slide cntr">
					<div class="carousel-inner">

						<div class="item active">
							<a href="#">
								<img src="<?php echo base_url() ?>assets/products/<?php echo $products[0]['image'] ?>" alt="" style="width:100%">
							</a>
						</div>

					</div>
					<!-- <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
					<a class="right carousel-control" href="#myCarousel" data-slide="next">›</a> -->
				</div>
			</div>
			<div class="span7">
				<h3>
					<?php echo $products[0]['product_name'] ?>
				</h3>
				<hr class="soft">

				<form class="form-horizontal qtyFrm">
					<div class="control-group">
						<label class="control-label">
							<span>
								<?php echo $this->mylib->torp($products[0]['unit_price']) ?>
							</span>
						</label>
						<div class="controls">
							<select id="qty" class="span8">
								<?php for($i=1;$i<=100;$i++){ ?>
								<option value="<?php echo $i ?>">
									<?php echo $i ?>
								</option>
								<?php }?>
							</select>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">
							<span>Warna</span>
						</label>
						<div class="controls">
							<label class="control-label">
								<?php $color = explode(',', $products[0]['color']); ?>
								<?php foreach ($color as $key => $value) { ?>
									<span><?php echo strtoupper($value) ?></span><br>
								<?php }?>
							</label>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">
							<span>Ukuran</span>
						</label>
						<div class="controls">
							<label class="control-label">
								<span>
									<?php echo $products[0]['size'] ?>
								</span>
							</label>
							</select>
						</div>
					</div>


					<div class="control-group">
						<label class="control-label">
							<span>Terjual</span>
						</label>
						<div class="controls">
							<label class="control-label">
								<span>90</span>
							</label>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">
							<span>Dilihat</span>
						</label>
						<div class="controls">
							<label class="control-label">
								<span>1090</span>
							</label>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">
							<span>Stock</span>
						</label>
						<div class="controls">
							<label class="control-label">
								<span>
									<?php echo $products[0]['stock'] ?>
								</span>
							</label>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">
							<span>Ketersediaan</span>
						</label>
						<div class="controls">
							<label class="control-label">
								<span>
									<?php if($products[0]['condition'] == 'tersedia'){ ?>
										<span>Tersedia</span>
									<?php }else{?>
										<span>Tidak Tersedia</span>
									<?php }?>
								</span>
							</label>
						</div>
					</div>

					<p>
						<?php if($products[0]['condition'] == 'tersedia' && $products[0]['stock'] > 0){ ?>
						<a onclick="add_to_cart_qty(<?php echo $products[0]['id'] ?>)" class="shopBtn">
							<span class="icon-shopping-cart shopBtn"></span> Tambah Ke Keranjang</a>
						<?php }else{?>
						<a style="background:#fff;color:#F86706;border:1px solid #ccc;" class="shopBtn">
							<span class="icon-shopping-cart"></span> Produk Tidak Tersedia</a>
						<?php }?>
					</p>
				</form>
			</div>
		</div>
		<hr class="softn clr">

		<ul id="productDetail" class="nav nav-tabs">
			<li class="active">
				<a href="#deskripsi" data-toggle="tab">Deskripsi</a>
			</li>
			<li class="">
				<a href="#related" data-toggle="tab">Produk Sejenis </a>
			</li>
		</ul>
		<div id="myTabContent" class="tab-content tabWrapper">
			<div class="tab-pane fade active in" id="deskripsi">
				<p>
					<?php echo $products[0]['description'] ?>
				</p>

			</div>
			<div class="tab-pane fade" id="related">
				<?php foreach ($related as $key => $value) { ?>
				<div class="row-fluid">
					<div class="span2">
						<img src="<?php echo base_url() ?>assets/products/<?php echo $value['image'] ?>" alt="">
					</div>
					<div class="span6">
						<h5>
							<?php echo $value['product_name'] ?>
						</h5>
						<p>
							<?php echo $value['description'] ?>
						</p>
					</div>
					<div class="span4 alignR">
						<form class="form-horizontal qtyFrm">
							<h3>
								<?php echo $this->mylib->torp($value['unit_price']) ?>
							</h3>

							<br>
							<div class="btn-group">
								<a onclick="add_to_cart(<?php echo $value['id'] ?>)" class="defaultBtn">
									<span class=" icon-shopping-cart"></span> Tambah</a>
								<a onclick="shop_menu('#','shop_controller/product_detail/<?php echo $value['id'] ?>')" class="shopBtn">VIEW</a>
							</div>
						</form>
					</div>
				</div>
				<hr class="soft">
				<?php }?>

			</div>
		</div>
	</div>

	<script>
		function add_to_cart(id) {
			do_something('cart_controller/add_to_cart/' + id);
			$('.alert').show();
			setTimeout(() => {
				check_cart();
			}, 500);
		}

		function add_to_cart_qty(id) {
			var qty = $("#qty").val();

			do_something('cart_controller/add_to_cart_qty/' + id + '/' + qty);
			$('.alert').show();
			setTimeout(() => {
				check_cart();
			}, 500);
		}
	</script>