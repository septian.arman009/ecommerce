<div class="span12">
	<ul class="breadcrumb">
		<li class="active">Lupa Password</li>
	</ul>
	<div id="success" class="alert alert-success" style="display: none">
		<a class="close" onclick="$('#success').hide()">×</a>
		<strong>Sukses !</strong> Link reset password berhasil dikirim, silakan check email anda.
	</div>

	<div class="well">
		<form class="form-horizontal">
			<h3>Masukan Email Anda</h3>
			
			<div class="control-group">
				<label class="control-label" for="inputEmail">Email
				</label>
				<div class="controls">
					<input onkeyup="check()" id="email" type="email">
					<p id="invalid_email" style="color:red;display:none;">Masukan format email yang benar (Contoh : @gmail.com) !</p>
					<p id="unregister" style="color:red;display:none;">Email tidak terdaftar !</p>
				</div>
			</div>

			<div class="control-group">
				<div class="controls">
					<a onclick="send_token()" id='save' style="display:none" disbaled class="exclusive shopBtn"> Kirim Token </a>
					<a id='save-loading' style="display:none" class="exclusive shopBtn"> Memproses .. </a>
					<a id='save-error' class="exclusive shopBtn"> Isi email dengan benar </a>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
	check();

	function check() {
		var name = $("#name").val();
		var email = $("#email").val();

        if (email != '') {
            if (ValidateEmail(email, '#save', "#save-error", '#invalid_email')) {
                check_email();
            }
        }else{
            $("#invalid_email").hide();
        }
    }

	function check_email() {

		var data = {
			email: $("#email").val()
		}

		postData('shop_controller/check_email_forgot', data, function (err, response) {
			if (response) {
				console.log("berhasil : ", response);
				var status = response.status;
				if (status == 'success') {
					$("#unregister").hide();
					$('#save').show();
					$('#save-error').hide();
				} else {
					$("#unregister").show();
					$('#save').hide();
					$('#save-error').show();
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function ValidateEmail(mail, btn, btn2, id_msg) {
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
			$(id_msg).hide();
			$(btn).show();
			$(btn2).hide();
			return true;
		}
		$(id_msg).show();
		$(btn).hide();
		$(btn2).show();
	}

	function send_token() {
		$('#save').hide();
		$('#save-loading').show();
		var data = {
			email: $("#email").val()
		}

		postData('shop_controller/send_token', data, function (err, response) {
			if (response) {
				console.log("berhasil : ", response);
				var status = response.status;
				if (status == 'success') {
					hs_menu('#', 'shop_controller/forgot_password','#sidebar','#');
					setTimeout(() => {
						$('#success').show();
					}, 500);
				} else {
					$('#save').show();
					$('#save-loading').hide();
					$('#error').show();
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>