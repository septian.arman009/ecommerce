<?php $this->load->view('auth/layout/header')?>
<div class="login-box animated fadeInDown">
	<!-- <div class="login-logo"></div> -->
	<div class="login-body">
		<div class="login-title">
			<strong>Admin Rumah Tiedye</strong>
		</div>
		<form class="form-horizontal">
			<div class="form-group">
				<div class="col-md-12">
					<input id="email" type="email" class="form-control" placeholder="Email" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<input id="password" type="password" class="form-control" placeholder="Password" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6">
					<a href="<?php echo base_url() ?>forgot_password" class="btn btn-link btn-block">Lupa password ?</a>
				</div>
				<div class="col-md-6">
					<a onclick="signin()" class="btn btn-info btn-block">Signin</a>
				</div>
			</div>
		</form>
	</div>
	<div class="login-footer">
		<div class="pull-left">
			&copy; 2018 Rumah Tiedya
		</div>
		<!-- <div class="pull-right">
			septian.arman009@gmail.com
		</div> -->
	</div>
</div>

<!-- failed -->
<button style="display:none" id="failed" type="button" class="btn btn-danger mb-control" data-box="#message-box-sound-2"></button>
<div class="message-box message-box-danger animated fadeIn" data-sound="fail" id="message-box-sound-2">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-times"></span> Terjadi Kesalahan </div>
			<div class="mb-content">
				<p>Signin gagal, silakan cek username dan password, kemudian coba kembali !</p>
			</div>
			<div class="mb-footer">
				<button class="btn btn-default btn-lg pull-right mb-control-close">Tutup</button>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('auth/layout/footer')?>

<script>
function signin() {

var data = {
	email: $("#email").val(),
	password: $("#password").val()
};

console.log("data :", data);
postData("auth_controller/signin_proccess", data, function (err, response) {
	if (response) {
		console.log("response : ", response);
		if (response.status == "success") {
			window.location = "admin";
		} else {
			failed.click();
		}
	}
});
}
</script>