<?php $this->load->view('auth/layout/header')?>
<div class="login-box animated fadeInDown">
	<!-- <div class="login-logo"></div> -->
	<div class="login-body">
		<div class="login-title">
			<strong>Ketikan Password Baru</strong>
		</div>
		<form class="form-horizontal">
			<div class="form-group">
				<div class="col-md-12">
					<input onkeyup="check_password()" id="password" type="password" class="form-control" placeholder="Password" />
					<p id="passless" class="btn btn-link btn-block" style="display:none;color:white;">Minimal password 8 karakter !</p>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-12">
					<input onkeyup="check_password()" id="confirm" type="password" class="form-control" placeholder="Ulangi Password" />
					<p id="conless" class="btn btn-link btn-block" style="display:none;color:white;">Minimal password 8 karakter !</p>
					<p id="notmatch" class="btn btn-link btn-block" style="display:none;color:white;">Password tidak sama !</p>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6">
					<a href="<?php echo base_url() ?>signin" class="btn btn-link btn-block">Kembali ke sign</a>
				</div>
				<div class="col-md-6">
					<a id="reset_password" onclick="reset()" class="btn btn-info btn-block">Atur Ulang</a>
					<a style="display:none" id="loading" class="btn btn-info btn-block">Mereset ..</a>
				</div>
			</div>

		</form>
	</div>
	<div class="login-footer">
		<div class="pull-left">
			&copy; 2018 Rumah Tiedya
		</div>
		<!-- <div class="pull-right">
			septian.arman009@gmail.com
		</div> -->
	</div>
</div>

<button style="display:none" id="failed" type="button" class="btn btn-danger mb-control" data-box="#message-box-sound-2">Fail</button>
<div class="message-box message-box-danger animated fadeIn" data-sound="fail" id="message-box-sound-2">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-times"></span> Terjadi Kesalahan </div>
			<div class="mb-content">
				<p>Gagal mengatur ulang password !</p>
			</div>
			<div class="mb-footer">
				<button class="btn btn-default btn-lg pull-right mb-control-close">Tutup</button>
			</div>
		</div>
	</div>
</div>

<button id="success" style="display:none" class="btn btn-default mb-control" data-box="#message-box-success"></button>
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-check"></span> Sukses </div>
			<div class="mb-content">
				<p>Password telah diatur ulang !</p>
			</div>
			<div class="mb-footer">
				<button class="btn btn-default btn-lg pull-right mb-control-close">Tutup</button>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('auth/layout/footer')?>

<script>
	check_password();

	function reset() {
		$("#reset_password").hide();
		$("#loading").show();
		var data = {
			email: '<?php echo $email ?>',
			password: $("#password").val()
		};

		console.log("data :", data);
		postData("<?php echo base_url() ?>auth_controller/reset", data, function (err, response) {
			if (response) {
				console.log("response : ", response);
				if (response.status == "success") {
					success.click();
					setTimeout(() => {
						window.location = '<?php echo base_url() ?>signin';
					}, 1000);
				} else {
					failed.click();
				}
			}
		});
	}

	function check_password() {
		var password = $("#password").val();
		var confirm = $("#confirm").val();

		var passlen = password.length;
		var conlen = confirm.length;

		if (passlen != '') {
			if (passlen < 8) {
				$("#passless").show();
			} else {
				$("#passless").hide();
			}
		} else {
			$("#passless").hide();
		}

		if (confirm != '') {
			if (conlen < 8) {
				$("#conless").show();
			} else {
				$("#conless").hide();
			}
		} else {
			$("#conless").hide();
		}


		if (password != '' && confirm != '') {

			if (password == confirm) {
				$("#notmatch").hide();

				if (password.length >= 8 && confirm.length >= 8) {
					$("#reset_password").removeAttr('disabled');
				} else {
					$("#reset_password").attr('disabled', 'disabled');
				}
			} else {
				$("#notmatch").show();
				$("#reset_password").attr('disabled', 'disabled');
			}

		} else {
			$("#reset_password").attr('disabled', 'disabled');
		}
	}
</script>