<?php $this->load->view('auth/layout/header')?>
<div class="login-box animated fadeInDown">
	<!-- <div class="login-logo"></div> -->
	<div class="login-body">
		<div class="login-title">
			<strong>Ketikan email anda</strong>
		</div>
		<form id="email_form" class="form-horizontal">
			<div class="form-group">
				<div class="col-md-12">
					<input onkeyup="check()" id="email" type="email" class="form-control" placeholder="Email" />
					<p id="invalid_email" class="btn btn-link btn-block" style="display:none;color:white;">Masukan format email yang benar (Contoh : @gmail.com) !</p>
					<p id="notexist" class="btn btn-link btn-block" style="display:none;color:white;">Email tidak terdaftar !</p>
				</div>

			</div>
			<div class="form-group">
				<div class="col-md-6">
					<a href="<?php echo base_url() ?>signin" class="btn btn-link btn-block">Kembali ke sign</a>
				</div>
				<div class="col-md-6">
					<a disabled id="send_token" onclick="send_token()" class="btn btn-info btn-block">Kirim Token</a>
					<a style="display:none" id="loading" class="btn btn-info btn-block">Mengirim ..</a>
				</div>
			</div>
		</form>
	</div>
	<div class="login-footer">
		<div class="pull-left">
			&copy; 2018 Rumah Tiedya
		</div>
		<!-- <div class="pull-right">
			septian.arman009@gmail.com
		</div> -->
	</div>
</div>

<button style="display:none" id="failed" type="button" class="btn btn-danger mb-control" data-box="#message-box-sound-2"></button>
<div class="message-box message-box-danger animated fadeIn" data-sound="fail" id="message-box-sound-2">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-times"></span> Terjadi Kesalahan </div>
			<div class="mb-content">
				<p>Gagal mengirim token_name !</p>
			</div>
			<div class="mb-footer">
				<button class="btn btn-default btn-lg pull-right mb-control-close">Tutup</button>
			</div>
		</div>
	</div>
</div>

<button id="success" style="display:none" class="btn btn-default mb-control" data-box="#message-box-success"></button>
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-check"></span> Sukses </div>
			<div class="mb-content">
				<p>Berhasil mengirim link reset password !</p>
			</div>
			<div class="mb-footer">
				<button class="btn btn-default btn-lg pull-right mb-control-close">Close</button>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('auth/layout/footer')?>

<script>
	check();

	function check() {
		var email = $("#email").val();

		if (email != '') {

			if (ValidateEmail(email, "#send_token", "#invalid_email")) {
				check_email();
			}

		} else {
			$("#invalid_email").hide();
			$("#send_token").attr('disabled', 'disabled');
		}
	}

	function check_email() {

		var data = {
			email: $("#email").val()
		}

		console.log('data', data);
		postData('auth_controller/check_email', data, function (err, response) {

			if (response) {
				console.log("berhasil : ", response);
				var status = response.status;
				if (status == 'success') {
					$("#notexist").hide();
					$('#send_token').removeAttr('disabled');
				} else {
					$("#notexist").show();
					$("#send_token").attr('disabled', 'disabled');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function send_token() {
		$("#send_token").hide();
		$("#loading").show();
		var data = {
			email: $("#email").val()
		};

		console.log("data :", data);
		postData("auth_controller/send_token", data, function (err, response) {

			if (response) {
				console.log("response : ", response);
				if (response.status == "success") {
					document.getElementById('email_form').reset();
					$("#send_token").show();
					$("#loading").hide();
					success.click();
				} else {
					$("#send_token").show();
					$("#loading").hide();
					failed.click();
				}
			}
		});
	}
</script>