<ul class="breadcrumb">
	<li>
		<a href="#">Member</a>
	</li>

	<li class="active">Perbarui Member</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">

			<form class="form-horizontal" id="member_form">
				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 class="panel-title">
							<strong>Perbarui</strong> Pengguna</h3>
					</div>
					<button onclick="main_menu('#member','crm_controller/member')" class="btn btn-default">
						<i class="fa fa-arrow-left"></i>
					</button>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">

								<div class="form-group">
									<label class="col-md-3 control-label">Nama</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-pencil"></span>
											</span>
											<input onkeyup="check()" id="name" type="text" class="form-control">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label">Email</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-envelope-o"></span>
											</span>
											<input onkeyup="check()" id="email" type="email" class="form-control">
										</div>
										<span class="help-block" style="display:none;" id="invalid_email">Masukan format email yang benar (Contoh : @gmail.com) !</span>
										<span class="help-block" style="display:none;" id="registered">Email sudah terdaftar !</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label">Role</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-key"></span>
											</span>
											<?php if($_SESSION['com_in']['role'] == 4){ ?>
											<select onchange="check()" class="form-control" id="role">
												<?php }else{?>
												<select disabled class="form-control" id="role">
													<?php }?>
													<?php foreach ($roles as $key => $value) { ?>
													<option value="<?php echo $value['id'] ?>">
														<?php echo $value['display_name'] ?>
													</option>
													<?php }?>
												</select>
										</div>

									</div>
								</div>

							</div>
						</div>
					</div>

					<div class="panel-footer">
						<a onclick="reset()" class="btn btn-default">Bersihkan Data</a>
						<a disabled id="save" onclick="action()" class="btn btn-primary pull-right">Simpan</a>
						<a style="display:none" id="loading" class="btn btn-primary pull-right">Tunggu ..</a>
					</div>

				</div>
			</form>

		</div>
	</div>
</div>

<script>
    var id = <?php echo $id ?>;
	detail(id);

	function reset() {
		_('member_form').reset();
		check();
	}

	function check() {
		var name = $("#name").val();
		var email = $("#email").val();
		if (name != '' && email != '') {
			if (ValidateEmail(email, "#save", "#invalid_email")) {
				check_email();
			}

		} else {
			$("#invalid_email").hide();
			$("#save").attr('disabled', 'disabled');
		}
	}

	function check_email() {

		var data = {
			id: id,
			table: 'members',
			email: $("#email").val()
		}

		console.log('data', data);
		postData('main_controller/check_email', data, function (err, response) {
			if (response) {
				console.log("berhasil : ", response);
				var status = response.status;
				if (status == 'success') {
					$("#registered").hide();
					role();
				} else {
					$("#registered").show();
					$("#save").attr('disabled', 'disabled');
				}
			} else {
				console.log('ini error : ', err);
			}
		});

	}

	function role() {
		var role = <?php echo $_SESSION['com_in']['role'] ?>;

		if (role == 4 || role == 3) {
			$('#save').removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function action() {
		$("#save").hide();
		$("#loading").show();
		if (id == 'null') {
			var message = 'Data telah disimpan !';
			var error = 'Gagal menyimpan data !';
		} else {
			var message = 'Data berhasil diubah !';
			var error = 'Gagal mengubah data !';
		}

		var data = {
			id: id,
			name: $('#name').val(),
			email: $('#email').val(),
			role: $('#role').val()
		}

		console.log('data', data);
		postData('crm_controller/save', data, function (err, response) {
			if (response) {
				console.log("berhasil : ", response);
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					btn_s.click();
					$("#message-s").html(message);
					var role = <?php echo $_SESSION['com_in']['role'] ?>;
					if (role == 4) {
						main_menu('#member', 'crm_controller/member');
					}
				} else {
					$("#save").show();
					$("#loading").hide();
					btn_e.click();
					$("#message-e").html(error);
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function detail(id) {
		data = {
			id: id,
			table: 'members'
		}
		console.log("data : ", data);
		postData('main_controller/detail', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					$("#name").val(response.data[0].name);
					$("#email").val(response.data[0].email);
					$("#role").val(response.data[0].role);
					check();
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>