<ul class="breadcrumb">
	<li>
		<a href="#">Member</a>
	</li>
</ul>

<div class="page-content-wrap">

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Member</h3>
				</div>
				<div class="panel-body">
					<table id="member-table" class="table stripe hover">
						<thead>
							<tr>
								<th id="th" width="10%">ID</th>
								<th id="th">Nama</th>
								<th id="th">Email</th>
								<th id="th" class="no-sort" width="8%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">ID</th>
								<th class="footer">Nama</th>
								<th class="footer">Email</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->
		</div>
	</div>
</div>

<script>
	$(document).ready(function () {
		//Fungsi untuk kolom pencarian pada footer table
		$('#member-table tfoot th').each(function () {
			var title = $(this).text();
			var inp = '<input type="text" class="form-control footer-s" placeholder="' + title + '" />';
			$(this).html(inp);
		});

		//Fungsi untuk memanggil datatables di controller localhost/JST/tabel/datatables_member
		//dan ditampilkan ke dalam tabel dengan id member_view
		var table = $('#member-table').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": 'crm_controller/show_member',
				"type": "POST"
			}
		});

		//Fungsi ketika kolom pencarian terisi oleh data
		table.columns().every(function () {
			var that = this;
			$('input', this.footer()).on('keyup change', function () {
				if (that.search() !== this.value) {
					that.search(this.value).draw();
				}
			});
		});
	});

	function destroy(id) {
		btn_d_c.click();
		$("#message-d-c").html("Apakah anda yakin ingin menghapus Member dengan ID : " + id + " ?");
		$("#yes-d-c").attr("onclick", "do_delete(" + id + ")");
	}

	function do_delete(id) {
		var data = {
			id: id
		}
		postData('main_controller/destroy/members/id/', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					loadView('crm_controller/member', '.content');
					btn_s.click();
					$("#message-s").html('members has been deleted !');
				} else {
					btn_e.click();
					$("#message-e").html('Failed to delete member !');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function edit(id) {
		main_menu('#member','crm_controller/form/'+id);
	}
</script>

<style>
	#member-table_filter {
		display: none;
	}
</style>