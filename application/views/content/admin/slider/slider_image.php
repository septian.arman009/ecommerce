<ul class="breadcrumb">
	<li>
		<a href="#">Toko</a>
	</li>
	<li class="active">Slider Utama</li>
</ul>

<div class="page-content-wrap">

	<div class="row">
		<div class="col-md-12">

			<form class="form-horizontal">

				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 class="panel-title">
							<strong>Slider</strong> Utama</h3>
					</div>


					<div class="panel-body">

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Nama Slide</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-pencil"></span>
									</span>
									<input onkeyup="check()" id="slide_name" type="text" class="form-control">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Deskripsi</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-align-left"></span>
									</span>
									<input onkeyup="check()" id="description" type="text" class="form-control">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Pilih Gambar</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-picture-o"></span>
									</span>
									<input type="file" name="file" class="form-control">
								</div>
								<span class="help-block">Silakan gunakan gambar 700 x 330 untuk keindahan tampilan</span>
							</div>
						</div>

						<table id="user-table" class="table stripe hover">
							<thead>
								<tr>
									<th id="th" width="5%">No</th>
									<th id="th" width="20%">Nama Slide</th>
									<th id="th" width="30%">Deskripsi</th>
									<th id="th" class="no-sort">File (Gambar)</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach ($slider as $key => $value) { ?>
								<tr>
									<td>
										<?php echo $no++;?>
									</td>
									<td>
										<input id="slide_name<?php echo $value['id'] ?>" class="form-control" type="text" value="<?php echo $value['slide_name']?>">
									</td>
									<td>
                                        <input id="description<?php echo $value['id'] ?>" class="form-control" type="text" value="<?php echo $value['description']?>">										
									</td>
									<td>
										<div class="input-group">
											<input name="file<?php echo $value['id'] ?>" type="file" class="form-control">
											<span class="input-group-addon">
												<a style="color:white;text-decoration:none;" target="_blank" href="<?php echo base_url() ?>assets/slider/<?php echo $value['image'] ?>">Look</a>
											</span>
											<span id="upload_image<?php echo $value['id'] ?>" class="input-group-addon">
												<a style="color:white;text-decoration:none;" onclick="update_image(<?php echo $value['id'] ?>)">Update</a>
											</span>
											<span style="display:none" id="upload_loading_image<?php echo $value['id'] ?>" class="input-group-addon">
												<a style="color:white;text-decoration:none;">Wait ..</a>
											</span>
											<span class="input-group-addon">
												<a style="color:white;text-decoration:none;" onclick="destroy(<?php echo $value['id'] ?>)">Delete</a>
											</span>
										</div>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>

					</div>

					<div class="panel-footer">
						<a disabled id="save" onclick="save_image()" class="btn btn-primary pull-right">Simpan</a>
						<a style="display:none" id="loading" class="btn btn-primary pull-right">Tunggu ..</a>
					</div>

				</div>

			</form>

		</div>
	</div>

</div>


<script>
	function check() {
		var slide_name = $("#slide_name").val();
		var description = $("#description").val();

		if (slide_name != '' && description != '') {
			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function save_image() {
		$("#save").hide();
		$("#loading").show();
		var inputFile = $('input[name=file]');
		var fileToUpload = inputFile[0].files[0];

		var formData = new FormData();
		formData.append("id", 'null');
		formData.append("slide_name", $("#slide_name").val());
		formData.append("description", $("#description").val());
		formData.append("file", fileToUpload);

		if (fileToUpload) {
			$.ajax({
				url: 'content_controller/save_slide/',
				type: 'post',
				data: formData,
				processData: false,
				contentType: false,
				success: function () {
					$("#save").show();
					$("#loading").hide();
					btn_s.click();
					$("#message-s").html('Slide telah disimpan !');
					sub_menu('#shop', '#slider-image', 'content_controller/slider_image');
				}
			});
		}else{
            $("#save").show();
			$("#loading").hide();
            btn_e.click();
			$("#message-e").html('File gambar belum dipilih !');
        }

	}

    function update_image(id) {
		$("#upload_image"+id).hide();
		$("#upload_loading_image"+id).show();

		var inputFile = $('input[name=file'+id+']');
		var fileToUpload = inputFile[0].files[0];

        var slide_name = $("#slide_name"+id).val();
        var description = $("#description"+id).val();

		var formData = new FormData();
		formData.append("id", id);
		formData.append("slide_name", slide_name);
		formData.append("description", description);
		formData.append("file", fileToUpload);

		if (slide_name != '' && description != '') {
			$.ajax({
				url: 'content_controller/save_slide/',
				type: 'post',
				data: formData,
				processData: false,
				contentType: false,
				success: function () {
					$("#upload_image"+id).show();
	            	$("#upload_loading_image"+id).hide();
					btn_s.click();
					$("#message-s").html('Slide telah disimpan !');
					sub_menu('#shop', '#slider-image', 'content_controller/slider_image');
				}
			});
		}else{
            $("#upload_image"+id).show();
	        $("#upload_loading_image"+id).hide();
            btn_e.click();
			$("#message-e").html('Data tidak boleh kosong !');
        }

	}

    
	function destroy(id) {
		btn_d_c.click();
		$("#message-d-c").html("Apakah anda yakin ingin mengapus slider dengan ID : " + id + " ?");
		$("#yes-d-c").attr("onclick", "do_delete(" + id + ")");
	}

	function do_delete(id) {
		var data = {
			id: id
		}
		postData('main_controller/destroy/slider/id/', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					btn_s.click();
					$("#message-s").html('Slider telah di hapus!');
					sub_menu('#shop', '#slider-image', 'content_controller/slider_image');
				} else {
					btn_e.click();
					$("#message-e").html('Gagal menghapus slider !');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>