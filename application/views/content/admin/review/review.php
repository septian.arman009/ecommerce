<ul class="breadcrumb">
	<li>
		<a href="#">Review Pelanggan</a>
	</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div style="margin-bottom:1%;" class="panel-heading ui-draggable-handle">
					<h3 class="panel-title">
						<strong>Grafik Kepuasan Pelanggan</strong> Tahun
						<?php echo $year ?>
					</h3>
				</div>

				<div class="panel-body">
					<div id="g_review"></div>
					<div class="row" style="margin;auto;text-align:center;">
						<div class="col-md-4">

							<div style="background:#7cb5ec" class="widget widget-item-icon">
								<div class="widget-item-left">
									<span class="fa fa-user"></span>
								</div>
								<div class="widget-data">
									<div class="widget-int num-count">
									<?php if($yes){echo $yes;}else{echo 0;} ?>
									</div>
									<div class="widget-title">Pelanggan Puas</div>
									<div class="widget-subtitle">Di
										<?php echo $yes_transaction ?> Transaksi</div>
								</div>

							</div>

						</div>

						<div class="col-md-4">

							<div style="background:#434348" class="widget widget-item-icon">
								<div class="widget-item-left">
									<span class="fa fa-user"></span>
								</div>
								<div class="widget-data">
									<div class="widget-int num-count">
									<?php if($no){echo $no;}else{echo 0;} ?>
									</div>
									<div class="widget-title"> Pelanggan Kecewa</div>
									<div class="widget-subtitle">Di
										<?php echo $no_transaction ?> Transaksi</div>
								</div>

							</div>

						</div>

						<div class="col-md-4">

							<div style="background:#90ed7d" class="widget widget-item-icon">
								<div class="widget-item-left">
									<span class="fa fa-user"></span>
								</div>
								<div class="widget-data">
									<div class="widget-int num-count">
										<?php if($unreview){echo $unreview;}else{echo 0;} ?>
									</div>
									<div class="widget-title"> Tidak Beri Ulasan</div>
									<div class="widget-subtitle">Di
										<?php echo $unreview_transaction ?> Transaksi</div>
								</div>

							</div>

						</div>

					</div>


				</div>

			</div>
		</div>
	</div>
</div>

<script>
	$(function () {
		var chart;
		$(document).ready(function () {

			$.getJSON("crm_controller/g_review/<?php echo $year ?>", function (json) {

				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'g_review',
						type: 'column'

					},
					title: {
						text: 'Grafik Review Tahun <?php echo $year ?>'

					},
					subtitle: {
						text: ''

					},
					credits: {
						enabled: false
					},
					xAxis: {
						categories: <?php echo json_encode($month) ?>
					},
					yAxis: {
						title: {
							text: 'Grafik Review'
						},
						plotLines: [{
							value: 0,
							width: 1,
							color: '#808080'
						}]
					},
					tooltip: {
						formatter: function () {
							return '<b>' + this.series.name + '</b><br/>' +
								this.x + ': ' + this.y;
						}
					},
					legend: {
						layout: 'vertical',
						align: 'right',
						verticalAlign: 'top',
						x: -10,
						y: 120,
						borderWidth: 0
					},
					series: json
				});
			});

		});

	});
</script>