<ul class="breadcrumb">
	<li>
		<a href="#">Pengaturan</a>
	</li>
	<li class="active">Email Pengiriman</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">

			<form class="form-horizontal">

				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 class="panel-title">
							<strong>Email</strong> Pengiriman</h3>
					</div>

					<div class="panel-body">

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Email</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-envelope"></span>
									</span>
									<input onkeyup="check()" id="send_mail" autocomplete="new-email" type="text" class="form-control" value="<?php echo $setting[0]['send_mail'] ?>">
								</div>
								<span class="help-block">Harus di isi dengan email @gmail.com</span>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Password</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-lock"></span>
									</span>
									<input onkeyup="check()" id="send_pass" autocomplete="new-password" type="password" class="form-control" value="<?php echo $setting[0]['send_pass'] ?>">
								</div>
							</div>
						</div>

					</div>

					<div class="panel-footer">
						<a disabled id="save" onclick="action()" class="btn btn-primary pull-right">Simpan</a>
						<a style="display:none" id="loading" class="btn btn-primary pull-right">Tunggu ..</a>
					</div>
				</div>

			</form>

		</div>
	</div>
</div>


<script>
	check();

	function check() {
		var email = $("#send_mail").val();
		var password = $("#send_pass").val();

		if (email != '' && password != '') {
			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function action() {
		$("#save").hide();
		$("#loading").show();

		var data = {
			email: $('#send_mail').val(),
			password: $('#send_pass').val()
		}

		var message = 'Data has been saved !';
		var error = 'Failed to save data !';

		console.log('data', data);
		postData('main_controller/update_email_sender', data, function (err, response) {
			if (response) {
				console.log("berhasil : ", response);
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					btn_s.click();
					$("#message-s").html(message);
					sub_menu('#settings', '#email_sender', 'main_controller/email_sender');
				} else {
					$("#save").show();
					$("#loading").hide();
					btn_e.click();
					$("#message-e").html(error);
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>