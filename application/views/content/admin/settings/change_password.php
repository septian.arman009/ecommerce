<ul class="breadcrumb">
	<li>
		<a href="#">Pengaturan</a>
	</li>
	<li class="active">Ganti Password</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">

			<form class="form-horizontal">

				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 class="panel-title">
							<strong>Ganti</strong> Password</h3>
					</div>

					<div class="panel-body">

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Password Lama</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-lock"></span>
									</span>
									<input id="old_password" onchange="check_password()" type="password" class="form-control">

								</div>
								<span class="help-block" id="wrong" style="display:none;">Password Salah !</span>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Password Baru</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-lock"></span>
									</span>
									<input disabled id="password" onkeyup="check()" type="password" class="form-control">

								</div>
								<span class="help-block" id="passless" style="display:none;">Minimal password 8 karakter !</span>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Ulangi Password Baru</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-lock"></span>
									</span>
									<input disabled id="confirm" onkeyup="check()" type="password" class="form-control">

								</div>
								<span class="help-block" id="conless" style="display:none;">Minimal password 8 karakter !</span>
								<span class="help-block" id="notmatch" style="display:none;">Password tidak sama !</span>
							</div>
						</div>

					</div>

					<div class="panel-footer">
						<a disabled id="save" onclick="action()" class="btn btn-primary pull-right">Simpan</a>
						<a style="display:none" id="loading" class="btn btn-primary pull-right">Tunggu ..</a>
					</div>
				</div>

			</form>

		</div>
	</div>
</div>


<script>
	check();

	function check() {
		var password = $("#password").val();
		var confirm = $("#confirm").val();

		var passlen = password.length;
		var conlen = confirm.length;

		if (passlen != '') {
			if (passlen < 8) {
				$("#passless").show();
			} else {
				$("#passless").hide();
			}
		} else {
			$("#passless").hide();
		}

		if (confirm != '') {
			if (conlen < 8) {
				$("#conless").show();
			} else {
				$("#conless").hide();
			}
		} else {
			$("#conless").hide();
		}


		if (password != '' && confirm != '') {

			if (password == confirm) {
				$("#notmatch").hide();

				if (password.length >= 8 && confirm.length >= 8) {
					$("#save").removeAttr('disabled');
				} else {
					$("#save").attr('disabled', 'disabled');
				}
			} else {
				$("#notmatch").show();
				$("#save").attr('disabled', 'disabled');
			}

		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function check_password() {

		var data = {
			old_password: $('#old_password').val(),
		}

		console.log('data', data);
		postData('main_controller/check_old_password', data, function (err, response) {
			if (response) {
				console.log("berhasil : ", response);
				var status = response.status;
				if (status == 'success') {
					$("#password").removeAttr('disabled');
					$("#confirm").removeAttr('disabled');
					$("#wrong").hide();
				} else {
					$("#password").attr('disabled', 'disabled');
					$("#confirm").attr('disabled', 'disabled');
					$("#wrong").show();
				}
			} else {
				console.log('ini error : ', err);
			}
		});

	}

	function action() {
		$("#save").hide();
		$("#loading").show();

		var data = {
			password: $('#password').val()
		}

		var message = 'Data has been saved !';
		var error = 'Failed to save data !';

		console.log('data', data);
		postData('main_controller/update_password', data, function (err, response) {
			if (response) {
				console.log("berhasil : ", response);
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					btn_s.click();
					$("#message-s").html(message);
					sub_menu('#settings', '#change_password', 'main_controller/change_password');
				} else {
					$("#save").show();
					$("#loading").hide();
					btn_e.click();
					$("#message-e").html(error);
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>