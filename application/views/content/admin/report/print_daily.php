<?php

$this->fpdf->FPDF('P', 'cm', 'A4');
$this->fpdf->AliasNbPages();
$this->fpdf->AddPage();

$this->fpdf->Image('assets/shop/img/logo1.png', 6.5, 0.4, 9, 0,'', base_url('admin'));

$this->fpdf->SetFont('Times', 'B', 13);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 26);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 16);
$this->fpdf->Ln(0.7);
$this->fpdf->Cell(0, 0, 'RUMAH SEJUTA GAYA (GANTI AJA MAUNYA APA)', 0, 0, 'C');

$this->fpdf->SetFont('Times', '', 10);
$this->fpdf->Ln(0.6);
$this->fpdf->Cell(0, 0, 'Jl. Cemara Raya No.47 - 49, Jakasampurna, Bekasi Sel., Kota Bks, Jawa Barat 17145', 0, 0, 'C');


$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.6, 20, 3.6);
$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.7, 20, 3.7);

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Ln(0.6);
$this->fpdf->Cell(0, 0, 'LAPORAN PENJUALAN TANGGAL '.$day.' '.strtoupper($this->mylib->to_month($month)).' '.$year, 0, 0, 'C');



$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->ln(1);
$this->fpdf->Cell(2, 0.5, 'NO', 1, 0, 'C');
$this->fpdf->Cell(6.5, 0.5, 'Nomor Invoice Pembayaran', 1, 0, 'C');
$this->fpdf->Cell(3, 0.5, 'Waktu Transaksi', 1, 0, 'C');
$this->fpdf->Cell(4, 0.5, 'Jumlah Produk Terjual', 1, 0, 'C');
$this->fpdf->Cell(3.5, 0.5, 'Total Penjualan', 1, 0, 'C');

$this->fpdf->Ln();
$index = 0;
$no = 1;
$total = 0;
foreach ($sales as $key => $value) {


        $this->fpdf->SetFont('Times', '', 10);
        $this->fpdf->Cell(2, 0.5, $no, 1, 0, 'C');
        $this->fpdf->Cell(6.5, 0.5, $value['purchasement_inv'], 1, 0, 'C');
        $this->fpdf->Cell(3, 0.5, $this->mylib->to_time($value['created_at']), 1, 0, 'C');
        $this->fpdf->Cell(4, 0.5, $qty[$index], 1, 0, 'C');
        $this->fpdf->Cell(3.5, 0.5, $this->mylib->torp($value['transfer_ammount']), 1, 0, 'L');
       
        $this->fpdf->Ln();

    $index++;
    $no++;
    $total += $value['transfer_ammount'];
    
}

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Cell(2, 0.5,'', 1, 0, 'C');
$this->fpdf->Cell(6.5, 0.5, '', 1, 0, 'C');
$this->fpdf->Cell(3, 0.5, '', 1, 0, 'C');
$this->fpdf->Cell(4, 0.5, 'TOTAL', 1, 0, 'C');
$this->fpdf->Cell(3.5, 0.5, $this->mylib->torp($total), 1, 0, 'L');

$this->fpdf->Ln();

$this->fpdf->Output();
