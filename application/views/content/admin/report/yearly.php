<ul class="breadcrumb">
	<li>
		<a href="#">Laporan</a>
	</li>
	<li class="active">Laporan Tahunan</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div style="margin-bottom:1%;" class="panel-heading ui-draggable-handle">
					<h3 class="panel-title">
						<strong>Laporan Pembayaran Bulanan </strong> Tahun <?php echo $year ?></h3>
				</div>

				<div class="form-horizontal">
					<div class="panel-body">

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Tahun</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-calendar-o"></span>
									</span>
									<select id="year" type="text" class="form-control">
										<?php for ($i=2018; $i<=2030 ; $i++) { ?>
										<?php if($i == $year){ ?>
										<option selected value="<?php echo $i ?>">
											<?php echo $i ?>
										</option>
										<?php }else{?>
										<option value="<?php echo $i ?>">
											<?php echo $i ?>
										</option>
										<?php }?>
										<?php } ?>
									</select>
									<span class="input-group-addon">
										<a style="color:white;text-decoration:none" onclick="filter()"> Tampilkan</a>
									</span>
									<span class="input-group-addon">
										<a style="color:white;text-decoration:none" href="<?php echo base_url() ?>yearly_report/<?php echo $year ?>" target="_blank" class="fa fa-print"> Cetak Tahunan</a>
									</span>
								</div>
							</div>
						</div>
					
					</div>
				</div>

				<div class="panel-body">
					<div id="g_yearly"></div>
				</div>

			</div>
		</div>
	</div>
</div>

<script>

	function filter() {
		var year = $("#year").val();

		sub_menu('#report','#yearly','report_controller/yearly/'+year);
	}
	$(function () {
		var chart;
		$(document).ready(function () {

			$.getJSON("report_controller/g_yearly/<?php echo $year ?>", function (json) {

				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'g_yearly',
						type: 'line'

					},
					title: {
						text: 'Grafik pembayaran Tahun <?php echo $year ?>'

					},
					subtitle: {
						text: ''

					},
					credits: {
						enabled: false
					},
					xAxis: {
						categories: <?php echo json_encode($month) ?>
					},
					yAxis: {
						title: {
							text: 'Grafik Pembayaran'
						},
						plotLines: [{
							value: 0,
							width: 1,
							color: '#808080'
						}]
					},
					tooltip: {
						formatter: function () {
							return '<b>' + this.series.name + '</b><br/>' +
								this.x + ': ' + this.y;
						}
					},
					legend: {
						layout: 'vertical',
						align: 'right',
						verticalAlign: 'top',
						x: -10,
						y: 120,
						borderWidth: 0
					},
					series: json
				});
			});

		});

	});
</script>