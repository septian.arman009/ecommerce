<ul class="breadcrumb">
	<li>
		<a href="#">Laporan</a>
	</li>
	<li class="active">Laporan Bulanan</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div style="margin-bottom:1%;" class="panel-heading ui-draggable-handle">
					<h3 class="panel-title">
						<strong>Laporan Pembayaran Harian </strong> Bulan
						<?php echo $this->mylib->to_month($month).' Tahun '.$year ?>
					</h3>
				</div>
				<div class="form-horizontal">
					<div class="panel-body">

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Tahun</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-calendar-o"></span>
									</span>
									<select id="year" type="text" class="form-control">
										<?php for ($i=2018; $i<=2030 ; $i++) { ?>
										<?php if($i == $year){ ?>
										<option selected value="<?php echo $i ?>">
											<?php echo $i ?>
										</option>
										<?php }else{?>
										<option value="<?php echo $i ?>">
											<?php echo $i ?>
										</option>
										<?php }?>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Bulan</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
									<select id="month" class="form-control">
										<?php $month_array = array('01' => 'Janauri', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni',
										'07' => 'Juli','08' => 'Agustus','09' => 'September','10' => 'Oktober','11' => 'November','12' => 'Desember'); ?>
										<?php foreach ($month_array as $key => $value) { ?>
										<?php if($month == $key){ ?>
										<option selected value="<?php echo $key ?>">
											<?php echo $value ?>
										</option>
										<?php }else{?>
										<option value="<?php echo $key ?>">
											<?php echo $value ?>
										</option>
										<?php }?>
										<?php } ?>
									</select>
									<span class="input-group-addon">
										<a style="color:white;text-decoration:none" onclick="filter()"> Tampilkan</a>
									</span>
									<span class="input-group-addon">
										<a style="color:white;text-decoration:none" href="<?php echo base_url() ?>monthly_report/<?php echo $month.'/'.$year ?>" target="_blank" class="fa fa-print"> Cetak Bulanan</a>
									</span>
									
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Bulan</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
									<select onchange="give_href()" id="day" type="text" class="form-control">
										<?php for ($i=1; $i<=31 ; $i++) { ?>
										<option selected value="<?php echo $i ?>">
											<?php echo $i ?>
										</option>
										<?php } ?>
									</select>
				
									<span class="input-group-addon">
										<a id="print_daily" style="color:white;text-decoration:none" target="_blank" class="fa fa-print"> Cetak Harian</a>
									</span>
									
								</div>
							</div>
						</div>
					
					</div>
				</div>
				<div class="panel-body">
					<hr>
					<div id="g_monthly"></div>
				</div>

			</div>
		</div>
	</div>
</div>

<script>
	give_href();
	function give_href(){
		var day = $("#day").val();
		var month = <?php echo $month ?>;
		var year = <?php echo $year ?>;

		$("#print_daily").attr('href', '<?php echo base_url() ?>daily_report/'+day+'/'+month+'/'+year);
	}

	function filter() {
		var year = $("#year").val();
		var month = $("#month").val();

		sub_menu('#report','#monthly','report_controller/monthly/'+month+'/'+year);
	}

	$(function () {
		var chart;
		$(document).ready(function () {

			$.getJSON("report_controller/g_monthly/<?php echo $month . '/' . $year ?>", function (json) {

				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'g_monthly',
						type: 'line'

					},
					title: {
						text: 'Grafik pembayaran bulan <?php echo $this->mylib->to_month($month). ' ' . $year ?>'

					},
					subtitle: {
						text: ''

					},
					credits: {
						enabled: false
					},
					xAxis: {
						categories: <?php echo json_encode($daily) ?>
					},
					yAxis: {
						title: {
							text: 'Grafik Pembayaran'
						},
						plotLines: [{
							value: 0,
							width: 1,
							color: '#808080'
						}]
					},
					tooltip: {
						formatter: function () {
							return '<b>' + this.series.name + '</b><br/>' +
								this.x + ': ' + this.y;
						}
					},
					legend: {
						layout: 'vertical',
						align: 'right',
						verticalAlign: 'top',
						x: -10,
						y: 120,
						borderWidth: 0
					},
					series: json
				});
			});

		});

	});
</script>