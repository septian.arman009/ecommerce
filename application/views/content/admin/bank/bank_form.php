<ul class="breadcrumb">
	<li>
		<a href="#">Data Master</a>
	</li>
	<li class="active">Bank</li>
	<?php if ($id == 'null') {?>
	<li class="active">Tambah Bank Penerima Dana</li>
	<?php }else{?>
	<li class="active">Edit Bank Penerima Dana</li>
	<?php }?>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">

			<form class="form-horizontal" id="bank_form">
				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<?php if ($id == 'null') {?>
						<h3 class="panel-title">
							<strong>Tambah</strong> Bank</h3>
						<?php }else{?>
						<h3 class="panel-title">
							<strong>Perbarui</strong> Bank</h3>
						<?php }?>
					</div>
					<button onclick="sub_menu('#shop','#receiver_bank','content_controller/receiver_bank')" class="btn btn-default">
						<i class="fa fa-arrow-left"></i>
					</button>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">

								<div class="form-group">
									<label class="col-md-3 col-xs-12 control-label">Pilih Gambar</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-picture-o"></span>
											</span>
											<input type="file" name="file" class="form-control">
										</div>
										<span class="help-block">Silakan gunakan gambar 300 x 100 untuk keindahan tampilan</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label">Nama Bank</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-pencil"></span>
											</span>
											<input onkeyup="check()" id="bank_name" type="text" class="form-control">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label">Pemilik (Atas Nama)</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-user"></span>
											</span>
											<input onkeyup="check()" id="owner" type="text" class="form-control">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label">No Rekening</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-credit-card"></span>
											</span>
											<input onkeyup="check()" id="account" type="text" class="form-control">
										</div>
									</div>
								</div>
								<div style="margin:auto;text-align:center;" class="images">
									<img style="width:200px;" id="image">
								</div>

							</div>
						</div>
					</div>

					<div class="panel-footer">
						<a onclick="reset()" class="btn btn-default">Bersihkan Data</a>
						<a disabled id="save" onclick="action()" class="btn btn-primary pull-right">Simpan</a>
						<a style="display:none" id="loading" class="btn btn-primary pull-right">Tunggu ..</a>
					</div>

				</div>
			</form>

		</div>
	</div>
</div>

<script>
	var id = '<?php echo $id ?>';

	if (id != 'null') {
		detail(id);
	}

	check();

	function reset() {
		_('bank_form').reset();
		check();
	}

	function check() {
		var bank_name = $("#bank_name").val();
		var owner = $("#owner").val();
		var account = $("#account").val();
		if (bank_name != '' && owner != '' && account != '') {
			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function action() {
		$("#save").hide();
		$("#loading").show();

		var inputFile = $('input[name=file]');
		var fileToUpload = inputFile[0].files[0];

		var formData = new FormData();
		formData.append("id", id);
		formData.append("bank_name", $("#bank_name").val());
		formData.append("owner", $("#owner").val());
		formData.append("account", $("#account").val());
		formData.append("file", fileToUpload);

		if (id == 'null') {
			var message = "Data telah tersimpan !";
		} else {
			var message = "Data berhasil diubah !";
		}

		if (fileToUpload || id != 'null') {
			$.ajax({
				url: 'content_controller/save_bank',
				type: 'post',
				data: formData,
				processData: false,
				contentType: false,
				success: function () {
					$("#save").show();
					$("#loading").hide();
					btn_s.click();
					$("#message-s").html(message);
					sub_menu('#shop', '#receiver_bank', 'content_controller/receiver_bank');
				}
			});
		} else {
			$("#save").show();
			$("#loading").hide();
			btn_e.click();
			$("#message-e").html("File gambar masih kosong !");
		}
	}

	function detail(id) {
		data = {
			id: id,
			table: 'bank'
		}
		console.log("data : ", data);
		postData('main_controller/detail', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					$("#bank_name").val(response.data[0].bank_name);
					$("#owner").val(response.data[0].owner);
					$("#account").val(response.data[0].account);
					$("#image").attr('src', '<?php echo base_url() ?>assets/banks/' + response.data[0].image);
					check();
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>