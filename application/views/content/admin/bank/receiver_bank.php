<ul class="breadcrumb">
	<li>
		<a href="#">Data Master</a>
	</li>
	<li class="active">Bank Penerima Dana</li>
</ul>

<div class="page-content-wrap">

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Daftar Bank</h3>
				</div>
				<div class="panel-body">
					<button onclick="sub_menu('#master_data','#users','content_controller/bank_form/null')" class="btn btn-default mb-control" data-box="#message-box-sound-2">Tambah Bank Penerima Dana</button>
					<br>
					<br>
					<table id="user-table" class="table stripe hover">
						<thead>
							<tr>
								<th id="th" width="10%">ID</th>
								<th id="th">Nama Bank</th>
                                <th id="th">Atas Nama</th>
                                <th id="th">Rekening</th>
								<th id="th" class="no-sort" width="15%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">ID</th>
								<th class="footer">Nama Bank</th>
								<th class="footer">Atas Nama</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->
		</div>
	</div>
</div>

<script>
	$(document).ready(function () {
		//Fungsi untuk kolom pencarian pada footer table
		$('#user-table tfoot th').each(function () {
			var title = $(this).text();
			var inp = '<input type="text" class="form-control footer-s" placeholder="' + title + '" />';
			$(this).html(inp);
		});

		//Fungsi untuk memanggil datatables di controller localhost/JST/tabel/datatables_user
		//dan ditampilkan ke dalam tabel dengan id user_view
		var table = $('#user-table').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": 'content_controller/show_bank',
				"type": "POST"
			}
		});

		//Fungsi ketika kolom pencarian terisi oleh data
		table.columns().every(function () {
			var that = this;
			$('input', this.footer()).on('keyup change', function () {
				if (that.search() !== this.value) {
					that.search(this.value).draw();
				}
			});
		});
	});

	function destroy(id) {
		btn_d_c.click();
		$("#message-d-c").html("Apakah anda yakin ingin menghapus Bank Penerima dengan ID : " + id + " ?");
		$("#yes-d-c").attr("onclick", "do_delete(" + id + ")");
	}

	function do_delete(id) {
		var data = {
			id: id
		}
		postData('main_controller/destroy/bank/id/', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					loadView('content_controller/receiver_bank', '.content');
					btn_s.click();
					$("#message-s").html('Users has been deleted !');
				} else {
					btn_e.click();
					$("#message-e").html('Failed to delete user !');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function edit(id) {
		sub_menu('#shop', '#receiver_bank', 'content_controller/bank_form/' + id);
	}
</script>

<style>
	#user-table_filter {
		display: none;
	}
</style>