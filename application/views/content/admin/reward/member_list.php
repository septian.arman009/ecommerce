<table id="special-table" class="table stripe hover">
	<thead>
		<tr>
			<th id="th" width="10%">No</th>
			<th id="th">Member</th>
			<th id="th">Banyak Transaksi</th>
			<th id="th">Total Belanja Komulatif</th>
			<th id="th">Tanggal Bergabung</th>
			<th id="th" width="18%">Transaksi Terakhir</th>
			<th id="th" width="20%">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php if(!empty($name)){?>
		<?php $index = 0; foreach ($name as $key => $value) {?>
		<tr>
             <td>
				<?php echo $index+1;; ?>
			</td>
			<td>
				<?php echo $value; ?>
			</td>
			<td>
				<?php echo $transaction[$index]; ?>
			</td>
			<td>
				<?php echo $total[$index]; ?>
			</td>
			<td>
				<?php echo $join[$index]; ?>
			</td>
			<td>
				<?php echo $last[$index]; ?>
            </td>
            <td>
                <a class="btn btn-success" onclick="reward_form(<?php echo $member_id[$index] ?>)">Berikan Reward</a>
            </td>
		</tr>
		<?php $index++; }?>
		<?php }else{?>
			<tr>
             <td>
				No Data !
			</td>
		</tr>
		<?php }?>
	</tbody>
</table>

<script>
    function reward_form(id){
        $("#save").removeAttr('onclick');
        loadView('crm_controller/reward_form/'+id, '.reward_form');
        $("#save").attr('onclick', 'save_special('+id+')');
    }
</script>