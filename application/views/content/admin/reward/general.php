<div class="form-horizontal">
	<div class="panel-body">

		<div class="form-group">
			<label class="col-md-3 col-xs-12 control-label">Kode Voucher</label>
			<div class="col-md-6 col-xs-12">
				<div class="input-group">
					<span class="input-group-addon">
						<span class="fa fa-credit-card"></span>
					</span>
					<input onkeyup="check()" id="voucher_code" type="text" class="form-control">
				</div>
				<span id="voucher_used" style="color:red;display:none;">Kode voucher sudah ada !</span>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 col-xs-12 control-label">Diskon</label>
			<div class="col-md-6 col-xs-12">
				<div class="input-group">
					<span class="input-group-addon">
						<span>%</span>
					</span>
					<input onkeyup="check()" id="discount" type="number" class="form-control" min="0">
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 col-xs-12 control-label">Description</label>
			<div class="col-md-6 col-xs-12">
				<div class="input-group">
					<span class="input-group-addon">
						<span class="fa fa-pencil"></span>
					</span>
					<input onkeyup="check()" id="description" type="text" class="form-control" min="0">
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 col-xs-12 control-label">Minimal Transaksi</label>
			<div class="col-md-6 col-xs-12">
				<div class="input-group">
					<span class="input-group-addon">
						<span class="fa fa-dollar"></span>
					</span>
					<input onkeyup="check()" id="transaction" type="number" class="form-control" min="0">
					<span class="input-group-addon">
						<span>Kali</span>
					</span>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 col-xs-12 control-label">Masa Berlaku</label>
			<div class="col-md-6 col-xs-12">
				<div class="input-group">
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					<input onkeyup="check()" id="validity" type="number" class="form-control" min="0">
					<span class="input-group-addon">
						<span>Hari</span>
					</span>
				</div>
			</div>
		</div>

	</div>

	<div class="panel-body">
		<table id="general-table" class="table stripe hover">
			<thead>
				<tr>
					<th id="th" width="10%">No</th>
					<th id="th">Kode Voucher</th>
					<th id="th">Discount</th>
					<th id="th">Description</th>
					<th id="th">Minimal Transaksi</th>
					<th id="th">Masa Berlaku (Hari)</th>
					<th id="th" width="18%">Kadaluarsa</th>
					<th id="th" width="20%">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $no = 1; foreach ($general as $key => $value) { ?>
				<tr>
					<td>
						<?php echo $no++; ?>
					</td>
					<td>
						<input id="voucher_code<?php echo $value['id'] ?>" class="form-control" value="<?php echo $value['voucher_code'] ?>">
					</td>
					<td>
						<input id="discount<?php echo $value['id'] ?>" class="form-control" value="<?php echo $value['discount'] ?>">
					</td>
					<td>
						<input id="description<?php echo $value['id'] ?>" class="form-control" value="<?php echo $value['description'] ?>">
					</td>
					<td>
						<input id="transaction<?php echo $value['id'] ?>" class="form-control" value="<?php echo $value['transaction'] ?>">
					</td>
					<td>
						<input id="validity<?php echo $value['id'] ?>" class="form-control" value="<?php echo $value['expired'] ?>">
					</td>
					<td>
						<?php echo $this->mylib->to_date_time($value['validity']) ?>
					</td>
					<td>
						<?php if($value['status'] != 'false'){ ?>
						<a id="update_general<?php echo $value['id'] ?>" onclick="update_general(<?php echo $value['id'] ?>)" class="btn btn-info btn-xs waves-effect">
							<i class="fa fa-pencil-square-o"> Update</i>
						</a>
						<a style="display:none" id="loading_general<?php echo $value['id'] ?>" onclick="update_general(<?php echo $value['id'] ?>)"
						class="btn btn-info btn-xs waves-effect">
							<i class="fa fa-pencil-square-o"> Memroses..</i>
						</a>
						<a onclick="destroy_general(<?php echo $value['id'] ?>)" class="btn btn-danger btn-xs waves-effect">
							<i class="fa fa-trash-o"> Hapus</i>
						</a>
						<?php }else{ ?>
						<a onclick="destroy_general(<?php echo $value['id'] ?>)" class="btn btn-danger btn-xs waves-effect">
							<i class="fa fa-trash-o"> Expired</i>
						</a>
						<?php }?>
					</td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
</div>

<script>
	check();
	function check() {
		var code = $("#voucher_code").val();
		var discount = $("#discount").val();
		var description = $("#description").val();
		var transaction = $("#transaction").val();
		var validity = $("#validity").val();

		if (code != '' && discount != '' && description != '' && transaction != '' && validity != '') {
			check_voucher();
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function check_voucher() {
	
		var data = {
			code: $("#voucher_code").val()
		}
		postData('crm_controller/check_voucher', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					$("#save").removeAttr('disabled');
					$("#voucher_used").hide();
				} else {
					$("#save").attr('disabled', 'disabled');
					$("#voucher_used").show();
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function save_general() {
		$("#save").hide();
		$("#loading").show();
		var data = {
			code: $("#voucher_code").val(),
			discount: $("#discount").val(),
			description: $("#description").val(),
			transaction: $("#transaction").val(),
			validity: $("#validity").val()
		}
		postData('crm_controller/save_general', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					loadView('crm_controller/reward', '.content');
					btn_s.click();
					$("#message-s").html('Reward telah disimpan !');
				} else {
					$("#save").show();
					$("#loading").hide();
					btn_e.click();
					$("#message-e").html('Gagal menyimpan rerward !');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function update_general(id) {
		$("#update_general" + id).hide();
		$("#loading_general" + id).show();

		var code = $("#voucher_code"+ id).val();
		var discount = $("#discount"+ id).val();
		var description = $("#description"+ id).val();
		var transaction = $("#transaction"+ id).val();
		var validity = $("#validity"+ id).val();

		if (code != '' && discount != '' && transaction != '' && validity != '') {
			var data = {
				id: id,
				code: $("#voucher_code" + id).val(),
				discount: $("#discount" + id).val(),
				transaction: $("#transaction" + id).val(),
				description: $("#description"+ id).val(),
				validity: $("#validity" + id).val()
			}
			postData('crm_controller/update_general', data, function (err, response) {
				if (response) {
					console.log('berhasil : ', response);
					if (response.status == 'success') {
						loadView('crm_controller/reward', '.content');
						btn_s.click();
						$("#message-s").html('Reward telah disimpan !');
					} else {
						$("#update_general" + id).show();
						$("#loading_general" + id).hide();
						btn_e.click();
						$("#message-e").html('Gagal menyimpan rerward !');
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		} else {
			$("#update_general" + id).show();
			$("#loading_general" + id).hide();
			btn_e.click();
			$("#message-e").html('Silakan lengkapi form terlebih dahulu !');
		}
	}

	function destroy_general(id) {
		btn_d_c.click();
		$("#message-d-c").html("Apakah anda yakin ingin Reward dengan ID : " + id + " ?");
		$("#yes-d-c").attr("onclick", "do_delete_general(" + id + ")");
	}

	function do_delete_general(id) {
		var data = {
			id: id
		}
		postData('main_controller/destroy/reward_list/id/', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					loadView('crm_controller/reward', '.content');
					btn_s.click();
					$("#message-s").html('Reward telah dihapus !');
				} else {
					btn_e.click();
					$("#message-e").html('Gagal menghapus reward !');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>