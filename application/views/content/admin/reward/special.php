<div class="form-horizontal">
	<div class="panel-body">

		<div class="form-group">
			<label class="col-md-3 col-xs-12 control-label">Member</label>
			<div class="col-md-6 col-xs-12">
				<div class="input-group">
					<span class="input-group-addon">
						<span class="fa fa-user"></span>
					</span>
					<input onchange="search_member()" id="member_name" type="text" class="form-control">
				</div>
			</div>
        </div>
        
        <div class="reward_form"></div>

	</div>

	<div class="panel-body">
		<div class="member_list">
			<table id="special-table" class="table stripe hover">
				<thead>
					<tr>
						<th id="th" width="10%">No</th>
						<th id="th">Member</th>
						<th id="th">Banyak Transaksi</th>
						<th id="th">Total Belanja Komulatif</th>
						<th id="th">Tanggal Bergabung</th>
						<th id="th" width="18%">Transaksi Terakhir</th>
						<th id="th" width="20%">Action</th>
					</tr>
				</thead>
				<tbody>
                   
				</tbody>
			</table>
		</div>

	</div>
</div>

<script>
	String.prototype.replaceAll = function(str1, str2, ignore)
	{
		return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
	}

	var name_ = document.cookie.replace(/(?:(?:^|.*;\s*)name_pass\s*\=\s*([^;]*).*$)|^.*$/, "$1");

    if(name_){
        loadView('crm_controller/member_detail/' + name_, '.member_list');
        $("#member_name").val(name_);
    }

	function search_member() {
		var name = $("#member_name").val().replace(/[^A-Za-z0-9 ]/g, "");
		var name_pass = name.replaceAll(' ','-',name);
        document.cookie = "name_pass=" + name_pass;
		loadView('crm_controller/member_detail/' + name_pass, '.member_list');
    }
</script>