<ul class="breadcrumb">
	<li>
		<a href="#">Reward</a>
	</li>
</ul>

<div class="page-content-wrap">

	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">

				<div class="panel panel-default tabs">
					<ul class="nav nav-tabs" role="tablist">
						<li id="general-tab">
							<a onclick="tab_1()" href="#tab-first" role="tab" data-toggle="tab" aria-expanded="true">Reward Umum</a>
						</li>
						<li id="special-tab">
							<a onclick="tab_2()" href="#tab-second" role="tab" data-toggle="tab" aria-expanded="false">Reward Khusus</a>
						</li>
					</ul>
					<div class="panel-body tab-content">
						<div class="tab-pane" id="tab-first">
							<div class="panel-body">
								<div class="general"></div>
							</div>
						</div>

						<div class="tab-pane" id="tab-second" style="min-height:50vh;">
							<div class="panel-body">
								<div class="special"></div>
							</div>
						</div>

					</div>
					<div class="panel-footer">
						<a id="save" class="btn btn-primary pull-right">Simpan</a>
						<a style="display:none" id="loading" class="btn btn-primary pull-right">Tunggu ..</a>
					</div>
				</div>

			</div>
		</div>
	</div>

</div>


<script>
	var tab_reward = '<?php echo $tab_reward ?>';

	if (tab_reward == '#tab-first') {
		$('#tab-second').removeClass('active');
		$("#special-tab").removeClass('active');
		$('#tab-first').addClass('active');
		$("#general-tab").addClass('active');
		tab_1();
        content_1();
	} else {
		$('#tab-first').removeClass('active');
		$("#general-tab").removeClass('active');
		$('#tab-second').addClass('active');
		$("#special-tab").addClass('active');
		tab_2();
        content_2();
	}

	function tab_1() {
		document.cookie = "tab_reward=#tab-first";
        content_1();
	}

	function tab_2() {
		document.cookie = "tab_reward=#tab-second";
        content_2();
	}

    function content_1(){
        loadView('crm_controller/general', '.general');
        $("#save").removeAttr('disabled');
        $("#save").removeAttr('onclick');
        $("#save").attr('onclick', 'save_general()');
    }

    function content_2(){
        loadView('crm_controller/special', '.special');
        $("#save").removeAttr('onclick');
        $("#save").attr('disabled', 'disabled');
    }
	
</script>