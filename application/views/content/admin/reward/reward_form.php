<hr>
<div class="form-group">
	<label class="col-md-3 col-xs-12 control-label">Nama</label>
	<div class="col-md-6 col-xs-12">
		<div class="input-group">
			<span class="input-group-addon">
				<span class="fa fa-user"></span>
			</span>
			<input onkeyup="form_reward()" id="name" type="text" class="form-control" value="<?php echo $member[0]['name'] ?>">
		</div>		
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 col-xs-12 control-label">Kode Voucher</label>
	<div class="col-md-6 col-xs-12">
		<div class="input-group">
			<span class="input-group-addon">
				<span class="fa fa-credit-card"></span>
			</span>
			<input onkeyup="form_reward()" id="voucher_code_special" type="text" class="form-control">
		</div>
		<span id="voucher_special_used" style="color:red;display:none;">Kode voucher sudah ada !</span>
		
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 col-xs-12 control-label">Discount</label>
	<div class="col-md-6 col-xs-12">
		<div class="input-group">
			<span class="input-group-addon">
				<span>%</span>
			</span>
			<input onkeyup="form_reward()" id="discount_special" type="number" min="0" class="form-control">
		</div>
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 col-xs-12 control-label">Masa Berlaku</label>
	<div class="col-md-6 col-xs-12">
		<div class="input-group">
			<span class="input-group-addon">
				<span class="fa fa-calendar"></span>
			</span>
			<input onkeyup="form_reward()" id="expired_special" type="number" min="0" class="form-control">
			<span class="input-group-addon">
				<span>Hari</span>
			</span>
		</div>
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 col-xs-12 control-label">Keterangan</label>
	<div class="col-md-6 col-xs-12">
		<div class="input-group">
			<span class="input-group-addon">
				<span class="fa fa-pencil"></span>
			</span>
			<input onkeyup="form_reward()" id="description_special" type="text" class="form-control">
		</div>
	</div>
</div>


<script>
	form_reward();

	function form_reward() {
		var name = $("#name").val();
		var code = $("#voucher_code_special").val();
		var discount = $("#discount_special").val();
		var expired = $("#expired_special").val();
		var description = $("#description_special").val();

		if (name != '' && code != '' && discount != '' && expired != '' && description != '') {
			check_voucher_special();
		} else {
			$("#save").attr("disabled", "disabled");
		}
	}

	function check_voucher_special() {
	
	var data = {
		code: $("#voucher_code_special").val()
	}
	postData('crm_controller/check_voucher_special', data, function (err, response) {
		if (response) {
			console.log('berhasil : ', response);
			if (response.status == 'success') {
				$("#save").removeAttr('disabled');
				$("#voucher_special_used").hide();
			} else {
				$("#save").attr('disabled', 'disabled');
				$("#voucher_special_used").show();
			}
		} else {
			console.log('ini error : ', err);
		}
	});
}

	function save_special(id) {
		var data = {
			member_id: id,
			name: $("#name").val(),
			code: $("#voucher_code_special").val(),
			discount: $("#discount_special").val(),
			expired: $("#expired_special").val(),
			description: $("#description_special").val()
		}

		postData('crm_controller/save_special', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					loadView('crm_controller/reward', '.content');
					btn_s.click();
					$("#message-s").html('Reward telah disimpan !');
				} else {
					$("#save").show();
					$("#loading").hide();
					btn_e.click();
					$("#message-e").html('Gagal menyimpan rerward !');
				}
			} else {
				console.log('ini error : ', err);
			}
		});

	}
</script>