<ul class="breadcrumb">
	<li>
		<a href="#">Data Master</a>
	</li>
	<li class="active">Pengguna</li>
</ul>

<div class="page-content-wrap">

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Pengguna</h3>
				</div>
				<div class="panel-body">
					<button onclick="sub_menu('#master_data','#users','user_controller/form/null')" class="btn btn-default mb-control" data-box="#message-box-sound-2">Tambah Pengguna</button>
					<br>
					<br>
					<table id="user-table" class="table stripe hover">
						<thead>
							<tr>
								<th id="th" width="10%">ID</th>
								<th id="th">Nama</th>
								<th id="th">Email</th>
								<th id="th" class="no-sort" width="15%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">ID</th>
								<th class="footer">Nama</th>
								<th class="footer">Email</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->
		</div>
	</div>
</div>

<script>
	$(document).ready(function () {
		//Fungsi untuk kolom pencarian pada footer table
		$('#user-table tfoot th').each(function () {
			var title = $(this).text();
			var inp = '<input type="text" class="form-control footer-s" placeholder="' + title + '" />';
			$(this).html(inp);
		});

		//Fungsi untuk memanggil datatables di controller localhost/JST/tabel/datatables_user
		//dan ditampilkan ke dalam tabel dengan id user_view
		var table = $('#user-table').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": 'user_controller/show',
				"type": "POST"
			}
		});

		//Fungsi ketika kolom pencarian terisi oleh data
		table.columns().every(function () {
			var that = this;
			$('input', this.footer()).on('keyup change', function () {
				if (that.search() !== this.value) {
					that.search(this.value).draw();
				}
			});
		});
	});

	function destroy(id) {
		btn_d_c.click();
		$("#message-d-c").html("Apakah anda yakin ingin menghapus Pengguna dengan ID : " + id + " ?");
		$("#yes-d-c").attr("onclick", "do_delete(" + id + ")");
	}

	function do_delete(id) {
		var data = {
			id: id
		}
		postData('main_controller/destroy/users/id/', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					loadView('user_controller', '.content');
					btn_s.click();
					$("#message-s").html('User telah dihapus !');
				} else {
					btn_e.click();
					$("#message-e").html('Gagal menghapus user !');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function edit(id) {
		sub_menu('#master_data', '#users', 'user_controller/form/' + id);
	}
</script>

<style>
	#user-table_filter {
		display: none;
	}
</style>