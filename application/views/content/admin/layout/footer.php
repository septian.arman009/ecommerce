<audio id="audio-alert" src="<?php echo base_url() ?>assets/admin/audio/alert.mp3" preload="auto"></audio>
<audio id="audio-fail" src="<?php echo base_url() ?>assets/admin/audio/fail.mp3" preload="auto"></audio>

<script src="<?php echo base_url() ?>assets/admin/js/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/plugins/jquery/jquery-ui.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/plugins/bootstrap/bootstrap.min.js"></script>

<script src='<?php echo base_url() ?>assets/admin/js/plugins/icheck/icheck.min.js'></script>
<script src="<?php echo base_url() ?>assets/admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>

<script src="<?php echo base_url() ?>assets/admin/js/plugins.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/actions.js"></script>

<script src="<?php echo base_url() ?>assets/admin/myjs/main.js"></script>
<script src="<?php echo base_url() ?>assets/admin/myjs/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin/HC/code/highcharts.js"></script>
<script src="<?php echo base_url() ?>assets/admin/myjs/jquery.maskMoney.js"></script>



</body>

<script>
	$(document).ready(function () {
		var admin_url = document.cookie.replace(/(?:(?:^|.*;\s*)admin_url\s*\=\s*([^;]*).*$)|^.*$/, "$1");

		if (admin_url) {
			var main_id = document.cookie.replace(/(?:(?:^|.*;\s*)main_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
			if (main_id) {
				var main_id = document.cookie.replace(/(?:(?:^|.*;\s*)main_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
				main_menu(main_id, admin_url);
			} else {
				var main_sub_id = document.cookie.replace(/(?:(?:^|.*;\s*)main_sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
				if (main_sub_id) {
					var sub_id = document.cookie.replace(/(?:(?:^|.*;\s*)sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
					sub_menu(main_sub_id, sub_id, admin_url);
				}

			}
		} else {
			main_menu('#home','main_controller/home/1');
		}

	});

	var btn_d_c = $("#btn-d-c");
	var btn_s = $("#btn-s");
	var btn_e = $("#btn-e");
</script>

<style>
	.no-sort::after {
		display: none !important;
	}

	.no-sort {
		pointer-events: none !important;
		cursor: default !important;
	}

	a{
		cursor:pointer;
	}
}
</style>

</html>