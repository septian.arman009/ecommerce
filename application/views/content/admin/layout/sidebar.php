<body>
<?php $date = explode('-', date('Y-m-d')); ?>
	<!-- START PAGE CONTAINER -->
	<div class="page-container page-navigation-top-fixed">

		<!-- START PAGE SIDEBAR -->
		<div class="page-sidebar page-sidebar-fixed scroll">
			<!-- START X-NAVIGATION -->
			<ul class="x-navigation">
				<li class="xn-logo">
					<a>Rumah Tiedye</a>
					<a class="x-navigation-control"></a>
				</li>

				<li class="xn-profile">
					<a class="profile-mini">
						<img src="<?php echo base_url() ?>assets/logo.png" />
					</a>
					<div class="profile">
						<div class="profile-image">
							<?php if($_SESSION['com_in']['role'] == 1 || $_SESSION['com_in']['role'] == 4){ ?>
							<img src="<?php echo base_url() ?>assets/logo.png" />
							<?php }else{?>
							<img src="<?php echo base_url() ?>assets/crm.png" />
							<?php }?>
						</div>
						<div class="profile-data">
							<div class="profile-data-name">
								<?php echo $_SESSION['com_in']['name'] ?>
							</div>
							<div class="profile-data-title">
								<?php echo $_SESSION['com_in']['role_name'] ?>
							</div>
						</div>
						<div class="profile-controls">
							<a onclick="sub_menu('#master_data','#users','user_controller/form/<?php echo $_SESSION['com_in']['id'] ?>','.content')"
							class="profile-control-left">
								<span class="fa fa-user"></span>
							</a>
							<?php if($_SESSION['com_in']['role'] == 1){ ?>
								<?php if($bill_count) {?>
								<a style="color:red;border-color:red" onclick="sub_menu('#sales','#billing','sales_controller/billing')" class="profile-control-right">
									<span class="fa fa-info"></span>
								</a>
								<?php }else{?>
								<a class="profile-control-right">
									<span class="fa fa-info"></span>
								</a>
								<?php }?>
							<?php }else if($_SESSION['com_in']['role'] == 3){?>
								<?php if($check_unread_main == 0 ){ ?>
									<a onclick="main_menu('#','crm_controller/discussion')" class="profile-control-right">
										<span class="fa fa-envelope"></span>
									</a>
									<?php }else{?>
									<a style="color:red;border-color:red" onclick="main_menu('#','crm_controller/discussion')" class="profile-control-right">
										<span class="fa fa-envelope"></span>
									</a>
								<?php }?>
							<?php } else if($_SESSION['com_in']['role'] == 4){?>
								<a class="profile-control-right" onclick="main_menu('#review','crm_controller/review/<?php echo $date[0] ?>')">
									<span class="fa fa-bar-chart-o"></span>
								</a>
							<?php }?>
						</div>
					</div>
				</li>
				
				<li class="xn-title">Navigasi Utama</li>

				<li id="home">
					<a onclick="main_menu('#home','main_controller/home/1')">
						<span class="fa fa-home"></span>
						<span class="xn-text">Utama</span>
					</a>
				</li>

				<?php if($_SESSION['com_in']['role'] == 3){ ?>
				<li id="member">
					<a onclick="main_menu('#member','crm_controller/member')">
						<span class="fa fa-user"></span>
						<span class="xn-text">Member</span>
					</a>
				</li>

				<li id="review">
					<a onclick="main_menu('#review','crm_controller/review/<?php echo $date[0] ?>')">
						<span class="fa fa-bar-chart-o"></span>
						<span class="xn-text">Review Pelanggan</span>
					</a>
				</li>

				<li id="reward">
					<a onclick="main_menu('#reward','crm_controller/reward')">
						<span class="fa fa-trophy"></span>
						<span class="xn-text">Reward (Discount)</span>
					</a>
				</li>

				<?php } else if($_SESSION['com_in']['role'] == 1 || $_SESSION['com_in']['role'] == 4){?>

				<?php if($_SESSION['com_in']['role'] == 4){ ?>
				<li id="master_data" class="xn-openable">
					<a>
						<span class="fa fa-hdd-o"></span>
						<span class="xn-text">Data Master</span>
					</a>
					<ul>
						<li id="users">
							<a onclick="sub_menu('#master_data','#users','user_controller')">
								<span class="fa fa-user"></span>Pengguna</a>
						</li>

						<li id="member_owner">
							<a onclick="sub_menu('#master_data','#member_owner','crm_controller/member')">
								<span class="fa fa-user"></span>Member</a>
						</li>
					</ul>
				</li>
				<?php }?>

				<?php if($_SESSION['com_in']['role'] == 1){ ?>
				<li id="shop" class="xn-openable">
					<a>
						<span class="fa fa-shopping-cart"></span>
						<span class="xn-text">Toko</span>
					</a>
					<ul>
						<li id="slider-image">
							<a onclick="sub_menu('#shop','#slider-image','content_controller/slider_image')">
								<span class="fa fa-picture-o"></span>Slider Utama</a>
						</li>
						<li id="receiver_bank">
							<a onclick="sub_menu('#shop','#receiver_bank','content_controller/receiver_bank')">
								<span class="fa fa-credit-card"></span>Bank Penerima Dana</a>
						</li>
						<li id="categories">
							<a onclick="sub_menu('#shop','#categories','content_controller/categories')">
								<span class="fa fa-puzzle-piece"></span>Produk Kategori</a>
						</li>
						<li id="products">
							<a onclick="sub_menu('#shop','#products','content_controller/products/1/null/null')">
								<span class="fa fa-list"></span>Daftar Produk</a>
						</li>
					</ul>
				</li>
				<?php }?>

				<li id="sales" class="xn-openable">
					<a>
						<span class="fa fa-money"></span>
						<span class="xn-text">Penjualan</span>
					</a>
					<ul>
						<?php if($_SESSION['com_in']['role'] == 1){ ?>
						<li id="billing">
							<a onclick="sub_menu('#sales','#billing','sales_controller/billing')">
								<span class="fa fa-dollar"></span>Tagihan Masuk</a>
						</li>
						<?php }?>
						<li id="purchasement">
							<a onclick="sub_menu('#sales','#purchasement','sales_controller/purchasement')">
								<span class="fa fa-dollar"></span>Pembayaran Masuk</a>
						</li>
						<li id="track_order">
							<a onclick="sub_menu('#sales','#track_order','sales_controller/track_order/1/null')">
								<span class="fa fa-truck"></span>Status Pemesanan</a>
						</li>
					</ul>
				</li>
				
				<?php if($_SESSION['com_in']['role'] == 4){ ?>
				<li id="review">
					<a onclick="main_menu('#review','crm_controller/review/<?php echo $date[0] ?>')">
						<span class="fa fa-bar-chart-o"></span>
						<span class="xn-text">Review Customer</span>
					</a>
				</li>
				<?php }?>

				<li id="report" class="xn-openable">
					<a>
						<span class="fa fa-file-text"></span>
						<span class="xn-text">Laporan</span>
					</a>
					<ul>
						<li id="monthly">
							<a onclick="sub_menu('#report','#monthly','report_controller/monthly/<?php echo $date[1].'/'.$date[0] ?>')">
								<span class="fa fa-calendar"></span>Laporan Bulanan</a>
						</li>
						<li id="yearly">
							<a onclick="sub_menu('#report','#yearly','report_controller/yearly/<?php echo $date[0] ?>')">
								<span class="fa fa-calendar-o"></span>laporan Tahunan</a>
						</li>
					</ul>
				</li>


				<li id="settings" class="xn-openable">
					<a>
						<span class="fa fa-gear"></span>
						<span class="xn-text">Pengaturan</span>
					</a>
					<ul>
						<?php if($_SESSION['com_in']['role'] == 1){ ?>
						<li id="email_sender">
							<a onclick="sub_menu('#settings','#email_sender','main_controller/email_sender')">
								<span class="fa fa-envelope"></span>Email pengiriman</a>
						</li>
						<?php }?>
						<li id="change_password">
							<a onclick="sub_menu('#settings','#change_password','main_controller/change_password')">
								<span class="fa fa-lock"></span>Ganti Password</a>
						</li>
					</ul>
				</li>

				<?php }?>

			</ul>
			<!-- END X-NAVIGATION -->
		</div>
		<!-- END PAGE SIDEBAR -->