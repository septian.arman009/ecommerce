<!DOCTYPE html>
<html lang="en">

<head>
	<!-- META SECTION -->
	<title>Rumah Tiedye - Admin Shop</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<link rel="icon" href="<?php echo base_url() ?>assets/shop/ico/favicon.ico" type="image/x-icon" />
	<!-- END META SECTION -->

	<!-- CSS INCLUDE -->
	<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url() ?>assets/admin/css/theme-brown.css" />
	<!-- EOF CSS INCLUDE -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/mycss/bootstrap-tagsinput.css">

</head>