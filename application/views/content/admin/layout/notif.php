<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-sign-out"></span> Log
				<strong>Out</strong> ?</div>
			<div class="mb-content">
				<p>Apakah anda yakin ingin keluar ?</p>
				<p>Klik Tidak untuk tetap bekerja, klik Ya untuk keluar aplikasi.</p>
			</div>
			<div class="mb-footer">
				<div class="pull-right">
					<a class="btn btn-success btn-lg" onclick="logout()">Ya</a>
					<button class="btn btn-default btn-lg mb-control-close">Tidak</button>
				</div>
			</div>
		</div>
	</div>
</div>

<button id="btn-d-c" style="display:none" class="btn btn-default mb-control" data-box="#d_confirm"></button>
<div class="message-box message-box-danger animated fadeIn" data-sound="fail" id="d_confirm">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-times"></span> Konfirmasi !</div>
			<div class="mb-content">
				<p id="message-d-c"></p>
			</div>
			<div class="mb-footer">
				<a id="yes-d-c" class="btn btn-success btn-lg mb-control-close" onclick="logout()">Ya</a>
				<button class="btn btn-default btn-lg mb-control-close">Tidak</button>
			</div>
		</div>
	</div>
</div>

<button id="btn-s" style="display:none" class="btn btn-default mb-control" data-box="#message-box-success"></button>
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-check"></span> Sukses </div>
			<div class="mb-content">
				<p id="message-s"></p>
			</div>
			<div class="mb-footer">
				<button class="btn btn-default btn-lg pull-right mb-control-close">Tutup</button>
			</div>
		</div>
	</div>
</div>

<button id="btn-e" style="display:none" class="btn btn-default mb-control" data-box="#message-box-warning"></button>
<div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-warning"></span> Terjadi Kesalahan </div>
			<div class="mb-content">
				<p id="message-e"></p>
			</div>
			<div class="mb-footer">
				<button class="btn btn-default btn-lg pull-right mb-control-close">Tutup</button>
			</div>
		</div>
	</div>
</div>
<!-- END MESSAGE BOX-->