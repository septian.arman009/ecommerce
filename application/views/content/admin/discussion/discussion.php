<ul class="breadcrumb">
	<li>
		<a href="#">Diskusi</a>
	</li>
</ul>

<div class="page-content-wrap">

	<div class="row">
		<div class="col-md-12">

			<form class="form-horizontal">

				<div class="panel panel-default tabs">
					<ul class="nav nav-tabs" role="tablist">
						<li id="discussion-tab">
							<a onclick="tab_1()" href="#tab-first" role="tab" data-toggle="tab" aria-expanded="true">Diskusi (
								<?php echo $discussion_chat ?>)</a>
						</li>
						<li id="complain-tab">
							<a onclick="tab_2()" href="#tab-second" role="tab" data-toggle="tab" aria-expanded="false">Komplen (
								<?php echo $complain_chat ?>)</a>
						</li>
					</ul>
					<div class="panel-body tab-content">
						<div class="tab-pane" id="tab-first">
							<div class="panel-body">
								<div class="discussion"></div>
							</div>
						</div>

						<div class="tab-pane" id="tab-second" style="min-height:50vh;">
							<div class="panel-body">
								<div class="complain"></div>
							</div>
						</div>

					</div>
					
				</div>

			</form>

		</div>
	</div>

</div>


<script>
	check_chat();
	check_complain();
	var tab_active = '<?php echo $tab_active ?>';

	if (tab_active == '#tab-first') {
		$('#tab-second').removeClass('active');
		$("#complain-tab").removeClass('active');
		$('#tab-first').addClass('active');
		$("#discussion-tab").addClass('active');
		tab_1();
	} else {
		$('#tab-first').removeClass('active');
		$("#discussion-tab").removeClass('active');
		$('#tab-second').addClass('active');
		$("#complain-tab").addClass('active');
		tab_2();
	}

	function tab_1() {
		document.cookie = "tab_active=#tab-first";
	}

	function tab_2() {
		document.cookie = "tab_active=#tab-second";
	}

    function check_chat(){
		var chat_admin = document.cookie.replace(/(?:(?:^|.*;\s*)chat_admin\s*\=\s*([^;]*).*$)|^.*$/, "$1");
		if(chat_admin){
			loadView(chat_admin,'.discussion');
		}else{
			loadView('crm_controller/list_chat','.discussion');
		}
	}

	function check_complain(){
		var chat_complain_admin = document.cookie.replace(/(?:(?:^|.*;\s*)chat_complain_admin\s*\=\s*([^;]*).*$)|^.*$/, "$1");
		if(chat_complain_admin){chat_complain_admin
			loadView(chat_complain_admin,'.complain');
		}else{
			loadView('crm_controller/list_complain','.complain');
		}
	}
</script>