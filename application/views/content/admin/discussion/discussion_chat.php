<div class="messages messages-img" style="height:60vh;overflow:auto;overflow-y:scroll;padding:20px;border:1px solid #f89406;margin-bottom:5%;border-radius:5px;">
	<?php foreach ($discussion as $key => $value) { ?>
	<?php if($value['from_email'] == $_SESSION['com_in']['email']){ ?>
	
	<div class="item item-visible">
		<div class="image">
			<img src="<?php echo base_url() ?>assets/admin_image.png">
		</div>
		<div class="text">
			<div class="heading">
				<a>
					<?php echo $_SESSION['com_in']['email'] ?>
				</a>
				<span class="date">
					<?php echo $this->mylib->to_date_time($value['created_at']) ?>
				</span>
			</div>
			<?php echo $value['message'] ?>
		</div>
	</div>
	<?php }else{ ?>

	<div class="item in item-visible">
		<div class="image">
			<img src="<?php echo base_url() ?>assets/member_image.png">
		</div>
		<div class="text">
			<div class="heading">
				<a href="#">
					<?php echo $members ?>
				</a>
				<span class="date">
					<?php echo $this->mylib->to_date_time($value['created_at']) ?>
				</span>
			</div>
			<?php echo $value['message'] ?>
		</div>
	</div>
	<?php }}?>

</div>


<div class="form-horizontal">

	<div class="form-group">
		<label class="col-md-3 col-xs-12 control-label">Member</label>
		<div class="col-md-6 col-xs-12">
			<input readonly id="member_email" type="email" class="form-control" value="<?php echo $members ?>">
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-3 col-xs-12 control-label">Pesan</label>
		<div class="col-md-6 col-xs-12">
			<textarea id="message" class="form-control" rows="5"></textarea>
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-3 col-xs-12 control-label"></label>
		<div class="col-md-6 col-xs-12">
			<?php if($status == 'aktif' || $status == 'unread'){ ?>
            <a onclick="return_discussion()" class="file-input-wrapper btn fileinput btn-primary">
				<span>Kembali</span>
			</a>
            <a onclick="loadView('crm_controller/discussion_chat/<?php echo $id ?>', '.discussion')" class="file-input-wrapper btn fileinput btn-primary">
				<span>Refresh</span>
			</a>
			<a onclick="reply()" class="file-input-wrapper btn fileinput btn-primary">
				<span>Kirim Pesan</span>
			</a>
			<a onclick="close_chat('<?php echo $title ?>','<?php echo $code ?>')" class="file-input-wrapper btn fileinput btn-danger">
				<span>Tutup Diskusi</span>
			</a>
			<?php }else{?>
			<a onclick="return_discussion()" class="file-input-wrapper btn fileinput btn-primary">
				<span>Kembali</span>
			</a>
			<a class="file-input-wrapper btn fileinput btn-danger">
				<span>Diskusi Ditutup</span>
			</a>
			<?php }?>
		</div>
	</div>

</div>

<script>
	$(".messages").scrollTop($(".messages")[0].scrollHeight - $(".messages")[0].clientHeight);

    function return_discussion() {
        var chat_url = 'crm_controller/list_chat';
        document.cookie = "chat_admin=" + chat_url;
        loadView(chat_url, '.discussion');
	}

    function reply() {
		data = {
			title: '<?php echo $title ?>',
			code: '<?php echo $code ?>',
            member: $("#member_email").val(),
            message: $("#message").val()
		}

		console.log("data : ", data);
		postData('crm_controller/reply', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					loadView('crm_controller/discussion_chat/<?php echo $id ?>', '.discussion');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function close_chat(title, code) {
		btn_d_c.click();
		$("#message-d-c").html("Apakah anda yakin ingin menutup diskusi dengan Judul Diskusi : " + title + " ?");
		$("#yes-d-c").attr("onclick", "do_close('" + title + "','" + code + "')");
	}

	function do_close(title, code) {
		var data = {
			code: code
		}
		postData('crm_controller/close', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					loadView('crm_controller/discussion_chat/<?php echo $id ?>', '.discussion');
					btn_s.click();
					$("#message-s").html('Diskusi dengan Judul Diskusi '+title+' telah ditutup !');
				} else {
					btn_e.click();
					$("#message-e").html('Gagal menutup diskusi !');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>