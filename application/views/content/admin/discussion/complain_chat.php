<div id="messages_complain" class="messages messages-img" style="height:60vh;overflow:auto;overflow-y:scroll;padding:20px;border:1px solid #f89406;margin-bottom:5%;border-radius:5px;">
	<?php foreach ($discussion as $key => $value) { ?>
	<?php if($value['from_email'] == ''){ ?>
	<div class="item item-visible">
		<div class="image">
			<img src="<?php echo base_url() ?>assets/admin_image.png">
		</div>
		<div class="text">
			<div class="heading">
				<a>
					Admin
				</a>
				<span class="date">
					<?php echo $this->mylib->to_date_time($value['created_at']) ?>
				</span>
			</div>
			<?php echo $value['message'] ?>
		</div>
	</div>
	<?php }else{ ?>

	<div class="item in item-visible">
		<div class="image">
			<img src="<?php echo base_url() ?>assets/member_image.png">
		</div>
		<div class="text">
			<div class="heading">
				<a href="#">
					<?php echo $members ?>
				</a>
				<span class="date">
					<?php echo $this->mylib->to_date_time($value['created_at']) ?>
				</span>
			</div>
			<?php echo $value['message'] ?>
		</div>
	</div>
	<?php }}?>

</div>


<div class="form-horizontal">

	<div class="form-group">
		<label class="col-md-3 col-xs-12 control-label">Member</label>
		<div class="col-md-6 col-xs-12">
			<input readonly id="member_email_complain" type="email" class="form-control" value="<?php echo $members ?>">
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-3 col-xs-12 control-label">Pesan</label>
		<div class="col-md-6 col-xs-12">
			<textarea id="message_complain" class="form-control" rows="5"></textarea>
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-3 col-xs-12 control-label"></label>
		<div class="col-md-6 col-xs-12">
            <?php if($status == 'aktif' || $status == 'unread'){ ?>
            <a onclick="return_complain()" class="file-input-wrapper btn fileinput btn-primary">
				<span>Kembali</span>
			</a>
            <a onclick="loadView('crm_controller/complain_chat/<?php echo $id ?>', '.complain')" class="file-input-wrapper btn fileinput btn-primary">
				<span>Refresh</span>
			</a>
			<a onclick="reply_complain()" class="file-input-wrapper btn fileinput btn-primary">
				<span>Kirim Pesan</span>
			</a>
            <a onclick="close_complain('<?php echo $inv ?>','<?php echo $code ?>')" class="file-input-wrapper btn fileinput btn-danger">
				<span>Tutup Komplen</span>
			</a>
            <?php }else{?>
			<a onclick="return_complain()" class="file-input-wrapper btn fileinput btn-primary">
			<span>Kembali</span>
			</a>
            <a class="file-input-wrapper btn fileinput btn-danger">
            <span>Tutup Ditutup</span>
			</a>
            <?php }?>
		</div>
	</div>

</div>

<script>
	$("#messages_complain").scrollTop($("#messages_complain")[0].scrollHeight - $("#messages_complain")[0].clientHeight);

    function return_complain() {
        var chat_url = 'crm_controller/list_complain';
        document.cookie = "chat_complain_admin=" + chat_url;
        loadView(chat_url, '.complain');
	}

    function reply_complain() {
		data = {
			title: '<?php echo $title ?>',
            inv: '<?php echo $inv ?>',
			code: '<?php echo $code ?>',
            member: $("#member_email_complain").val(),
            message: $("#message_complain").val()
		}

		console.log("data : ", data);
		postData('crm_controller/reply_complain', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					loadView('crm_controller/complain_chat/<?php echo $id ?>', '.complain');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

    function close_complain(inv, code) {
		btn_d_c.click();
		$("#message-d-c").html("Apakah anda yakin ingin menutup komplen dengan No Invoice Pembayaran : " + inv + " ?");
		$("#yes-d-c").attr("onclick", "do_close_complain('" + inv + "','" + code + "')");
	}

	function do_close_complain(inv, code) {
		var data = {
			code: code
		}
		postData('crm_controller/close_complain', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					loadView('crm_controller/complain_chat/<?php echo $id ?>', '.complain');
					btn_s.click();
					$("#message-s").html('Komplen dengan No Invoice Pembayaran '+inv+' telah ditutup !');
				} else {
					btn_e.click();
					$("#message-e").html('Gagal menutup komplen !');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>