<div class="messages" style="height:60vh;overflow:auto;overflow-y:scroll;padding:20px;border:1px solid #f89406;border-radius:5px;margin-top:2%;">
	<?php foreach ($chat as $key => $value) {?>
	<?php $index = 0; if($check_unread[$index] == 0){ ?>
		<div onclick="complain_chat(<?php echo $value['id'] ?>)" class="item item-visible" style="cursor:pointer">
	<?php }else{?>
		<div onclick="complain_chat(<?php echo $value['id'] ?>)" class="item item-visible" style="cursor:pointer;border:1px solid red;border-radius:5px;">
	<?php }?>
		<div class="text">
			<div class="heading">
				<a>
					Judul Komplen :
					<?php echo $value['title']?> (<?php echo $value['total_chat'] ?> Chat)
					<span class="date">
					</span>
				</a>
			</div>
			No Invoice Pembayaran : <?php echo $value['inv'] ?>
			<br>
			Dari :
			<?php if($value['from_email'] != $_SESSION['com_in']['email']){ ?>
			<?php echo $value['from_email']; }else if($value['to_email'] != $_SESSION['com_in']['email']){?>
			<?php echo $value['to_email']; }?>
			<br>
			Status :
			<?php if($value['status'] == 'close'){?>
			<?php echo strtoupper($value['status']) ?>
			<?php }else{?>
				AKTIF
			<?php }?>
		</div>
	</div>
	<?php $index++; }?>
</div>

<script>
	function complain_chat(id) {
		var chat_url = 'crm_controller/complain_chat/' + id;
		document.cookie = "chat_complain_admin=" + chat_url;
		loadView(chat_url, '.complain');
	}
</script>