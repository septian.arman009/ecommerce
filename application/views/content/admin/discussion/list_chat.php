<div class="form-horizontal">

	<div class="form-group">
		<label class="col-md-3 col-xs-12 control-label">Member</label>
		<div class="col-md-6 col-xs-12">
			<select id="member_email" class="form-control">
				<?php foreach ($members as $key => $value) { ?>
					<option value="<?php echo $value['email'] ?>"><?php echo $value['email'] ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-3 col-xs-12 control-label">Judul Diskusi</label>
		<div class="col-md-6 col-xs-12">
		<input onkeyup="check()" id="title" type="email" class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-3 col-xs-12 control-label"></label>
		<div class="col-md-6 col-xs-12">
			<a id="add-btn" onclick="add_discussion()" class="file-input-wrapper btn btn-default  fileinput btn-primary">
				<span>Mulai Diskusi</span>
			</a>
		</div>
	</div>

</div>
<div class="messages" style="height:60vh;overflow:auto;overflow-y:scroll;padding:20px;border:1px solid #f89406;border-radius:5px;margin-top:2%;">
	<?php $index = 0; foreach ($chat as $key => $value) {?>
	<?php if($check_unread[$index] == 0){ ?>
	<div onclick="discussion_chat(<?php echo $value['id'] ?>)" class="item item-visible" style="cursor:pointer">
	<?php }else{?>
	<div onclick="discussion_chat(<?php echo $value['id'] ?>)" class="item item-visible" style="cursor:pointer;border:1px solid red;border-radius:5px;">
	<?php }?>
		<div class="text">
			<div class="heading">
				<a>
					Judul Diskusi :
					<?php echo $value['title']?> (<?php echo $value['total_chat'] ?> Chat)

					<span class="date">
					</span>
				</a>
			</div>
			Dari :
			<?php if($value['from_email'] != $_SESSION['com_in']['email']){ ?>
			<?php echo $value['from_email']; }else if($value['to_email'] != $_SESSION['com_in']['email']){?>
			<?php echo $value['to_email']; }?>
			<br>
			Status :
			<?php if($value['status'] == 'close'){?>
			<?php echo strtoupper($value['status']) ?>
			<?php }else{?>
				AKTIF
			<?php }?>
		</div>
	</div>
	<?php $index++; }?>
</div>

<script>
	check();
	function discussion_chat(id) {
		var chat_url = 'crm_controller/discussion_chat/' + id;
		document.cookie = "chat_admin=" + chat_url;
		loadView(chat_url, '.discussion');
	}

	function check(){
		var title = $("#title").val();
		if(title != ''){
			$("#add-btn").removeAttr('disabled');
		}else{
			$("#add-btn").attr('disabled', 'disabled');
		}
	}

	function add_discussion() {
		data = {
			title: $("#title").val(),
			member: $("#member_email").val()
		}

		console.log("data : ", data);
		postData('crm_controller/add_discussion', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					main_menu('#','crm_controller/discussion');
					btn_s.click();
					$("#message-s").html('Diskusi telah ditambahkan !');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>