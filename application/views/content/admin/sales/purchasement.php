<ul class="breadcrumb">
	<li>
		<a href="#">Penjualan</a>
	</li>
	<li class="active">Pembayaran Masuk</li>
</ul>

<div class="page-content-wrap">

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Daftar Pembayaran</h3>
				</div>
				<div class="panel-body">
					<table id="purchasement-table" class="table stripe hover">
						<thead>
							<tr>
								<th id="th" width="10%">ID</th>
								<th id="th">Nomor Invoice Pembayaran</th>
                                <th id="th">Total Transfer</th>
                                <th id="th">Status</th>
								<th id="th" class="no-sort" width="10%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">ID</th>
								<th class="footer">No Invoice Pembayaran</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->
		</div>
	</div>
</div>

<script>
	$(document).ready(function () {
		//Fungsi untuk kolom pencarian pada footer table
		$('#purchasement-table tfoot th').each(function () {
			var title = $(this).text();
			var inp = '<input type="text" class="form-control footer-s" placeholder="' + title + '" />';
			$(this).html(inp);
		});

		//Fungsi untuk memanggil datatables di controller localhost/JST/tabel/datatables_user
		//dan ditampilkan ke dalam tabel dengan id user_view
		var table = $('#purchasement-table').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": 'sales_controller/show_purchasement',
				"type": "POST"
			}
		});

		//Fungsi ketika kolom pencarian terisi oleh data
		table.columns().every(function () {
			var that = this;
			$('input', this.footer()).on('keyup change', function () {
				if (that.search() !== this.value) {
					that.search(this.value).draw();
				}
			});
		});
	});

	function detail(id) {
		sub_menu('#sales', '#purchasement', 'sales_controller/purchasement_detail/' + id + '/purchasement');
	}
</script>

<style>
	#purchasement-table_filter {
		display: none;
	}
</style>