<ul class="breadcrumb">
	<li>
		<a href="#">Penjualan</a>
	</li>
	<li class="active">Tagihan Masuk</li>
</ul>

<div class="page-content-wrap">

	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">

				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 class="panel-title">
							<strong>
								Status Pengiriman
								<a onclick="prev()" class="btn btn-default">
									<i class="fa fa-arrow-left"></i>
								</a>
								<?php echo ' Page : ' . $page . ' From ' . $all ?>
								<a onclick="next()" class="btn btn-default">
									<i class="fa fa-arrow-right"></i>
								</a>
							</strong>
						</h3>
					</div>

					<div class="panel-body">
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Cari Invoice Pembayaran</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-user"></span>
									</span>
									<input onchange="search_inv()" id="search" style="color:black;" value="<?php if($key_pass!='null'){echo $key_pass;} ?>" type="text" class="form-control">
								</div>
							</div>
						</div>
					</div>

					<div class="panel-body">
						<?php if($order){ ?>
						<?php $no = count($order); foreach ($order as $key => $value) { ?>

						<div class="messages">
							<div class="item item-visible">
								<div class="text">
									<div class="heading">
										<a>
											<a style="font-size:18px;font-weight:bold;" onclick="sub_menu('#sales', '#purchasement', 'sales_controller/purchasement_detail/<?php echo $value['id'] ?>')">(#
												<?php echo $no--; ?>) -
												<?php echo $value['purchasement_inv'] ?>
											</a>
											

											<span style="cursor:pointer;" onclick="show_wizard('#wizard<?php echo $value['id'] ?>')" class="pull-right">Tmpilkan Status</span>
											<span class="date">
												<div class="form-group">
													<label class="col-md-3 col-xs-12 control-label"></label>
													<div class="col-md-6 col-xs-12">
														<div class="input-group">
															<span class="input-group-addon">
																<span>Resi</span>
															</span>
															<?php if($_SESSION['com_in']['role'] == 4){ ?>
															<input disabled value="<?php echo $value['receipt_number'] ?>" type="text" class="form-control">
															<?php }else{ ?>
															<input id="resi<?php echo $value['id'] ?>" onchange="save_resi(<?php echo $value['id'] ?>)" value="<?php echo $value['receipt_number'] ?>" type="text" class="form-control">
															<?php }?>
														</div>
													</div>
												</div>
											</span>
										</a>
									</div>

									<div style="display:none" class="wizard" id="wizard<?php echo $value['id'] ?>">
										<div class="wizard-inner">
											<div class="connecting-line"></div>
											<ul class="nav nav-tabs" role="tablist">
												
												<li role="presentation" class="active">
													<a data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
														<span class="round-tab">
															<i class="fa fa-money"></i>
														</span>
													</a>
												</li>

												<?php if($value['status'] == 'sent' || $value['status'] == 'delivered' || $value['status'] == 'finish'){ ?>
													<li role="presentation" class="active">
												<?php }else{?>
													<li role="presentation" class="disabled">
												<?php }?>
													<a data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
														<span class="round-tab">
															<i class="fa fa-truck"></i>
														</span>
													</a>
												</li>

												<?php if($value['status'] == 'delivered'){ ?>
													<li role="presentation" class="active">
												<?php }else{?>
													<?php if($_SESSION['com_in']['role'] == 4){ ?>
														<li role="presentation" disabled>
													<?php }else{?>
														<li role="presentation" onclick="delivered(<?php echo $value['id'] ?>)">													
													<?php }?>
												<?php }?>
													<a data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
														<span class="round-tab">
															<i class="fa fa-dropbox"></i>
														</span>
													</a>
												</li>

												<?php if($value['status'] == 'delivered'){ ?>
													<li role="presentation" class="active">
												<?php }else{?>
													<li role="presentation" class="disabled">
												<?php }?>
													<a data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
														<span class="round-tab">
															<i class="fa fa-check-square-o"></i>
														</span>
													</a>
												</li>
											</ul>
													
										</div>
									</div>

								<?php if($value['status'] == 'delivered' && $value['satisfaction'] != ''){ ?>
								<div class="center" style="text-align:center;margin-top:5%;">
									<?php if($value['review'] != ''){ ?>
									<p>Ulasan Member : <?php echo $value['review'] ?></p>
									<?php }else{?>
									<p>Ulasan Member : Member tidak memberikan ulasan</p>
									<?php }?>
								</div>
								<?php }?>

								</div>
							</div>
						</div>

						<?php } ?>
						<?php }else{?> No Data !
						<?php }?>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function show_wizard(id) {
		$(id).slideToggle();
	}

	String.prototype.replaceAll = function(str1, str2, ignore)
	{
		return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
	}

	function search_inv() {
		var key = $("#search").val().replace(/[^A-Za-z0-9/]/g, "");
		var key_pass = key.replaceAll('/','-',key);
		if (key_pass == '') {
			key_pass = 'null';
		}

		loadView('sales_controller/track_order/1/' + key_pass, '.content');

	}

	function next() {
		var page = <?php echo $page + 1 ?>;
		
		var key = $("#search").val().replace(/[^A-Za-z0-9/]/g, "");
	
		key_pass = key.replaceAll('/','-',key);
		if (key_pass == '') {
			key_pass = 'null';
		}

		if (page >= 0) {
			loadView('sales_controller/track_order/' + page + '/' + key_pass, '.content');
		}
	}

	//replace(/[^A-Za-z]/g, "");

	function prev() {
		var page = <?php echo $page - 1 ?>;
		var key = $("#search").val().replace(/[^A-Za-z0-9/]/g, "");
		key_pass = key.replaceAll('/','-',key);
		if (key_pass == '') {
			key_pass = 'null';
		}
		if (page > 0) {
			loadView('sales_controller/track_order/' + page + '/' + key_pass, '.content');
		}
	}

	function save_resi(id){
		data = {
			id: id,
			resi: $("#resi"+id).val()
		}
		console.log("data : ", data);
		postData('sales_controller/save_resi', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					var page = <?php echo $page ?>;
		
					var key = $("#search").val().replace(/[^A-Za-z0-9/]/g, "");
				
					key_pass = key.replaceAll('/','-',key);
					if (key_pass == '') {
						key_pass = 'null';
					}

					loadView('sales_controller/track_order/' + page + '/' + key_pass, '.content');
					
					btn_s.click();
					$("#message-s").html('Resi telah disimpan !');
				}else{
					btn_e.click();
					$("#message-e").html('Gagal menyimpan resi !');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function delivered(id) {
		btn_d_c.click();
		$("#message-d-c").html("Apakah anda yakin ingin mengubah status menjadi diterima ?");
		$("#yes-d-c").attr("onclick", "do_delivered(" + id + ")");
	}

	function do_delivered(id) {
		var page = <?php echo $page ?>;
		var key = $("#search").val().replace(/[^A-Za-z0-9/]/g, "");
		key_pass = key.replaceAll('/','-',key);
		if (key_pass == '') {
			key_pass = 'null';
		}
		var data = {
			id: id
		}
		postData('sales_controller/delivered', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					loadView('sales_controller/track_order/' + page + '/' + key_pass, '.content');
					btn_s.click();
					$("#message-s").html('Status telah diubah !');
				} else {
					btn_e.click();
					$("#message-e").html('Gagal mengubah status !');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>

<style>
	.wizard {
		margin: 20px auto;
		background: #fff;
	}

	.wizard .nav-tabs {
		position: relative;
		margin: 40px auto;
		margin-bottom: 0;
		border-bottom-color: #e0e0e0;
	}

	.wizard>div.wizard-inner {
		position: relative;
	}

	.connecting-line {
		height: 2px;
		background: #e0e0e0;
		position: absolute;
		width: 80%;
		margin: 0 auto;
		left: 0;
		right: 0;
		top: 50%;
		z-index: 1;
	}

	.wizard .nav-tabs>li.active>a,
	.wizard .nav-tabs>li.active>a:hover,
	.wizard .nav-tabs>li.active>a:focus {
		color: #555555;
		cursor: default;
		border: 0;
		border-bottom-color: transparent;
	}

	span.round-tab {
		width: 70px;
		height: 70px;
		line-height: 70px;
		display: inline-block;
		border-radius: 100px;
		background: #fff;
		border: 2px solid #e0e0e0;
		z-index: 2;
		position: absolute;
		left: 0;
		text-align: center;
		font-size: 25px;
	}

	span.round-tab i {
		color: #555555;
	}

	.wizard li.active span.round-tab {
		background: #fff;
		border: 2px solid #5bc0de;

	}

	.wizard li.active span.round-tab i {
		color: #5bc0de;
	}

	span.round-tab:hover {
		color: #333;
		border: 2px solid #333;
	}

	.wizard .nav-tabs>li {
		width: 25%;
	}

	.wizard li:after {
		content: " ";
		position: absolute;
		left: 46%;
		opacity: 0;
		margin: 0 auto;
		bottom: 0px;
		border: 5px solid transparent;
		border-bottom-color: #5bc0de;
		transition: 0.1s ease-in-out;
	}

	.wizard li.active:after {
		content: " ";
		position: absolute;
		left: 46%;
		opacity: 1;
		margin: 0 auto;
		bottom: 0px;
		border: 10px solid transparent;
		border-bottom-color: #5bc0de;
	}

	.wizard .nav-tabs>li a {
		width: 70px;
		height: 70px;
		margin: 20px auto;
		border-radius: 100%;
		padding: 0;
	}

	.wizard .nav-tabs>li a:hover {
		background: transparent;
	}

	.wizard .tab-pane {
		position: relative;
		padding-top: 50px;
	}

	.wizard h3 {
		margin-top: 0;
	}

	@media( max-width: 585px) {

		.wizard {
			width: 90%;
			height: auto !important;
		}

		span.round-tab {
			font-size: 16px;
			width: 50px;
			height: 50px;
			line-height: 50px;
		}

		.wizard .nav-tabs>li a {
			width: 50px;
			height: 50px;
			line-height: 50px;
		}

		.wizard li.active:after {
			content: " ";
			position: absolute;
			left: 35%;
		}
	}
</style>