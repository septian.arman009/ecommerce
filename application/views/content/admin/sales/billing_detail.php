<ul class="breadcrumb">
	<li>
		<a href="#">Penjualan</a>
	</li>
	<li class="active">Tagihan Masuk</li>
	<li class="active">Detail Tagihan</li>

</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">

			<div class="form-horizontal">

				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 class="panel-title">
							<strong><?php echo $billing_inv ?></strong></h3>
					</div>
					<button onclick="sub_menu('#sales','#billing','sales_controller/billing')" class="btn btn-default">
						<i class="fa fa-arrow-left"></i>
					</button>

					<div class="form-group">
						<label class="col-md-3 col-xs-12 control-label">Nama Penerima</label>
						<div class="col-md-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">
									<span class="fa fa-user"></span>
								</span>
								<input readonly style="color:black;" value="<?php echo $address[0]['name'] ?>" type="text" class="form-control">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 col-xs-12 control-label">Telpon</label>
						<div class="col-md-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">
									<span class="fa fa-phone"></span>
								</span>
								<input readonly style="color:black;" value="<?php echo $address[0]['phone'] ?>" type="text" class="form-control">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 col-xs-12 control-label">Deskripsi Barang</label>
						<div class="col-md-6 col-xs-12">
							<textarea readonly style="color:black;" class="form-control" rows="5"><?php echo $address[0]['address'] ?></textarea>
						</div>
					</div>
					<div style="margin:auto;text-align:center;" class="images">
						<img style="width:200px;" id="image">
					</div>

					<div class="form-group">
						<label class="col-md-3 col-xs-12 control-label">Provinsi</label>
						<div class="col-md-6 col-xs-12">
							<input readonly style="color:black;" type="text" class="form-control" value="<?php echo $province ?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 col-xs-12 control-label">Kota</label>
						<div class="col-md-6 col-xs-12">
							<input readonly style="color:black;" type="text" class="form-control" value="<?php echo $city ?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 col-xs-12 control-label">Kecamatan</label>
						<div class="col-md-6 col-xs-12">
							<input readonly style="color:black;" type="text" class="form-control" value="<?php echo $district ?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 col-xs-12 control-label">Kode Pos</label>
						<div class="col-md-6 col-xs-12">
								<input readonly style="color:black;" value="<?php echo $address[0]['postcode'] ?>" type="text" class="form-control">
						</div>
					</div>

                    <div class="form-group">
						<label class="col-md-3 col-xs-12 control-label">Bank Tujuan</label>
						<div class="col-md-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">
									<span class="fa fa-credit-card"></span>
								</span>
								<input readonly style="color:black;" value="<?php echo $receiver_bank[0]['bank_name'].' a/n '.$receiver_bank[0]['owner'].', No Rek : '.$receiver_bank[0]['account'] ?>" type="text" class="form-control">
							</div>
						</div>
					</div>


                    <div class="form-group">
						<label class="col-md-3 col-xs-12 control-label"></label>
						<div class="col-md-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">
                                <a style="color:white;text-decoration:none" href="<?php echo base_url() ?>assets/payments/<?php echo $p_o_pay ?>" target="_blank">
									Lihat Bukti Pembayaran
                                </a>
								</span>
								
							</div>
						</div>
					</div>

                    <div class="form-group">
						<label class="col-md-3 col-xs-12 control-label"></label>
						<div class="col-md-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon" style="background-color:#f89406;border-color:#f89406">
                                <a onclick="confirm(<?php echo $id ?>)" style="color:white;text-decoration:none">
									Konfirmasi Pembayaran ?
                                </a>
								</span>
								
							</div>
						</div>
					</div>

					<div class="panel-body">
						<table class="table stripe hover">
							<thead>
								<th>No</th>
								<th>Nama Produk</th>
								<th>Qty</th>
								<th>Harga Satuan</th>
								<th>Catatan Pembeli</th>
								<th>Total</th>
							</thead>

							<tbody>
								<?php $index = 0; $no = 0; foreach ($product_id as $key => $value) { ?>
								<tr>
									<td>
										<?php echo $index+1 ?>
									</td>
									<td>   
										<?php echo $name[$index] ?>
                                        <br>
                                        <img width="100px" src="<?php echo base_url() ?>assets/products/<?php echo $product_image[$index] ?>">
									</td>
									<td>
										<?php echo $product_qty[$index] ?> Pcs
									</td>
									<td>
										<?php echo $this->mylib->torp($product_price[$index]) ?>
									</td>
									<td>
										<?php echo $buyer_notes[$index] ?>
									</td>
									<td>
										<?php echo $this->mylib->torp($product_subtotal[$index]) ?>
									</td>
								</tr>
								<?php $index++; } ?>
								<tr>
									<td>
									</td>
									<td>   
										
									</td>
									<td>
									</td>
									<td>
									</td>
									<td>
										<b>Total</b>
									</td>
									<td>
									<b><?php echo $this->mylib->torp($discount_ammount + $billing_transfer) ?></b>
									</td>
								</tr>
								<tr>
									<td>
									</td>
									<td>   
										
									</td>
									<td>
									</td>
									<td>
									</td>
									<td>
									<b>Diskon (<?php echo $discount.'%' ?>)</b>
									</td>
									<td>
									<b><?php echo $this->mylib->torp($discount_ammount) ?></b>
									</td>
								</tr>
								<tr>
									<td>
									</td>
									<td>   
										
									</td>
									<td>
									</td>
									<td>
									</td>
									<td>
									<b>Total Transfer</b>
									</td>
									<td>
									<b><?php echo $this->mylib->torp($billing_transfer) ?></b>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="panel-footer">

					</div>

				</div>

			</div>

		</div>
	</div>
</div>

<script>
function confirm(id) {
		btn_d_c.click();
		$("#message-d-c").html("Apakah anda yakin ingin melakukan Konfirmsi untuk No Tagihan <?php echo $billing_inv ?> ?");
		$("#yes-d-c").attr("onclick", "do_confirm(" + id + ")");
	}

	function do_confirm(id) {
		var data = {
			id: id
		}
		postData('sales_controller/purchase_confirmation', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					sub_menu("#sales", "#billing", "sales_controller/billing");
					btn_s.click();
					$("#message-s").html('Konfirmasi Berhasil !');
				} else {
					btn_e.click();
					$("#message-e").html('Gagal melakukan konfirmasi !');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>