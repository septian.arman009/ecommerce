<ul class="breadcrumb">
	<li>
		<a href="#">Toko</a>
	</li>
	<li class="active">Produk</li>
	<li class="active">Produk Kategori</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div style="margin-bottom:1%;" class="panel-heading ui-draggable-handle">
					<h3 class="panel-title">
						<strong>Daftar </strong> Categori</h3>
				</div>

				<form class="form-horizontal">
					<div class="panel-body">

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Produk Kategori</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-puzzle-piece"></span>
									</span>
									<input onkeyup="check()" id="product_categories" type="text" class="form-control">
									<span class="input-group-addon">
										<span id="save" class="fa fa-plus">
                                            <a id="save-btn" onclick="save()" style="color:white;text-decoration:none;"> Tambah</a>
                                            <a id="save-loading" style="display:none;color:white;text-decoration:none;"> Tunggu ..</a>
										</span>
										<span id="save_error" class="fa fa-plus">
											<a style="color:white;text-decoration:none;"> Tambah</a>
										</span>
									</span>
								</div>
							</div>
						</div>

						<table id="user-table" class="table stripe hover">
							<thead>
								<tr>
									<th id="th" width="5%">No</th>
									<th id="th" >Produk Kategori</th>
								</tr>
							</thead>
							<tbody>
								<?php $no =1; foreach ($categories as $key => $value) { ?>
								<tr>
									<td>
										<?php echo $no++ ?>
									</td>
									<td>
										<div class="input-group">
											<input id="product_categories<?php echo $value['id'] ?>" value="<?php echo $value['categories'] ?>" type="text" class="form-control">

											<span id="update<?php echo $value['id'] ?>" class="input-group-addon">
												<a style="color:white;text-decoration:none;" onclick="update(<?php echo $value['id'] ?>)">Perbarui</a>
											</span>
											<span style="display:none;" id="update_loading<?php echo $value['id'] ?>" class="input-group-addon">
												<a style="color:white;text-decoration:none;">Tunggu ..</a>
											</span>
											<span class="input-group-addon">
												<a style="color:white;text-decoration:none;" onclick="destroy(<?php echo $value['id'] ?>)">Hapus</a>
											</span>
										</div>
									</td>
								</tr>
								<?php }?>
							</tbody>
						</table>

					</div>

				</form>

			</div>
		</div>
	</div>
</div>

<script>
	check();

	function check() {
		var categories = $("#product_categories").val();

		if (categories != '') {
			$("#save").show();
			$("#save_error").hide();
		} else {
			$("#save").hide();
			$("#save_error").show();
		}
	}

	function save() {
        $("#save-btn").hide();
		$("#save-loading").show();
		data = {
			categories: $("#product_categories").val()
		}
		console.log("data : ", data);
		postData('content_controller/save_categories/null', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					btn_s.click();
					$("#message-s").html('Data has been saved');
					sub_menu("#shop", "#categories", 'content_controller/categories');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

    function update(id) {
		$("#update" + id).hide();
		$("#update_loading" + id).show();
        data = {
			categories: $("#product_categories"+id).val()
		}
        console.log("data : ", data);
		postData('content_controller/save_categories/'+id, data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					btn_s.click();
					$("#message-s").html('Data has been saved');
					sub_menu("#shop", "#categories", 'content_controller/categories');
				}
			} else {
				console.log('ini error : ', err);
			}
		});

	}

    function destroy(id) {
		btn_d_c.click();
		$("#message-d-c").html("Apakah anda yakin ingin menghapus Kategori dengan ID : " + id + " ?");
		$("#yes-d-c").attr("onclick", "do_delete(" + id + ")");
	}

	function do_delete(id) {
		var data = {
			id: id
		}
		postData('main_controller/destroy/categories/id/', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					btn_s.click();
					$("#message-s").html('Kategori telah di hapus !');
					sub_menu('#shop', '#categories', 'content_controller/categories');
				} else if (response.status == 'used'){
					btn_e.click();
					$("#message-e").html('Kategori masih digunakan untuk beberapa produk !');
				} else {
					btn_e.click();
					$("#message-e").html('Gagal menghapus kategori !');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>