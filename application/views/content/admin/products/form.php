<script src="<?php echo base_url() ?>assets/admin/myjs/bootstrap-tagsinput.min.js"></script>
<ul class="breadcrumb">
	<li>
		<a href="#">Toko</a>
	</li>
	<li class="active">Produk</li>
	<?php if($id == 'null') {?>
		<li class="active">Tambah Produk</li>
	<?php }else{?>
		<li class="active">Ubah Produk</li>
	<?php }?>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">

			<div class="form-horizontal">

				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 class="panel-title">
						<?php if($id == 'null') {?>
							<strong>Tambah</strong> Produk</h3>
						<?php }else{?>
							<strong>Ubah</strong> Produk</h3>
						<?php }?>	
					</div>
					<button onclick="sub_menu('#shop','#products','content_controller/products/<?php echo $page.'/'.$key_pass.'/'.$categori ?>')" class="btn btn-default">
						<i class="fa fa-arrow-left"></i>
					</button>

					<div class="panel-body" style="min-height:50vh;">
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Pilih Gambar</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-picture-o"></span>
									</span>
									<input type="file" name="file" class="form-control">
								</div>
								<span class="help-block">Silakan gunakan gambar 276 x 357 untuk keindahan tampilan</span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Nama Produk</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-pencil"></span>
									</span>
									<input onkeyup="check()" id="product_name" type="text" class="form-control">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Kategori Produk</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-puzzle-piece"></span>
									</span>
									<select onchange="check()" id="product_categories" class="form-control">
										<option value="">- Pilih Kategori -</option>
										<?php foreach ($categories as $key => $value) { ?>
											<option value="<?php echo $value['id'] ?>"><?php echo $value['categories'] ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Harga Satuan</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-money"></span>
									</span>
									<input onkeyup="check()" id="unit_price" type="text" class="form-control">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Perkiraan Berat</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span>Gr</span>
									</span>
									<input onkeyup="check()" id="weight" type="number" min="0" max="100" class="form-control">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Stok</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span>-/+</span>
									</span>
									<input onkeyup="check()" id="stock" type="number" min="0" max="500" class="form-control">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Warna</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<input onchange="check()" id="color" type="text" data-role="tagsinput" />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Ukuran</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<input onchange="check()" id="size" type="text" data-role="tagsinput" />
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12">
								<label class="col-md-3 control-label">Ketersediaan</label>
								<div class="col-md-4">
									<label class="switch switch-small">
										<input id="condition" type="checkbox">
										<span></span>
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Deskripsi Barang</label>
							<div class="col-md-6 col-xs-12">
								<textarea id="description" onkeyup="check()" class="form-control" rows="5"></textarea>
								<span class="help-block">Max 240 karakter</span>
							</div>
						</div>
						<div style="margin:auto;text-align:center;" class="images">
							<img style="width:200px;" id="image">
						</div>
						

					</div>

					<div class="panel-footer">
						<a onclick="reset()" class="btn btn-default">Bersihkan Data</a>
						<a disabled id="save" onclick="action()" class="btn btn-primary pull-right">Simpan</a>
						<a style="display:none" id="loading" class="btn btn-primary pull-right">Tunggu ..</a>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>

<script>
	 $('#color').tagsinput({
      allowDuplicates: false,
        itemText: 'text'
    });

	$('#unit_price').maskMoney({
		prefix: 'Rp. ',
		thousands: '.',
		decimal: ',',
		precision: 0
	});

	var id  = '<?php echo $id ?>';

	if(id !='null'){
		detail(id);
	}

	function check() {
	
		var product_name = $("#product_name").val();
		var product_categories = $("#product_categories").val();
		var unit_price = $("#unit_price").val();
		var weight = $("#weight").val();
		var stock = $("#stock").val();
		var description = $("#description").val();

		if (product_name != '' && product_categories != '' && unit_price != '' &&
			weight != '' && stock != '' && description != ''){
                $("#save").removeAttr('disabled');
            }else{
                $("#save").attr('disabled', 'disabled');
            }
	}

	function detail(id){
		data = {
			id: id,
			table: 'products'
		}
		console.log("data : ", data);
		postData('main_controller/detail', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					$("#product_name").val(response.data[0].product_name);
					$("#product_categories").val(response.data[0].product_categories);
					$("#unit_price").val(response.data[0].unit_price);
					$("#weight").val(response.data[0].weight);
					$("#stock").val(response.data[0].stock);
					$("#description").html(response.data[0].description);

					if(response.data[0].condition == 'tersedia'){
						$("#condition").attr('checked','checked');
					}else{
						$("#condition").removeAttr('checked');
					}

					$('#color').tagsinput('add', response.data[0].color);
					$('#size').tagsinput('add', response.data[0].size);

					$("#image").attr('src', '<?php echo base_url() ?>assets/products/'+response.data[0].image);

					check();
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

    function action(){

        $("#save").hide();
		$("#loading").show();

        if (_("condition").checked) {
			var condition = 'tersedia';
		} else {
			var condition = 'bekas';
		}
		
		var inputFile = $('input[name=file]');
		var fileToUpload = inputFile[0].files[0];

		var formData = new FormData();
		formData.append("id", id);
		formData.append("color", $("#color").val());
		formData.append("size", $("#size").val());
		formData.append("product_name", $("#product_name").val());
		formData.append("product_categories", $("#product_categories").val());
        formData.append("unit_price", $("#unit_price").val());
        formData.append("weight", $("#weight").val());
        formData.append("stock", $("#stock").val());
        formData.append("condition", condition);
        formData.append("description", $("#description").val());
		formData.append("file", fileToUpload);
        
        if(id =='null'){
			var message = "Data telah tersimpan !";
		}else{
			var message = "Data berhasil diubah !";
		}
        
        if(fileToUpload || id != 'null'){
            $.ajax({
                url: 'content_controller/save_product',
                type: 'post',
                data: formData,
                processData: false,
                contentType: false,
                success: function () {
                    $("#save").show();
                    $("#loading").hide();
                    btn_s.click();
                    $("#message-s").html(message);
                    sub_menu('#shop', '#products', 'content_controller/products/<?php echo $page.'/'.$key_pass.'/'.$categori ?>');
                }
            });
        }else{
			$("#save").show();
            $("#loading").hide();
            btn_e.click();
			$("#message-e").html("File gambar masih kosong !");
        }
		
    }
</script>