<ul class="breadcrumb">
	<li>
		<a href="#">Toko</a>
	</li>
	<li class="active">Produk</li>
	<li class="active">Daftar Produk</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div style="margin-bottom:1%;" class="panel-heading ui-draggable-handle">
					<h3 class="panel-title">
						<strong>Daftar </strong> Produk</h3>
				</div>


				<form class="form-horizontal">
					<div class="panel-body" style="border-bottom:1px solid #ccc;margin-bottom:2%;">
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Kategori Produk</label>
							<div class="col-md-5 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-puzzle-piece"></span>
									</span>
									<select id="product_categories" class="form-control">
										<option value="">- Pilih Kategori -</option>
										<?php foreach ($categories as $key => $value) { ?>

										<?php if($value['id'] == $categori){ ?>
										<option selected value="<?php echo $value['id'] ?>">
											<?php echo $value['categories'] ?>
										</option>
										<?php }else{?>
										<option value="<?php echo $value['id'] ?>">
											<?php echo $value['categories'] ?>
										</option>
										<?php }?>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Nama Produk</label>
							<div class="col-md-5 col-xs-12">
								<div class="input-group">
									<input value="<?php if($key_pass != 'null'){echo $key_pass;} ?>" type="text" id="search" class="form-control">
									<span class="input-group-addon">
										<a onclick="search_product()" style="color:white;text-decoration:none;" class="fa fa-search"> Cari</a>
									</span>
									<span class="input-group-addon">
										<a onclick="sub_menu('#shop','#products','content_controller/form/null/<?php echo $page.'/'.$key_pass.'/'.$categori ?>')" style="color:white;text-decoration:none;" class="fa fa-plus">
											Tambah</a>
									</span>
								</div>
								<br>
							</div>
							<a onclick="prev()" class="btn btn-default">
								<i class="fa fa-arrow-left"></i></a>
							<?php echo ' Halaman : ' . $page . ' Dari ' . $all ?>
							<a onclick="next()" class="btn btn-default">
								<i class="fa fa-arrow-right"></i>
							</a>
						</div>
					</div>
				</form>

				<?php if($products){ ?>
				<?php foreach ($products as $key => $value) { ?>
				<div class="col-md-4">
					<div class="panel panel-default">
						<div style="margin: auto;text-align:center;height:45vh;margin-bottom:5%;" class="panel-body panel-body-image">
							<img style="width:200px;" src="<?php echo base_url() ?>assets/products/<?php echo $value['image'] ?>" alt="Ocean">
							<a onclick="sub_menu('#shop', '#products', 'content_controller/form/<?php echo $value['id'].'/'.$page.'/'.$key_pass.'/'.$categori ?>')" class="panel-body-inform">
								<span class="fa fa-pencil"></span>
							</a>
						</div>
						<div class="panel-body">
							<h3>
								<?php echo $value['product_name'] ?>
							</h3>
							<p>
								<?php echo $this->mylib->torp($value['unit_price']) ?>
							</p>
						</div>
						<div class="panel-footer text-muted">
							<a style="border-radius: 10px;" onclick="destroy(<?php echo $value['id'] ?>)" class="btn btn-danger btn-xs">
								<i class="fa fa-trash-o"> Hapus</i>
							</a>
							<span style="color:black;" class="fa fa-clock-o">
								<?php echo $value['created_at'] ?>
							</span>
							&nbsp;&nbsp;&nbsp;
							<span style="color:black;">Stock :
								<?php  echo $value['stock'] ?>
							</span>
						</div>
					</div>
				</div>
				<?php } }else{?>
				<div class="col-md-4">
					<div class="panel panel-default">
						<div style="margin: auto;text-align:center;height:45vh;margin-bottom:5%;" class="panel-body panel-body-image">
							<img style="width:200px;" src="<?php echo base_url() ?>assets/no_image.png" alt="Ocean">
							<a class="panel-body-inform">
								<span class="fa fa-pencil"></span>
							</a>
						</div>
						<div class="panel-body">
							<h3>
								Tidak ada data
							</h3>
							<p>
								Tidak ada data
							</p>
						</div>
						<div class="panel-footer text-muted">
							<span style="color:black;" class="fa fa-clock-o"> -</span>
							&nbsp;&nbsp;&nbsp;
							<span style="color:black;">Stock : -</span>
						</div>
					</div>
				</div>
				<?php }?>

			</div>
		</div>
	</div>
</div>

<script>
	function search_product() {
		var key = $("#search").val();
		var key_pass = key.replace(/[^A-Za-z]/g, "");
		if (key_pass == '') {
			key_pass = 'null';
		}

		var categories = $("#product_categories").val();
		if (categories == '') {
			categories = 'null';
		}

		loadView('content_controller/products/1/' + key_pass + '/' + categories, '.content');

	}

	function next() {
		var page = <?php echo $page + 1 ?>;
		var key = $("#search").val();
		key_pass = key.replace(/[^A-Za-z]/g, "");
		if (key_pass == '') {
			key_pass = 'null';
		}

		var categories = $("#product_categories").val();
		if (categories == '') {
			categories = 'null';
		}

		if (page >= 0) {
			loadView('content_controller/products/' + page + '/' + key_pass + '/' + categories, '.content');
		}
	}

	function prev() {
		var page = <?php echo $page - 1 ?>;
		var key = $("#search").val();
		key_pass = key.replace(/[^A-Za-z]/g, "");
		if (key_pass == '') {
			key_pass = 'null';
		}

		var categories = $("#product_categories").val();
		if (categories == '') {
			categories = 'null';
		}

		if (page > 0) {
			loadView('content_controller/products/' + page + '/' + key_pass + '/' + categories, '.content');
		}
	}

	function destroy(id) {
		btn_d_c.click();
		$("#message-d-c").html("Apakah anda yakin ingin menghapus Produk dengan ID : " + id + " ?");
		$("#yes-d-c").attr("onclick", "do_delete(" + id + ")");
	}

	function do_delete(id) {
		var page = <?php echo $page ?>;
		var key = $("#search").val();
		key_pass = key.replace(/[^A-Za-z]/g, "");
		if (key_pass == '') {
			key_pass = 'null';
		}

		var categories = $("#product_categories").val();
		if (categories == '') {
			categories = 'null';
		}
		
		var data = {
			id: id
		}
		postData('main_controller/destroy/products/id/', data, function (err, response) {
			if (response) {
				console.log('berhasil : ', response);
				if (response.status == 'success') {
					btn_s.click();
					$("#message-s").html('Procuk telah dihapus !');
					sub_menu('#shop', '#products', 'content_controller/products/' + page + '/' + key_pass+'/'+categories);
				} else {
					btn_e.click();
					$("#message-e").html('Gagal menghapus produk !');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>