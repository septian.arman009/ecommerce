<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main_model extends CI_Model
{

    public function gda1p($table)
    {
        return $this->db->get($table)->result_array();
    }

    public function gda1po($table, $order_by, $flow)
    {
        return $this->db->query("select * from $table order by $order_by $flow")->result_array();
    }

    public function gda1pg($table, $group, $column, $id)
    {
        return $this->db->query("select * from $table where $column = '$id' group by $group")->result_array();
    }

    public function gda3p($table, $column, $id)
    {
        $this->db->where($column, $id);
        return $this->db->get($table)->result_array();
    }

    public function gda5p($table, $column, $id, $column1, $id1)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        return $this->db->get($table)->result_array();
    }

    public function gda7p($table, $column, $id, $column1, $id1, $column2, $id2)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        $this->db->where($column2, $id2);
        return $this->db->get($table)->result_array();
    }

    public function gdo4p($table, $take, $column, $id)
    {
        $this->db->where($column, $id);
        $query = $this->db->get($table)->result_array();
        if($query){
            if($query[0][$take]){
                return $query[0][$take];
            }
        }
    }

    public function update($table, $data, $column, $id)
    {
        $this->db->where($column, $id);
        return $this->db->update($table, $data);
    }

    public function update6p($table, $data, $column, $id, $column1, $id1)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        return $this->db->update($table, $data);
    }

    public function update8p($table, $data, $column, $id, $column1, $id1, $column2, $id2)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        $this->db->where($column2, $id2);
        return $this->db->update($table, $data);
    }

    public function store($table, $data)
    {
        return $this->db->insert($table, $data);
    }

    public function truncate($table)
    {
        return $this->db->query("truncate $table");
    }

    public function destroy($table, $column, $id)
    {
        $this->db->where($column, $id);
        return $this->db->delete($table);
    }

    public function destroy2where($table, $column, $id, $column1, $id1)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        return $this->db->delete($table);
    }

    public function max($tabel, $kolom)
    {
        $query = $this->db->query("select max($kolom)$kolom from $tabel")->result_array();
        if($query){
            if($query[0][$kolom]){
                return $query[0][$kolom];
            }
        }
    }

    public function maxwhere($tabel, $kolom, $column, $key)
    {
        $query = $this->db->query("select max($kolom)$kolom from $tabel where $column = '$key'")->result_array();
        if($query){
            if($query[0][$kolom]){
                return $query[0][$kolom];
            }
        }
    }

    public function last_where($table, $id, $column, $data)
    {
        $query = $this->db->query("select * from $table where $id = (select max($id) from $table where $column='$data')")->result_array();
        return $query;
    }

    public function max_all($tabel, $kolom)
    {
        $query = $this->db->query("select * from $tabel where $kolom=(select max($kolom) from $tabel)")->result_array();
        return $query;
    }

    public function count($table, $id)
    {
        $query = $this->db->query("select count($id) as total_row from $table")->result_array();
        if($query){
            if ($query[0]['total_row'] != '') {
                return $query[0]['total_row'];
            } else {
                return 0;
            }
        }else{
            return 0;
        }
       
    }

    public function countwhere($table, $id, $column, $key)
    {
        $query = $this->db->query("select count($id) as total_row from $table where $column = '$key'")->result_array();
        if($query){
            if ($query[0]['total_row'] != '') {
                return $query[0]['total_row'];
            } else {
                return 0;
            }
        }else{
            return 0;
        }
    }

    public function countwhereg($table, $id, $column, $key, $group)
    {
        $query = $this->db->query("select count($id) as total_row from $table where $column = '$key' group by $group")->result_array();
        if($query){
            if ($query[0]['total_row'] != '') {
                return $query[0]['total_row'];
            } else {
                return 0;
            }
        }else{
            return 0;
        }
    }

    public function countwherega($table, $id, $column, $key, $group)
    {
        $query = $this->db->query("select count($id) as total_row from $table where $column = '$key' group by $group");
        return $query->result_array();
    }

    public function count2where($table, $id, $column, $data, $column1, $data1)
    {
        $query = $this->db->query("select count($id) as total_row from $table
        where $column = '$data' and $column1 = '$data1'")->result_array();
        if($query){
            if ($query[0]['total_row'] != '') {
                return $query[0]['total_row'];
            } else {
                return 0;
            }
        }else{
            return 0;
        }
    }

    public function count4where($table, $id, $column, $data, $column1, $data1, $column2, $data2)
    {
        $query = $this->db->query("select count($id) as total_row from $table
        where $column = '$data' and $column1 = '$data1' and $column2 = '$data2'")->result_array();
        if($query){
            if ($query[0]['total_row'] != '') {
                return $query[0]['total_row'];
            } else {
                return 0;
            }
        }else{
            return 0;
        }
    }

    public function sum2where($tabel, $take, $column, $data, $column1, $data1)
    {
        $query = $this->db->query("select sum($take) as total from $tabel
        where $column = '$data' and $column1 = '$data1'")->result_array();
        if($query){
            if ($query[0]['total'] != '') {
                return $query[0]['total'];
            } else {
                return 0;
            }
        }else{
            return 0;
        }
    }

    public function sumwhere($tabel, $take, $column, $data)
    {
        $query = $this->db->query("select sum($take) as total from $tabel
        where $column = '$data'")->result_array();
        if($query){
            if ($query[0]['total'] != '') {
                return $query[0]['total'];
            } else {
                return 0;
            }
        }else{
            return 0;
        }
    }

    public function in3p($table, $column, $in)
    {
        return $query = $this->db->query("select * from $table where $column in ($in)")->result_array();
    }

    public function sumin2where($tabel, $take, $column, $in, $column1, $data1, $column2, $data2)
    {
        $query = $this->db->query("select sum($take) as total from $tabel
		where $column in ($in) and $column1 = '$data1' and $column2 = '$data2'")->result_array();

        if($query){
            if ($query[0]['total'] != '') {
                return $query[0]['total'];
            } else {
                return 0;
            }
        }
      
    }

    public function in7p($table, $column, $in, $column1, $data1, $colum2, $data2)
    {
        return $query = $this->db->query("select * from $table where $column in ($in) and $column1 = '$data1' and $colum2 = '$data2'")->result_array();
    }

    public function notin3p($table, $column, $in)
    {
        return $query = $this->db->query("select * from $table where $column not in ($in)")->result_array();
    }

    public function limit($start, $content_per_page, $table)
    {
        $sql = "SELECT * FROM  $table order by id desc LIMIT $start,$content_per_page";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function limitWhere($start, $content_per_page, $table,$column,$key)
    {
        $sql = "SELECT * FROM  $table where $column like '%$key%' order by id desc LIMIT $start,$content_per_page";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function limitWhereNotLike($start, $content_per_page, $table,$column,$key)
    {
        $sql = "SELECT * FROM  $table where $column = '$key' order by id desc LIMIT $start,$content_per_page";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function limitWhereNotLikeOrder($start, $content_per_page, $table,$column,$key, $order)
    {
        $sql = "SELECT * FROM  $table where $column = '$key' order by '$order' LIMIT $start,$content_per_page";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function limit2Where($start, $content_per_page, $table,$column,$key,$column1,$key1)
    {
        $sql = "SELECT * FROM  $table where $column1 = '$key1' and $column like '%$key%' order by id desc LIMIT $start,$content_per_page";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function gda3plike($table,$column,$key,$order)
    {
        $sql = "select * from $table where $column like '%$key%' order by $order desc";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    //SPESIFIK QUERY -->

    public function monthly($month, $year)
    {
        $query = $this->db->query("select created_at, count(id) total_row, sum(transfer_ammount) total_ammount from purchasement where month(created_at) = '$month' and year(created_at) = '$year' group by date(created_at)")->result_array();
        return $query;
    }

    public function monthly_all($month, $year)
    {
        $query = $this->db->query("select * from purchasement where month(created_at) = '$month' and year(created_at) = '$year'")->result_array();
        return $query;
    }

    public function daily($day, $month, $year)
    {
        $query = $this->db->query("select * from purchasement where day(created_at) = '$day' and month(created_at) = '$month' and year(created_at) = '$year' group by date(created_at)")->result_array();
        return $query;
    }

    public function yearly($year)
    {
        $query = $this->db->query("select month(created_at) month, count(id) total_row, sum(transfer_ammount) total_ammount, sum(qty_count) qty from purchasement where year(created_at) = '$year' group by month(created_at)")->result_array();
        return $query;
    }

    public function review($year, $status)
    {
        $query = $this->db->query("select month(created_at) month, count(id) total_row, sum(transfer_ammount) total_ammount from purchasement where year(created_at) = '$year' and satisfaction = '$status' group by month(created_at)")->result_array();
        return $query;
    }

    public function get_chat($from_email, $to_email, $subject)
    {
        $query = $this->db->query("select *, count(id) total_chat from discussion where from_email = '$from_email' and to_email = '$to_email' and subject = '$subject' or  from_email = '$to_email' and to_email = '$from_email' and subject = '$subject' group by discussion_code order by status asc")->result_array();
        return $query;
    }

    public function get_chat_crm($from_email, $subject)
    {
        $query = $this->db->query("select *, count(id) total_chat from discussion where from_email = '$from_email' and subject = '$subject' or to_email = '$from_email' and subject = '$subject' group by discussion_code order by status asc")->result_array();
        return $query;
    }

    public function discussion_chat($code,$from_email, $to_email, $subject)
    {
        $query = $this->db->query("select * from discussion where from_email = '$from_email' and to_email = '$to_email' and discussion_code = '$code' and subject = '$subject' or  from_email = '$to_email' and to_email = '$from_email' and discussion_code = '$code' and subject = '$subject' order by created_at asc")->result_array();
        return $query;
    }

    public function get_chat_complain($email, $subject)
    {
        $query = $this->db->query("select *, count(id) total_chat from discussion where from_email = '$email' and subject = '$subject' or  to_email = '$email' and subject = '$subject' group by discussion_code order by status asc")->result_array();
        return $query;
    }

    public function complain_chat($code,$email, $subject)
    {
        $query = $this->db->query("select * from discussion where from_email = '$email' and discussion_code = '$code' and subject = '$subject' or  to_email = '$email' and discussion_code = '$code' and subject = '$subject' order by created_at asc")->result_array();
        return $query;
    }


}
