<?php
defined('BASEPATH') or exit('No direct script access allowed');

class D_table extends CI_Model
{

    public function Datatables($dt)
    {
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $sql = "SELECT {$columns} FROM {$dt['table']}";
        $data = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();
        $columnd = $dt['col-display'];
        $count_c = count($columnd);
        $search = $dt['search']['value'];
        $where = '';
        if ($search != '') {
            for ($i = 0; $i < $count_c; $i++) {
                $where .= $columnd[$i] . ' LIKE "%' . $search . '%"';

                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }
        for ($i = 0; $i < $count_c; $i++) {
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                break;
            }
        }
        if ($where != '') {
            $sql .= " WHERE " . $where;
        }

        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        $start = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        $list = $this->db->query($sql);
        $option['draw'] = $dt['draw'];
        $option['recordsTotal'] = $rowCount;
        $option['recordsFiltered'] = $rowCount;
        $option['data'] = array();

        foreach ($list->result_array() as $row => $val) {
            $rows = array();
            $id = 0;
            $data = 1;
            foreach ($dt['col-display'] as $key => $kolom) {
                if ($id == 0) {
                    $id = $val[$kolom];
                    $rows[] = $val[$kolom];
                } else if($data ==3 && $dt['table'] == 'billing' || $data ==3 && $dt['table'] == 'purchasement'){
                    $rows[] = $this->mylib->torp($val[$kolom]);
                } else if($data ==4 && $dt['table'] == 'billing'){
                    $status = $val[$kolom];
                } else if($data ==5 && $dt['table'] == 'billing'){
                    if($status == 'active'){
                        $rows[] = $val[$kolom];
                    }else{
                        $rows[] = '<p style="font-weight;bold;color:red;">Expired</p>';
                    }
                    
                }else {
                    $rows[] = $val[$kolom];
                }
                $data++;
            }

            if($dt['table'] == 'billing' || $dt['table'] == 'purchasement'){
            $rows[] =
                '<a id="detail" title="Edit" class="btn btn-info btn-xs waves-effect" onclick="' . $dt['d'] . '(' . "'" . $id . "'" . ');""><i class="fa fa-eye"> Detail</i></a>';
            }else{
                if($_SESSION['com_in']['role'] == 4 && $dt['table'] == 'members'){
                    $rows[] ='<a id="destroy" title="Hapus" class="btn btn-danger btn-xs waves-effect" onclick="' . $dt['d'] . '(' . "'" . $id . "'" . ');"><i class="fa fa-trash-o"> Hapus</i></a>';
                }else if($_SESSION['com_in']['role'] == 3 && $dt['table'] == 'members'){
                    $rows[] =
                '<a id="edit" title="Edit" class="btn btn-info btn-xs waves-effect" onclick="' . $dt['e'] . '(' . "'" . $id . "'" . ');""><i class="fa fa-pencil-square-o"> Edit</i></a>';
                }else{
                    $rows[] =
                '<a id="edit" title="Edit" class="btn btn-info btn-xs waves-effect" onclick="' . $dt['e'] . '(' . "'" . $id . "'" . ');""><i class="fa fa-pencil-square-o"> Edit</i></a>
            	|<a id="destroy" title="Hapus" class="btn btn-danger btn-xs waves-effect" onclick="' . $dt['d'] . '(' . "'" . $id . "'" . ');"><i class="fa fa-trash-o"> Hapus</i></a>';
                }
                
            }
            $option['data'][] = $rows;
        }

        echo json_encode($option);
    }

    public function gdo4p($table, $take, $column, $id)
    {
        $this->db->where($column, $id);
        $query = $this->db->get($table)->result_array();
        return $query[0][$take];
    }

}
