<?php
class Mylib
{
    public function setJSON()
    {
        header('Content-Type:application/json');
    }

    public function random_string($length)
    {
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= $chars[rand(0, strlen($chars) - 1)];

        }
        return $string;
    }

    public function random_number($length)
    {
        $chars = "1234567890";
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= $chars[rand(0, strlen($chars) - 1)];

        }
        return $string;
    }

    public function torp($number)
    {
        return 'Rp. ' . strrev(implode('.', str_split(strrev(strval($number)), 3)));
    }

    public function to_date($date)
    {

        $bersih = explode(' ', $date);

        $pisah = explode('-', $bersih[0]);

        if ($pisah[1] == '01') {
            $pisah[1] = 'Januari';
        } else if ($pisah[1] == '02') {
            $pisah[1] = 'Februari';
        } else if ($pisah[1] == '03') {
            $pisah[1] = 'Maret';
        } else if ($pisah[1] == '04') {
            $pisah[1] = 'April';
        } else if ($pisah[1] == '05') {
            $pisah[1] = 'Mei';
        } else if ($pisah[1] == '06') {
            $pisah[1] = 'Juni';
        } else if ($pisah[1] == '07') {
            $pisah[1] = 'Juli';
        } else if ($pisah[1] == '08') {
            $pisah[1] = 'Agustus';
        } else if ($pisah[1] == '09') {
            $pisah[1] = 'September';
        } else if ($pisah[1] == '10') {
            $pisah[1] = 'Oktober';
        } else if ($pisah[1] == '11') {
            $pisah[1] = 'November';
        } else if ($pisah[1] == '12') {
            $pisah[1] = 'Desember';
        }

        $urutan = array($pisah[2], $pisah[1], substr($pisah[0], -2));
        return $satukan = implode(' ', $urutan);

    }

    public function to_time($date)
    {

        $bersih = explode(' ', $date);

        return $satukan = $bersih[1];

    }

    public function to_date_time($date)
    {

        if(!empty($date)){
            $bersih = explode(' ', $date);

            $pisah = explode('-', $bersih[0]);

            if ($pisah[1] == '01') {
                $pisah[1] = 'Januari';
            } else if ($pisah[1] == '02') {
                $pisah[1] = 'Februari';
            } else if ($pisah[1] == '03') {
                $pisah[1] = 'Maret';
            } else if ($pisah[1] == '04') {
                $pisah[1] = 'April';
            } else if ($pisah[1] == '05') {
                $pisah[1] = 'Mei';
            } else if ($pisah[1] == '06') {
                $pisah[1] = 'Juni';
            } else if ($pisah[1] == '07') {
                $pisah[1] = 'Juli';
            } else if ($pisah[1] == '08') {
                $pisah[1] = 'Agustus';
            } else if ($pisah[1] == '09') {
                $pisah[1] = 'September';
            } else if ($pisah[1] == '10') {
                $pisah[1] = 'Oktober';
            } else if ($pisah[1] == '11') {
                $pisah[1] = 'November';
            } else if ($pisah[1] == '12') {
                $pisah[1] = 'Desember';
            }

            $urutan = array($pisah[2], $pisah[1], substr($pisah[0], -2));
            $satukan = implode(' ', $urutan);

            return $satukan.' '.$bersih[1];
        }else{
            return '-';
        }
        

    }

    public function to_month($month)
    {
        if ($month == 1) {
            return 'Januari';
        } else if ($month == 2) {
            return 'Februari';
        } else if ($month == 3) {
            return 'Maret';
        } else if ($month == 4) {
            return 'April';
        } else if ($month == 5) {
            return 'Mei';
        } else if ($month == 6) {
            return 'Juni';
        } else if ($month == 7) {
            return 'Juli';
        } else if ($month == 8) {
            return 'Agustus';
        } else if ($month == 9) {
            return 'September';
        } else if ($month == 10) {
            return 'Oktober';
        } else if ($month == 11) {
            return 'November';
        } else if ($month == 12) {
            return 'Desember';
        }
    }

}
